FROM eboraas/apache
MAINTAINER Ed Boraas <ed@boraas.ca>
RUN apt-get update && apt-get -y install php5 php5-mysql vim && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN /usr/sbin/a2dismod 'mpm_*' && /usr/sbin/a2enmod mpm_prefork
RUN a2enmod rewrite	
COPY ./conf/apache2.conf /etc/apache2
EXPOSE 80
EXPOSE 443
CMD ["/usr/sbin/apache2ctl", "-D", "FOREGROUND"]
