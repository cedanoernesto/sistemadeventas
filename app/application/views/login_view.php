<style type="text/css">
    .green{
        color: #159145;
    }
    .a_green{
        color: #159145;
        font-weight: bold;
    }
    .secure_icon{
        cursor: pointer;
    }
</style>
<script type="text/javascript" src="<?= base_url('public/js/components/webix_utils.js')."?ver=".uniqid(); ?>"></script>
<script type="text/javascript" src="<?= base_url('public/js/components/views/login.js')."?ver=".uniqid(); ?>"></script>
<script>
    var id_negocio = "<?= $id_negocio ?>";
    _URL_LOGO = "<?= base_url('public/images/negocios').'/'.$logo ?>";
    document.getElementById("pager").remove();
    webix.ready(function(){
        $$('btn_menu').hide();
        <?php if ($log==1): ?>
        $$('notify').hide();
        <?php endif; ?>
        $$('menuOptions').hide();
        $$('generic_button_new').hide();
        webix.ui({
            container:'container',
            rows:[
                {
                    cols:[
                        {},
                        {width:250,height:250,view:'template', template:"<img style='width:250px;' src='"+_URL_LOGO+"'/>",borderless:true},
                        {}
                    ]
                },
                {cols:[
                    {},
                    {rows:[
                        {view:'toolbar',
                        cols:[
                            {view:'label',label:'Inicio de sesión',align:'center'}
                        ]},
                        login_form
                    ]},
                    {}
                ]},
                {},
                {
                    cols:[
                        {},
                       // {view:'template',borderless:true,width:300,height:60,template:"<img width='300' src='<?= base_url('public/images/poweredbycloudflare.png') ?>'/>"}
                    ]
                }
            ]
            });
            $$('usuario').focus();
            webix.ui(popup_secure);
        
            /*$$('usuario').setValue("ernesto");
            $$('contrasena').setValue("123");*/
    });
</script>
</html>
