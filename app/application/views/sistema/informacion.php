<script src ="<?= base_url('public/js/components/views/sistema/functions_informacion.js') ?>"></script>
<script src ="<?= base_url('public/js/components/views/sistema/informacion.js') ?>"></script>

<script type="text/javascript">

    webix.ready(function(){
        webix.ui({
            container:'container',
            rows:[
                {
                    cols:[
                        {width:100},
                        form,
                        {width:100}
                    ]
                }
            ]
        });
        $$('generic_button_new').hide();
        webix.ajax().post(URL_INFORMACION,{},function(response){
            response = JSON.parse(response);
            $$('form_informacion').setValues(response[0]);
            if (response[0].logo!="")
            $$('uploader_logo').files.parse([{name:response[0].logo}]);
        });
    });
</script>