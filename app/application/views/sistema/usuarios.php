<script type="text/javascript" src ="<?= base_url('public/js/components/utils.js')?>"></script>
<script type="text/javascript" src ="<?= base_url('public/js/components/webix_utils.js')?>"></script>
<script type="text/javascript" src ="<?= base_url('public/js/components/forms/sistema/usuarios.js')?>"></script>
<script type="text/javascript" src ="<?= base_url('public/js/components/datatables/sistema/usuarios.js')?>"></script>
<script type="text/javascript">
webix.ready(function(){
  webix.ui(generic_window);
  webix.ui({
    container:'container',
    rows:[
      datatable_usuarios
    ]
  });
  $$('generic_button_new').attachEvent("onItemClick",function(){
      $$('generic_window').show();
      $$('btn_change_estatus').hide();
      $$('form_usuarios').clear();
      $$('nombre').focus();
      $$('id').setValue('0');
  });
    webix.extend($$('datatable_usuarios'),webix.ProgressBar)
    $$("datatable_usuarios").showProgress({
        type:"icon"
    });

});
</script>
