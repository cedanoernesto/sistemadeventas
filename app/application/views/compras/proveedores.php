<script src="<?=base_url('public/js/components/utils.js') ?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js') ?>"></script>
    <script src="<?=base_url('public/js/components/datatables/compras/proveedores.js') ?>"></script>
<script src="<?=base_url('public/js/components/forms/compras/proveedores.js') ?>"></script>
<script type="text/javascript">
    webix.ready(function(){
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            rows:[
                datatable_proveedores,
            ]
        });
        $$('generic_button_new').attachEvent("onItemClick",function(){
            $$('form_proveedores').clear();
            $$('form_proveedores').clearValidation();
            $$('id').setValue("0");
            $$('generic_window').show();
            $$('btn_change_estatus').hide();
            $$('nombre').focus();
        });
        webix.extend($$('datatable_proveedores'),webix.ProgressBar)
        $$("datatable_proveedores").showProgress({
            type:"icon"
        });
    });

</script>
