<script type="text/javascript" src="<?= base_url('/public/js/components/datatables/compras/reporte.js') ?>"> </script>
<script type="text/javascript">
    document.getElementById('pager').remove();
    webix.ready(function () {
       webix.ui({
         container:'container',
           rows:[
               {
                 cols:[
                     {},
                     {view:'combo',id:'tipo',value:2,width:200,label:'Mostrar compras de ',labelPosition:'top',options:[
                         {id:1,value:'Dia'},
                         {id:2,value:'Semana'},
                         {id:3,value:'Mes'}
                     ],margin:20,
                     on:{
                         onChange:function (value) {
                             $$('datatable_reporte').clearAll();
                             $$('datatable_reporte').define('url',URL_GET_REPORTE+value);
                             $$('datatable_reporte').refresh();
                             $$('datatable_reporte_detalles').clearAll();
                             $$('datatable_reporte_detalles').showOverlay("Seleccione un registro del resumen para ver los detalles");
                         }
                     }},
                     {}

                 ]
               },
               {height:30},
               {
                   cols:[
                       {
                           rows:[
                               {
                                   cols:[
                                       {},
                                       {view:'template',template:'<h1>Resumen </h1>',borderless:true,height:80,width:300},
                                       {}
                                   ]
                               },
                               datatable_reporte
                           ]
                       },
                       {width:20},
                       {
                           rows:[
                               {
                                   cols:[
                                       {},
                                       {view:'template',template:'<h1>Detalles de compras</h1>',borderless:true,height:80,width:300},
                                       {}
                                   ]
                               },
                               datatable_reporte_detalles
                           ]
                       }

                   ]
               }
           ]
       });

    });

</script>