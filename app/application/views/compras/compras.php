<script charset="utf-8" src="<?= base_url('public/js/components/views/compras/compras.js') ?>"></script>
<script type="text/javascript" charset="UTF-8">
    webix.ready(function(){
        webix.ui({
            view:"layout",
            container:"container",
            rows:[
                form_compras
            ]
        });
        $$('id_articulo').focus();
        actualizarArticulos();
    });
</script>