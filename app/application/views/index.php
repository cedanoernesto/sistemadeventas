<script type="text/javascript" src="<?= base_url('public/js/components/clock.js')?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js')?>"></script>
<script src="<?=base_url('public/webix/sidebar.js')?>"></script>
<link rel="stylesheet" href="<?= base_url('public/webix/sidebar.css')?>">
<link rel="stylesheet" href="<?= base_url('public/css/animate.css')?>" type="text/css" media="screen" charset="utf-8">
<div style="display: none;" data-view="submenu" data-id="submenu1">
    <ul  data-view="data">
       <li data-id="1.1">Cerrar sesíon</li>
    </ul>
</div>
<div data-view="rows">
  <div data-view="toolbar" data-id="toolbar" data-height="52">
      <div data-icon="bars" data-view="icon" data-id="btn_menu" ></div>
      <div data-view="label" data-id="label_place" data-label="<?= $nombre_negocio ?>" ></div>
      <div data-view="label" style:"font-size:80px;" data-label="<b><?php echo (isset($modulo))?$modulo:""; ?> </b>" data-align="center" data-gravity="2"></div>
    <?php if (!$log): ?>
        <div data-view="label" data-label=""></div>
    <?php endif; ?>
    <?php if (isset($id_perfil) && $id_perfil==1): ?>
      <div data-view="button" data-id="notify" data-type="icon" data-icon="bell-o" data-badge="" data-width="40"></div>
    <?php endif;?>

    <div data-view="menu" data-height="50" data-id="menuOptions"  data-width="300">
    <ul data-view="data">
        <div data-view="menu">
            <ul data-view="data">
                <li style="display: none;"  data-align="right"  data-id="1" data-submenu="submenu1">Bienvenido <?php echo (isset($perfil) && isset($usuario))?$perfil.": ".$usuario:""; ?></li>
            </ul>
        </div>
    </ul>
</div>
  </div>
      <div data-view="rows">
          <div data-view="cols">
                    <div data-view="rows">
                        <div data-id="webix_container" id="container" style="height:100%"></div>

                        <div data-view="cols">
                            <div  data-borderless="true" data-height="1" ></div>
                            <div id="pager" data-borderless="true"  data-align="middle" style="margin-left: 25%" data-height="38" ></div>
                            <div  data-borderless="true" ></div>
                        </div>
                        <!--div id="toolbar"    data-height="56" >
                            </div>-->
                        <div data-view="toolbar" data-id="footer_toolbar">
                            <div data-view="label" data-id="label_date" data-css="label_date" ></div>
                            <div data-view="label" data-id="label_clock" data-css="label_clock" ></div>
                            <div data-view="button" data-id="generic_button_new" data-label="Nuevo" data-width="120" data-type="imageButton"></div>
                        </div>
                        </div>
                    </div>
          </div>
      </div>
</div>
<!--
<div id="img_loading"  >
    <img src="<?= base_url('public/images/sistema/iconos/Icon-512@2x.png')?>">
</div>
<div  id ='loading'class='loading'>
    <div class='loading-bar'></div>
    <div class='loading-bar'></div>
    <div class='loading-bar'></div>
    <div class='loading-bar'></div>
</div>-->
<style type="text/css">
    #img_loading img{
        position:    absolute;
        top:         50%;
        left:        50%;
        width:        512px;
        height:       512px;
        margin-left: -250px;
        opacity: .5;
        margin-top:  -240px;
    }
.label_clock div{
    font-weight: bold;
    font-size: 30px;
}
.label_date div{
    font-size: 22px;
    cursor: pointer;
}
    .modulo{
        font-size: 25px;
    }
.my_menu .webix_view{
    background-color: #ECEFF1;
}
.my_menu .webix_list_item{
    line-height: 35px;
    border-bottom-color: #ddd;
}

.inactivo{
     background-color:#ffaaaa;
 }
.bajo_inventario{
    background-color: #ffbe65;
}
.ui-pnotify-title{
    font-family: 'PT Sans',Tahoma;
    font-size: 15px;
    color: #323135;
}
.ui-pnotify-text{
    font-family: 'PT Sans',Tahoma;
    font-size: 15px;
    color: #323135;
}
.nuevo{
    background-color: #c7e1ff;
}
.hora{
    width:150px;
    text-align: center;
    font-weight:bold;
    float:right;
    background-color:#444;
    color:white;
    border-radius:3px;
}
    .logo{
        content:""; /*removes FontAwesome icon */
        background-image: url("https://www.sistemadeventas.com.mx/public/images/sistema/iconos/76x50.png");
        background-repeat: no-repeat;

    }
    .label_sistema{
        font-size: 50px;
    }
    .hover_tree{
        background-color: red;
    }
</style>
<script>
    URL_NOTIFICACIONES = BASE_URL +"sistema/general/notificacion";
    URL_VER_NOTIFICACION = BASE_URL +"sistema/general/verNotificacion";
    var sound = new Howl({
        urls: ['<?= base_url('public') ?>/sound2.mp3']
    });
    var width = screen.width;
    function toggle_menu(){
        if( $$("menu").config.hidden){
            $$("menu").show();
           //document.getElementById('container').style.width=""+(window.innerWidth-$$('sidebar').$width)+"px";
            //document.getElementById('container').style.margin="0px 0px 0px "+$$('sidebar').$width+"px";
         //   $$('webix_container').$setSize(window.innerWidth-$$('sidebar').$width-10, 'auto');
           // $$('container').adjust();

        }
        else{
            $$("menu").hide();
            //document.getElementById('container').style.width="100%";
           // document.getElementById('container').style.margin="0px 0px 0px 0px";
           // $$('webix_container').$setSize(window.innerWidth-10, 'auto');
        }


    }
    function notificacion(){
        var popup_notify ={
            view:'popup',
            id:'popup_notify',
            width:500,
            height:200,
            body:{
                view:'list',
                id:'list_notify',
                select:true,
                url:URL_NOTIFICACIONES,
                scheme:{
                    $init:function(obj){
                        if (obj.estatus==0) obj.$css = "nuevo";
                    }
            },
                template:"#value# <div class='hora'>#fecha#</div>",
                type:{
                    height:62
                },
        }
        };
        webix.ui(popup_notify);
        $$('notify').define('popup',popup_notify);
        webix.ajax().post(URL_NOTIFICACIONES,{},function(response){
            response = JSON.parse(response);
            var n =0;
            response.forEach(function(item,index){
                if(item.estatus==0){
                    n++;
                    sound.play();
                    webix.message({text:item.value});
                    $$('notify').define('badge',n);
                    $$('notify').refresh();
                }
            });
        });

    }
    function coordenadas(event) {
        x=event.clientX;
        y=event.clientY;
        if (x>=0 && x<=30){
           // $$('sidebar').expand();
            //$$('menu').show();
        }

        else if (x>270 && y<600){
            //$$('sidebar').collapse();
            // $$('menu').hide();
        }

    }
    var tree=    {
        view: "sidebar",
        id:"sidebar",
       // collapsed:true,
        url:BASE_URL+"login/getModules/<?php echo (isset($id_seccion))?$id_seccion:0; ?>",
        template:"{common.icon()}<span style='cursor: pointer;'><img src='#icono#' style='float:left; margin:3px 10px 0px 10px; '>#value#</span>",
        on:{
            onItemClick:function(id){
                var item = this.getItem(id);
                if(item.$level!=1){
                    window.location=item.url;
                }
                else{
                  /*  var open =this.getState().open;
                    var flag  =false;
                    for (var i=0; i<open.length; i++){
                        if (open[i]==id)
                            flag=true;
                    }
                        if (flag)
                            this.close(id);
                        else
                        this.open(id);*/

                }
            },
            onAfterLoad:function(){
                this.open("<?php echo (isset($id_seccion))?$id_seccion:0; ?>");
                this.select("<?php echo (isset($id_modulo))?$id_modulo:0; ?>");

            }

        }
    };

URL_SESS_DESTROY =BASE_URL +"login/sessDestroy";
    webix.ready(function(){

       // $$('cargando').hide()
       // $$('cargando').show();
       // var popup_notify =
    // webix.ui(window_calendar);
      webix.markup.init();
        $$('generic_button_new').define('image',BASE_URL_ICONS+"new.png");
        $$('generic_button_new').refresh();
      $$('label_place').attachEvent('onItemClick',function(){
          document.location = "<?= base_url('inicio') ?>"
      });
        webix.ui({
            view: "sidemenu",
            id: "menu",
            width: 250,
            position: "left",
            state:function(state){
                var toolbarHeight = $$("toolbar").$height;
                state.top = toolbarHeight;
                state.height -=toolbarHeight;
            },
            css: "my_menu",
            body:tree
        });
        <?php if ($log==1 ): ?>
        if (width>1600){
            toggle_menu();
        }
        <?php endif; ?>
       // webix.ui(generic_toolbar);

        $$('label_date').define("label",date());
        $$('label_date').refresh();
        
        var hora_servidor = new Date("<?= date("Y-m-d H:i:s")?>");
        setInterval(function(){
            hora_cliente = hora_servidor;
            hora_cliente.setSeconds(hora_cliente.getSeconds()+1);
            $$('label_clock').define("label",clock(hora_cliente));
            $$('label_clock').refresh();
        },1000);
        webix.ui(calendar);
        $$('label_date').attachEvent("onItemClick",function(){
            $$('calendar').show();
        });
        $$('btn_menu').attachEvent("onItemClick",function(){
            //$$('sidebar').toggle();
            toggle_menu();
        });

          $$('menuOptions').attachEvent("onMenuItemClick",function(id){
            switch(id){
            case "1.1":
                webix.confirm({
                    text:"¿Desea cerrar sesíon?",
                    ok:"si",
                    cancel:"no",
                    callback:function(result){
                        if(result)
                            window.location=URL_SESS_DESTROY;
                    }
                });
            break;
        }
      });
        <?php if ($log==1 && $id_perfil==1): ?>
        //notificacion();
        $$('notify').attachEvent('onItemClick',function(){
            $$('notify').define('badge','0');
            $$('notify').refresh();
            webix.ajax().post(URL_VER_NOTIFICACION,{},function(){

            });
        });
        <?php endif; ?>
        /*document.getElementById("loading").style = "display:none;";
        document.getElementById("img_loading").remove();
        */
        webix.event(window, "resize", function(){
          //  $$('webix_container').adjust();
           // $$('container').adjust();
        });
    });
</script>
