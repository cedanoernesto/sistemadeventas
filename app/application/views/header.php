<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <title><?= $modulo ?></title>
    <link rel="icon" type="image/png" href="<?= base_url('public/images/Icon-29.png')."?v=".uniqid();?>" />
    <script   src="https://code.jquery.com/jquery-2.2.3.min.js"   integrity="sha256-a23g1Nt4dtEYOj7bR+vTu7+T8VP13humZFBJNIYoEJo="   crossorigin="anonymous"></script>
    <script src="<?= base_url('public/webix/webix.js')?>"></script>
    <script src="<?= base_url('public/js/pnotify.min.js')?>"></script>
    <script src="<?= base_url('public/js/components/windows/loading.js')?>"></script>
    <script src="<?= base_url('public/js/howler.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.js"></script>
    <link rel="stylesheet" href="<?= base_url('public/webix/skins')."/".$tema?>" media="screen" charset="utf-8"/>
    <link rel="stylesheet" href="<?= base_url('public/css/pnotify.min.css')?>" media="screen" charset="utf-8"/>
    <link rel="stylesheet" href="<?= base_url('public/css/loading.css')?>" media="screen" charset="utf-8"/>
    <link rel="stylesheet" href="<?= base_url('public/css/index.css')?>" media="screen" charset="utf-8">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/qtip2/2.2.1/jquery.qtip.min.css" media="screen" charset="utf-8">
    <script>
        BASE_URL =  "<?= base_url() ?>";
        BASE_URL_ICONS = "<?= base_url('public/images/icons') ?>/";
        BASE_URL_MODULES = "<?= base_url('public/images/modules') ?>/";
        webix.ready(function(){
            webix.ui(loading);
        });
    </script>
</head>
<body>
