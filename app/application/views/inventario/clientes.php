<script src="<?=base_url('public/js/components/utils.js')?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js')?>"></script>
<script src="<?=base_url('public/js/components/datatables/inventario/clientes.js')?>"></script>
<script src="<?=base_url('public/js/components/forms/inventario/clientes.js')?>"></script>
<script type="text/javascript">
    webix.ready(function(){
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            rows:[
                datatable_clientes
            ]
        });

        $$('generic_button_new').attachEvent("onItemClick",function(){
            $$('btn_change_estatus').hide();
            $$('form_clientes').clear();
            $$('form_clientes').clearValidation();
            $$('id').setValue("0");
            $$('generic_window').show();
            $$('nombre').focus();
        });
        webix.extend($$('datatable_clientes'),webix.ProgressBar)
        $$("datatable_clientes").showProgress({
            type:"icon"
        });
    });

</script>
