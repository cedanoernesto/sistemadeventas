<script src="<?=base_url('public/js/components/utils.js')  ?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js')  ?>"></script>
<script src="<?=base_url('public/js/components/datatables/inventario/servicios.js')  ?>"></script>
<script src="<?=base_url('public/js/components/forms/inventario/servicios.js')  ?>"></script>
<script type="text/javascript">
    webix.ready(function(){
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            rows:[
                datatable_servicios
            ]
        });
        $$('generic_button_new').attachEvent("onItemClick",function(){
            $$('btn_change_estatus').hide();
            $$('form_servicios').clear();
            $$('form_servicios').clearValidation();
            $$('id').setValue("0");
            $$('generic_window').show();
            $$('descripcion').focus();
        });
        webix.extend($$('datatable_servicios'),webix.ProgressBar)
        $$("datatable_servicios").showProgress({
            type:"icon"
        });
    });

</script>
