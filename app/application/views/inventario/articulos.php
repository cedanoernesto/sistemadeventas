<script src="<?=base_url('public/js/components/utils.js') ?>"></script>
<script src="<?=base_url('public/js/components/datatables/inventario/articulos.js').'?ver='.uniqid() ?>"></script>
<script src="<?=base_url('public/js/components/forms/inventario/articulos.js').'?ver='.uniqid() ?>"></script>
<script type="text/javascript">
    var URL_PREFERENCIAS_ARTICULOS =BASE_URL +'inventario/articulos/preferencias/';
    var clonar=0;
    var id_tipo_venta=0;
    var obj_pref = {};
        webix.ajax().sync().post(URL_PREFERENCIAS_ARTICULOS,{},function (response) {
            response = JSON.parse(response);
            obj_pref.config_clonar=parseInt(response[0].clonar);
            obj_pref.aplicar_iva=parseInt(response[0].aplicar_iva);
            obj_pref.desc_mayoreo=parseFloat(response[0].descuento_mayoreo);
            obj_pref.sugerir_precio_venta=parseFloat(response[0].sugerencia_precio_venta);
            obj_pref.maneja_proveedor=parseInt(response[0].maneja_proveedor);
            obj_pref.venta_unidad=parseInt(response[0].venta_unidad);
            obj_pref.maneja_categoria=parseInt(response[0].maneja_categoria);
        });
    webix.ready(function(){
       // debugger;
        webix.ui(contextmenu_articulos);
        webix.ui(window_cantidades);
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            id:'webix_container',
            rows:[
                datatable_articulos,
            ]
        });
        $$('generic_button_new').attachEvent("onItemClick",nuevo_articulo);
        $$('generic_button_new').setValue('label');
        $$('generic_button_new').refresh();
        $$('generic_window').attachEvent('onHide',function () {$$('datatable_articulos').unselect($$('datatable_articulos').getSelectedId());});
        if (!obj_pref.maneja_categoria){
            $$('id_categoria').hide();
        }
        if (!obj_pref.maneja_proveedor){
            $$('cont_id_proveedor').hide();
        }
        if (!obj_pref.venta_unidad){
            $$('cont_venta_unidad').hide();
        }
        webix.ajax().sync().post(URL_TIPOS_VENTAS,{},function (response) {
            response = JSON.parse(response);
           if (response.length>1){
               $$('id_tipo_venta').define('options',response);
               $$('id_tipo_venta').refresh();
           }
            else{
               $$('id_tipo_venta').hide();
               id_tipo_venta = response[0]['id'];
           }

        });
        $$("cmenu").attachTo($$("datatable_articulos"));
        webix.extend($$('datatable_articulos'),webix.ProgressBar)
        $$("datatable_articulos").showProgress({
            type:"icon"
        });
    });
</script>
