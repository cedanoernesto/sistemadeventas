<script src="<?=base_url('public/js/components/utils.js')  ?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js')  ?>"></script>
<script src="<?=base_url('public/js/components/forms/inventario/paquetes.js')  ?>"></script>
<script src="<?=base_url('public/js/components/datatables/inventario/paquetes.js')  ?>"></script>
<style>
    .help_block{
        font-size: 10.5px;
    }
</style>
<script type="text/javascript">
    var obj_pref ={};
    var URL_PREFERENCIAS= BASE_URL+ 'inventario/paquetes/preferencias';
    webix.ready(function(){
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            rows:[
                datatable_paquetes
            ]
        });
        webix.ajax().sync().post(URL_PREFERENCIAS,{},function(response){
            response = JSON.parse(response);
            obj_pref.maneja_hora=parseInt(response[0].maneja_hora);
        });

        if (!obj_pref.maneja_hora){
           $$('cont_maneja_hora').hide();
        }
        $$('cont_exacto').hide();
        $$('generic_button_new').attachEvent("onItemClick",function(){
            $$('form_paquetes').clear();
            $$('form_paquetes').clearValidation();
            $$('generic_window').show();
            $$('id').setValue("0");
            $$('btn_change_estatus').hide();
            //$$('tipo_venta').setValue('c81e728d9d4c2f636f067f89cc14862c');
            cargarArticulos();
        });
        webix.extend($$('datatable_paquetes'),webix.ProgressBar)
        $$("datatable_paquetes").showProgress({
            type:"icon"
        });
    });
</script>
