<script src="<?=base_url('public/js/components/utils.js')?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js')?>"></script>
<script src="<?=base_url('public/js/components/datatables/inventario/categorias.js')?>"></script>
<script src="<?=base_url('public/js/components/forms/inventario/categorias.js')?>"></script>
<script type="text/javascript">
    webix.ready(function(){
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            rows:[
                datatable_categorias
            ]
        });

        $$('generic_button_new').attachEvent("onItemClick",function(){
            $$('btn_change_estatus').hide();
            $$('form_categorias').clear();
            $$('form_categorias').clearValidation();
            $$('id').setValue("0");
            $$('generic_window').show();
            $$('nombre').focus();
        });
        webix.extend($$('datatable_categorias'),webix.ProgressBar)
        $$("datatable_categorias").showProgress({
            type:"icon"
        });
    });

</script>
