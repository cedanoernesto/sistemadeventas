<script src="<?=base_url('public/js/components/utils.js')."?ver=".uniqid() ?>"></script>
<script src="<?=base_url('public/js/components/webix_utils.js')."?ver=".uniqid() ?>"></script>
<script src="<?=base_url('public/js/components/datatables/apartados/apartados_pendientes.js')."?ver=".uniqid() ?>"></script>
<script src="<?=base_url('public/js/components/forms/apartados/apartados_pendientes.js')."?ver=".uniqid() ?>"></script>
<script type="text/javascript">
    webix.ready(function(){
        webix.ui(generic_window);
        webix.ui({
            container:"container",
            rows:[
                datatable_apartados_pendientes
            ]
        });
        $$('generic_button_new').hide();

    });

</script>
