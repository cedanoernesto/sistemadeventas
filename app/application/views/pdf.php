<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <title>Provincias españolas en pdf</title>
 <style type="text/css">
        body {
            background-color: #fff;
            margin: 40px;
            font-family: Lucida Grande, Verdana, Sans-serif;
            font-size: 14px;
            color: #4F5155;
        }

        a {
            color: #003399;
            background-color: transparent;
            font-weight: normal;
        }

        h1 {
            color: #444;
            background-color: transparent;
            font-size: 25px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
        }

        h2 {
            color: #444;
            background-color: transparent;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
            text-align: center;
        }
        h3{
          font-size: 25px;
        }

        table{
            text-align: center;
        }

        /* estilos para el footer y el numero de pagina */
        @page { margin: 180px 50px; }
        #header {
            position: fixed;
            left: 0px; top: -180px;
            right: 0px;
            height: 150px;
            background-color: #fff;
            color: #585958;
            font-family: Sans-serif;
            text-align: center;
            display: inline;
        }
        #header img{
            width:100px;
            padding-right: 600px;
        }
        #texto{
            width: 500px;
            margin: 0 auto;
            border-bottom: 1px solid #D0D0D0;
            border-bottom-width: medium;
        }
        #header h1 {
            color: #444;
            background-color: transparent;
            font-size: 30px;
            font-weight: bold;
            margin: -95px 0 2px 0;
            padding: 5px 0 6px 0;
        }
        #header h2 {
            color: #444;
            background-color: transparent;
            font-size: 16px;
            font-weight: bold;
            margin: 24px 0 2px 0;
            padding: 5px 0 6px 0;
            text-align: center;
        }
        #footer {
            position: fixed;
            left: 0px;
            bottom: -180px;
            right: 0px;
            height: 150px;
            background-color: red;
            color: #fff;
        }
        #footer .page:after {
            content: counter(page, upper-roman);
        }
        .separador_header{
          border-bottom: 1px solid #D0D0D0;
          width: 200px;
          height: 200px;
        }
        .header_info h3{
          margin-top: -55px;
        }
    </style>
</head>
<body>
<div id="header">
    <div id="contenedor_header">
    <img src="<?= base_url('public/images/papeleriaelmarcirculo.png') ?>">
        <h1>Papeleria el mar</h1>
        <div class="header_info">
            <h3>Modulo<h3>
        </div>
        <span class="separador_header"></span>
        <!--<h2>Articulos</h2>-->
    </div>
</div>
<div id="footer">
    <p class="page"></p>
</div>
<h2>Provincias españolas con html2pdf.</h2>
<table>
    <thead>
    <tr>
        <th width="100">Id</th>
        <th width="400">Provincia</th>
    </tr>
    </thead>
    <tbody>
    </tbody>
</table>
</body>
</html>
