<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/views/reportes/functions_reportes.js').'?v='.uniqid() ?>"></script>
<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/views/reportes/reportes.js').'?v='.uniqid() ?>"></script>
<script>
    var id_negocio = parseInt("<?= $id_negocio ?>");
    webix.ready(function(){
        $$('generic_button_new').hide();
        webix.ui({
            container:'container',
            rows:[
                {cols:[
                    {},
                    sup_reportes,
                    {}
                ]}
            ]
        });
        var fecha = new Date();
        $$('fecha_inicio').setValue(fecha);
        fecha.setDate(fecha.getDate()+1);
        $$('fecha_fin').setValue(fecha);
    });
</script>