<script src="<?= base_url('public/js/highcharts/js/highcharts.js') ?>"></script>
<script src="<?= base_url('public/js/components/inicio_admin.js') ?>"></script>

<script type="text/javascript" charset="utf-8">
    webix.ready(function(){
        webix.ui({
            container:'container',
            id:'webix_container',
            rows:[
              {
                cols:[
                  {},
                  {view:'combo',id:'tipo_reporte',name:'tipo_reporte',label:'Tipo de grafica',width:250,labelWidth:110,value:'da4b9237bacccdf19c0760cab7aec4a8359010b0',options:[
                      {id:'da4b9237bacccdf19c0760cab7aec4a8359010b0',value:'Semana'},
                      {id:'77de68daecd823babbb58edb1c8e14d7106e83bb',value:'Mes'},
                      {id:'1b6453892473a467d07372d45eb05abc2031647a',value:'Año'},
                  ],
                  on:{
                      onChange:loadChartLine
                  }},
                  {}
                ]
              },
                {
                  cols:[
                {view:'template',template:'<div id="chart_line" style="height: 100%;"></div>'},
                /*{
                  rows:[
                    {view:'template',template:'<div id="chart_circle" style="height: 100%;"></div>'},
                    {}
                  ]
                },*/
              ]
            },
               /* {
                    cols:[
                        {},
                       // {width:800,view:'template', template:'<img style="width:600px; margin-left:120px;" src="<?= base_url('public/images/papeleriaelmarcirculo.png') ?>"/>',borderless:true},
                        {}
                    ]
                },
                {height:10}*/
            ]
        });
        $$('generic_button_new').hide();
        $(function () {
            $('#chart_line').highcharts({
                chart: {
                    type: 'area',
                    events:{
                        load:function(){
                          //  loadChartLine($$('tipo_reporte').getValue());
                        }
                    }
                },
                title: {
                    text: 'Resumen de ventas'
                },
                subtitle: {
                   //text: 'Source: WorldClimate.com'
                },
                xAxis: {},
                yAxis: {
                    title: {
                        text: 'Dinero'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: true
                    }
                },
                series: []
            });
        });
    });
</script>
