<style type="text/css">
    .tipo_venta{
        font-weight: bold;
        /* text-decoration: underline;*/
        cursor: pointer;
        font-size: 20px;
    }
    .precio{
        font-weight: bold;
        font-size: 25px;
    }
    .total{
        font-weight: bold;
        font-size: 30px;
    }
    .cantidad{
        font-weight: bold;
        font-size:18px;
    }
    .cantidad_valor{
        font-weight: bold;
        font-size: 23px;
    }
    .existencia{
        font-weight: bold;
        font-size: 20px;
    }
    .webix_accordionitem{
        transition:width 0.5s;
    }
    .webix_accordionitem.vertical{
        transition:height 0.5s;
    }
</style>
<script src="<?= base_url('public/js/components/utils.js') ?>"></script>
<script src="<?= base_url('public/js/components/views/ventas/functions_ventas.js') ?>"></script>
<script src="<?= base_url('public/js/components/views/ventas/ventas.js') ?>"></script>
<script src="<?=  base_url('public/js/mousetrap.min.js') ?>"></script>
<script src="<?= base_url('public/js/mousetrap-global-bind.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/dependencies/rsvp-3.1.0.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/dependencies/sha-256.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/qz-tray.js')?>"></script>
<script type="text/javascript" charset="utf-8">
    setInterval(function () {
        webix.ajax().post(BASE_URL+'ventas/ventas/session',{})
    },600000);
    URL_PREFERENCIAS = BASE_URL +"ventas/ventas/preferencias";
    var obj_pref ={};
    window.onbeforeunload = function exitAlert(){
        if ($$('datatable_ventas').count() > 0){
            var text = "¿Desea cerrar la pagina?,se perderan los datos capturados de la venta";
            return text;
        }
    }
    document.addEventListener("keyup",function(event){
        var key = event.keyCode;
        if (key==27 && !$$('window_tipo_venta').isVisible()){
            if ($$('id_articulo').getValue()!="" && $$('id_articulo').getValue()!=0){
                mostrarArticulo($$('id_articulo').getValue());
                $$('id_articulo').setValue("");
            }
        }
        else if ($$('window_tipo_venta').isVisible()==true){
            if (key==49){
                mostrarPrecio(1);
                if (use_cod)
                    add();
            }
            else if (key==50){
                mostrarPrecio(2);
                if (use_cod){
                    add();
                }
            }
            else if (key==27){
                $$('window_tipo_venta').hide();
                // $$('id_articulo').setValue('0');
                $$('id_articulo').focus();
            }
        }
        Mousetrap.bindGlobal('ctrl+f1', function(e) {
            e.preventDefault()
            $$('id_articulo').blur()
            $$('id_articulo').getPopup().hide();
            cobrarVenta()
        });
        Mousetrap.bindGlobal('ctrl+f2', function(e) {
            e.preventDefault()
            mostrarCopia();
             if (obj_pref.maneja_copia){
             }
        });
        Mousetrap.bind('ctrl+f3', function(e) {
         e.preventDefault()
         if (obj_pref.nueva_nota){
         nuevaNota();
         }
         });
         //else if ($$('datatable_ventas').getSelectedId()!=undefined && key ==8 && $$('cantidad').getValue()==0)
         //deletes($$('datatable_ventas').getSelectedId());

        Mousetrap.bind('ctrl+f3', function(e) {
            e.preventDefault()
            if (obj_pref.maneja_gastos==1){
                gastos();
            }
        });
        if (key==27){
            if ($$('window_realizar_venta').isVisible()){
                $$('window_realizar_venta').hide();
            }
            else if ($$('window_copia').isVisible()){
                $$('window_copia').hide();
            }
            else if ($$('window_gastos').isVisible()){
                $$('window_gastos').hide();
            }
            else if ($$('window_comun').isVisible()){
                $$('window_comun').hide();
            }
            $$('id_articulo').focus();
        }
        else  if (key == 46){
            cancelarVenta();
        }
        else if (key==16){
            if ($$('precio_mayoreo').isEnabled()){
                $$('precio_mayoreo').callEvent("onItemClick");
            }
        }
    },false);
    var id_negocio = parseInt("<?= $id_negocio;?>");
    webix.ui(window_tipo_venta);
    webix.ui(window_realizar_venta);
    webix.ui(window_copia);
    webix.ui(window_gastos);
    webix.ui(window_salida);
    webix.ui(window_fondo);
    webix.ui(window_comun);
    webix.ui(window_cancelacion);
    //webix.ui(suggest_articulo);
    document.getElementById("pager").remove();
    webix.ready(function(){
        webix.ajax().sync().post(URL_PREFERENCIAS,{},function (response) {
            response = JSON.parse(response);
            obj_pref= formatObjPreferences(response[0]);
            $$('tipo_pago').define('options',obj_pref.tipos_pago);
            $$('tipo_pago').refresh();
            if (obj_pref.descuentos.length>1){
                $$('id_descuento').define('options',obj_pref.descuentos);
                $$('id_descuento').refresh();
                obj_pref.descuentos.forEach(function(item){
                    if (item.default == 1){
                        $$('id_descuento').setValue(item.id)
                    }
                })
            }
            else{
                $$('id_descuento').hide();
            }
        });
        webix.ui({
            view:'layout',
            id:'main_layout',
            container:'container',
            rows:[
                {
                    cols:[
                        {width:10},
                        {
                            rows:[
                                {view:'text',id:'nfondo',hidden:true},
                                sup_ventas,
                                datatable_ventas
                            ]
                        },
                        {width:10}

                    ]
                }
            ]
        });
        modulo=1;
        $$('generic_button_new').hide();
        actualizarArticulos();
        verificaFondo();
        getPaquetes();
        $$('cancelar_venta').hide();
        if (!obj_pref.boton_mayoreo)
            $$('precio_mayoreo').hide();
        if (!obj_pref.maneja_copias)
            $$('boton_copias').hide();
        if (!obj_pref.nueva_nota)
            $$('boton_nueva_nota').hide();
        if (!obj_pref.maneja_gastos)
            $$('boton_gastos').hide();
        $$('precio_mayoreo').disable();
        $$('btn_agregar').disable();
        /*$$('cantidad').getInputNode().addEventListener('click',function () {
         console.log(1)
         })*/
        $$('cantidad').attachEvent('onTimedKeyPress',function () {
            calcularSubtotal(this.getValue());
        });
        qz.websocket.connect()
    });

</script>
