<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title><?= $modulo; ?></title>
</head>
<style>
    @font-face {
        font-family: 'barcode';
        src: url(https://www.sistemadeventas.com.mx/public/fonts/barcode.ttf);
    }
    #barcode{
        margin-top: -10px;
        font-family: barcode;
        font-size: 100px;
        margin-bottom: -80px;
    }
    h1,h2,h3,h4,h5{
        font-family: arial;
    }
    label{
        font-family: arial;
    }
    h2,h3,h4,h5{
        margin-top: -15px;
    }
    table{
        width: 100%;
        font-family: Arial;
    }
    .separador{
        width: 250px;
    }
    .total{
        font-size: 18px;
    }
    .datos{
        font-size: 15px;
    }
    .datos_header{
        font-size: 15px;
        text-align: right;
    }
    hr{
        margin-top: -10px;
        margin-bottom: 15px;
    }
    img{
        position: relative;
        right: 180px;
        top:70px;
        margin-top: -50px;
    }
    .datos_separador{
        margin-bottom: 25px;
    }
    .aviso{
        font-family: arial;
        margin-top: 100px;
        margin-bottom: -80px;
    }
    .cliente{
        font-family: arial;
        margin-top: 120px;
        margin-bottom: -80px;
    }
    .sep{
        margin-bottom: 52px;
    }
    .articulo p{
        font-size: 15px;
        margin-top: -7px;
    }
    body{
        width:400px;
    }

</style>
<script>
    function imprime(){
        window.print();
    }
</script>
<body onload="imprime()">
<center>
    <h1><?= $datos_ticket['nombre_negocio']; ?></h1>
    <label class="datos_header"> <?= $datos_ticket['datos_fiscales']; ?></label>
    <br>
    <label class="datos_header"><?= $datos_ticket['domicilio']; ?></label>
    <br>
    <br>
</center>
<table>
    <tr>
        <td width="170" align="center">
            <h5 class="datos">  <?php  $date = date_create(date('Y-m-d H:i:s')); echo date_format($date,"d-m-Y H:i:s" ); ?></h5>
        </td>
    </tr>
</table>
<table>
    <thead>
    <th align="left"><p class="datos">DESCRIPCIÓN</p></th>
    <th align="left">MONTO</th>
    </thead>
    <tbody>
        <tr class="articulo" >
            <td align="left"><p>TOTAL POR VENTAS</p></td>
            <td align="left"><p>$<?=$total_ventas  ?></p></td>
        </tr>
        <tr class="articulo" >
            <td align="left"><p>GASTOS</p></td>
            <td align="left"><p>$<?= $gastos  ?></p></td>
        </tr>
        <tr class="articulo" >
            <td align="left"><p>TOTAL</p></td>
            <td align="left"><p>$<?= $total  ?></p></td>
        </tr>
    </tbody>
    <tfoot>

    </tfoot>
</table>
</table>
</body>
</html>
