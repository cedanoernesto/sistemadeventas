<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title><?= $modulo; ?></title>
</head>
<style>
    @font-face {
        font-family: 'barcode';
        src: url(https://www.sistemadeventas.com.mx/public/fonts/barcode.ttf);
    }
    #barcode{
        margin-top: -10px;
        font-family: barcode;
        font-size: 100px;
        margin-bottom: -80px;
    }
    h1,h2,h3,h4,h5{
        font-family: arial;
    }
    label{
        font-family: arial;
    }
    h2,h3,h4,h5{
        margin-top: -15px;
    }
    table{
        width: 100%;
        font-family: Arial;
    }
    .separador{
        width: 250px;
    }
    .total{
        font-size: 18px;
    }
    .datos{
        font-size: 15px;
    }
    .datos_header{
        font-size: 15px;
        text-align: right;
    }
    hr{
        margin-top: -10px;
        margin-bottom: 15px;
    }
    img{
        position: relative;
        right: 180px;
        top:70px;
        margin-top: -50px;
    }
    .datos_separador{
        margin-bottom: 25px;
    }
    .aviso{
        font-family: arial;
        margin-top: 100px;
        margin-bottom: -80px;
    }
    .cliente{
        font-family: arial;
        margin-top: 120px;
        margin-bottom: -80px;
    }
    .sep{
        margin-bottom: 52px;
    }
    .articulo p{
        font-size: 11px;
        margin-top: -7px;
    }

</style>
<script>
    function imprime(){
       // window.print();
    }
</script>
<body onload="imprime()">
<?php  $this->load->view('ventas/ticket/header');?>
<table>
    <thead>
    <th >#</th>
    <th><p class="datos">DESCRIPCIÓN</p></th>
    <th>CANT</th>
    <th>PREC</th>
    <th>SUB</th>
    </thead>
    <tbody>
<?php foreach ($articulos as $index => $articulo) :?>
    <tr class="articulo">
        <td align="center"><p><?= $index+1; ?></p></td>
        <td align="center"><p><?=$articulo['descripcion']  ?></p></td>
        <td align="center"><p><?= $articulo['cantidad'] ?></p></td>
        <td align="center"><p>$<?=$articulo['precio'] ?></p></td>
        <td align="center"><p>$<?=$articulo['subtotal']  ?></p></td>
    </tr>
<?php endforeach; ?>
    </tbody>
    <tfoot>
    </tfoot>
</table>
<table style="width:200px;   float:right;">
    <th>
        <tr></tr>
        <tr></tr>
    </th>
    <tr>
        <td  ><span>Total: </span> </td>
        <td > <b class="total">$<?= $venta[0]['total'] ?></b></td>
    </tr>
    <tr>
        <td ><span class="separador"></span></td>
    </tr>
    <tr>
        <td ><span><?php echo ($numero_abonos==1)?"Pago inicial":"Abono" ?>:</span> </td>
        <td > <b>$<?= $venta[0]['abono'] ?></b></td>
    </tr>
    <tr>
        <td  ><span>Saldo: </span></td>
        <td ><b>$<?= $venta[0]['saldo']?></b></td>
    </tr>
    <tr class="sep" height="5">
    </tr>
    <tr>
        <td  ><span>Pago: </span></td>
        <td ><b>$<?= $venta[0]['pago'] ?></b></td>
    </tr>
    <tr>
        <td  ><span>Cambio: </span></td>
        <td ><b>$<?= $venta[0]['cambio'] ?></b></td>
    </tr>
    <?php if ($venta[0]['estatus']): ?>
    <tr>
        <td  ><span>Nuevo saldo: </span></td>
        <td ><b>0.00</b></td>
    </tr>
    <?php endif; ?>
</table>
</table>
<p class="cliente">Cliente: <br><b><?= $venta[0]['cliente'] ?></b></p>
<?php if (!$venta[0]['estatus']) :?>
<p class="aviso">Cuenta con <b>30</b> dias para liquidar el saldo.
    <br>
    <br>
    Liquidar la cantidad de <b>$<?= $venta[0]['saldo']  ?></b> antes de   <b><?php $date  = date_create($venta[0]['fecha_vencimiento']); echo $date->format('d-m-Y');  ?></b>
</p>
<?php endif; ?>
<?php  $this->load->view('ventas/ticket/footer');?>
<p id="barcode"><?= $venta[0]['codigo']  ?></p>
</body>
</html>
