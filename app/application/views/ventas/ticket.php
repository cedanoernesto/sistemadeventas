<!doctype html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <meta charset="UTF-8">
    <title><?= $modulo; ?></title>
</head>
<style>
    @font-face {
        font-family: 'barcode';
        src: url(https://www.sistemadeventas.com.mx/public/fonts/barcode.ttf);
    }
    #barcode{
        margin-top: -10px;
        font-family: barcode;
        font-size: 100px;
        margin-bottom: -80px;
    }
    h1,h2,h3,h4,h5{
        font-family: arial;
    }
    label{
        font-family: arial;
    }
    h2,h3,h4,h5{
        margin-top: -15px;
    }
    table{
        width: 100%;
        font-family: Arial;
    }
    .separador{
        width: 250px;
    }
    .total{
        font-size: 18px;
    }
    .datos{
        font-size: 15px;
    }
    .datos_header{
        font-size: 15px;
        text-align: right;
    }
    hr{
        margin-top: -10px;
        margin-bottom: 15px;
    }
    img{
        position: relative;
        right: 180px;
        top:70px;
        margin-top: -50px;
    }
    .datos_separador{
        margin-bottom: 25px;
    }
    .aviso{
        font-family: arial;
        margin-top: 100px;
        margin-bottom: -80px;
    }
    .cliente{
        font-family: arial;
        margin-top: 120px;
        margin-bottom: -80px;
    }
    .sep{
        margin-bottom: 52px;
    }
    .articulo p{
        font-size: 11px;
        margin-top: -7px;
    }
    body{
        width:400px;
    }

</style>
<script>
    function imprime(){
        <?php if ($imprimir): ?>
        window.print();
        <?php endif; ?>
    }
</script>
<body onload="imprime()">
<?php  $this->load->view('ventas/ticket/header');?>
<table>
    <thead>
    <th >#</th>
    <th><p class="datos">DESCRIPCIÓN</p></th>
    <th>CANT</th>
    <th>PREC</th>
    <th>SUB</th>
    </thead>
    <tbody>
    <?php foreach ($detalles as $index => $articulo) :?>
        <tr class="articulo" >
            <td align="center"><p><?= $index+1; ?></p></td>
            <td align="center"><p><?=$articulo['descripcion']  ?></p></td>
            <td align="center"><p><?= $articulo['cantidad'] ?></p></td>
            <td align="center"><p><?=$articulo['precio'] ?></p></td>
            <td align="center"><b><p><?=$articulo['subtotal']  ?></p></b></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
    <tfoot>

    </tfoot>
</table>
<table style="width:200px;   float:right;">
    <th>
        <tr></tr>
        <tr></tr>
    </th>
    <tr>
        <td  ><span>Total: </span> </td>
        <td > <b class="total">$<?= $venta[0]['total'] ?></b></td>
    </tr>
    <tr>
        <td ><span class="separador"></span></td>
    </tr>
        <td  ><span>Pago: </span></td>
        <td ><b>$<?= $venta[0]['pago'] ?></b></td>
    </tr>
    <tr>
        <td  ><span>Cambio: </span></td>
        <td ><b>$<?= $venta[0]['cambio'] ?></b></td>
    </tr>
</table>
</table>
<?php  $this->load->view('ventas/ticket/footer');?>
</body>
</html>
