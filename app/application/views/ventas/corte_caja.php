<style>
    .titulo{
        font-weight: bold;
        font-size: 15px;
    }
    .titulo_final{
        font-weight: bold;
        font-size: 18px;
    }   .a_titulo_final{
        color:#3c87ff;
        font-weight: bold;
        font-size: 18px;
    }
    .valor{
           font-weight: bold;
           font-size: 18px;
       }
    .valor_ganancia{
        font-weight: bold;
        font-size: 18px;
        color: #22c360;
    }
    .valor_gastos{
        font-weight: bold;
        font-size: 18px;
        color:#aa0000;
    }
</style>
<script src="<?= base_url('public/js/components/utils.js') ?>"></script>
<script src="<?= base_url('public/js/components/webix_utils.js') ?>"></script>
<script src="<?= base_url('public/js/components/views/ventas/corte_caja.js') ?>"></script>
<script src="<?= base_url('public/js/components/views/ventas/functions_corte_caja.js') ?>"></script>
<script src="<?= base_url('public/js/highcharts/js/highcharts.js') ?>"></script>
<script src="<?= base_url('public/js/qz-tray/dependencies/rsvp-3.1.0.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/dependencies/sha-256.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/qz-tray.js')?>"></script>
<script>
    URL_PREFERENCIAS = BASE_URL +"ventas/corte_caja/preferencias";
    var obj_pref ={};
   // document.getElementById("pager").remove();

    webix.ready(function(){
        $$('generic_button_new').hide();
        webix.ui(window_corte_caja);
        webix.ui(window_detalle);
        webix.ui(window_gastos);
        webix.ui(window_salidas);
        webix.ui(window_multiple_corte);
        webix.ui(window_mostrar_multiple_corte_caja);
        webix.ui({
            container:'container',
            rows:[
                {
                    cols:[
                        {},
                        view_corte_caja,
                        {}
                    ]
                }
            ]
        });
        data();
        check();

        webix.ajax().sync().post(URL_PREFERENCIAS,{},function (response) {
            response = JSON.parse(response);
            obj_pref = response;
            if (!existValue(obj_pref.tipos_ventas,'id',3)){
                $$('cont_tipo_copia').hide();
                $$('cont_ganancia_copias').hide();
            }

            if (!existValue(obj_pref.tipos_ventas,'id',2))
                $$('cont_tipo_apartados').hide();
            if (!existValue(obj_pref.tipos_pagos,'id',2))
                $$('cont_pago_tarjeta').hide();
            if (!parseInt(response.corte))
                $$('cont_cortes').hide();
        });

        if ((obj_pref.tipos_pagos.length + obj_pref.tipos_ventas.length)<5)
            $$('cont_chart').hide();
        console.log(obj_pref.tipos_pagos);
        $(function () {
            $(document).ready(function () {
                // Build the chart
                $('#chart').highcharts({
                    chart: {
                        events:{
                            load:loadChart
                        },
                        plotBackgroundColor: null,
                        plotBorderWidth: null,
                        plotShadow: false,
                        type: 'pie'
                    },
                    title: {
                        text: 'Porcentaje por tipos de venta.'
                    },
                    tooltip: {
                        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                    },
                    plotOptions: {
                        pie: {
                            dataLabels: {
                                enabled: true,
                                distance: -50,
                                style: {
                                    fontWeight: 'bold',
                                    color: 'white',
                                    textShadow: '0px 1px 2px black'
                                }
                            },
                            allowPointSelect: true,
                            cursor: 'pointer',
                            dataLabels: {
                                enabled: false
                            },
                           showInLegend: true
                        }
                    },
                    series: []
                });
            });
        });
        qz.websocket.connect();
    });
</script>
