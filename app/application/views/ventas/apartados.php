<style type="text/css">
    .tipo_venta{
        font-weight: bold;
    // text-decoration: underline;
        cursor: pointer;
        font-size: 20px;
    }
    .precio{
        font-weight: bold;
        font-size: 20px;
    }
    .total{
        font-weight: bold;
        font-size: 30px;
    }
    .existencia{
        font-weight: bold;
        font-size: 20px;
    }
</style>
<script type="text/javascript"  charset="utf-8" src="<?= base_url('public/js/components/utils.js') ?>"></script>
<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/views/ventas/functions_ventas2.js') ?>"></script>
<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/views/ventas/ventas.js') ?>"></script>
<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/forms/ventas/functions_apartados.js') ?>"></script>
<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/forms/ventas/apartados.js') ?>"></script>
<script type="text/javascript"  charset="utf-8" src = "<?= base_url('public/js/components/forms/inventario/clientes.js') ?>"></script>
<script src="<?= base_url('public/js/qz-tray/dependencies/rsvp-3.1.0.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/dependencies/sha-256.min.js')?>"></script>
<script src="<?= base_url('public/js/qz-tray/qz-tray.js')?>"></script>
<script type="text/javascript">
    URL_PREFERENCIAS = BASE_URL +"ventas/ventas/preferencias";
    var obj_pref ={};

    document.addEventListener("keyup",function(event) {
        var key = event.keyCode;
        if (key == 27 && !$$('window_tipo_venta').isVisible()) {
            if ($$('id_articulo').getValue() != "" && $$('id_articulo').getValue() != 0) {
                mostrarArticulo($$('id_articulo').getValue());
                $$('id_articulo').setValue("");
            }
        }
        else if ($$('window_tipo_venta').isVisible() == true) {
            if (key == 49) {
                mostrarPrecio(1);
                if (use_cod)
                    add();
            }
            else if (key == 50) {
                mostrarPrecio(2);
                if (use_cod)
                    add();
            }
            else if (key == 27) {
                $$('window_tipo_venta').hide();
                // $$('id_articulo').setValue('0');
                $$('id_articulo').focus();
            }
        }
    });
    webix.ready(function(){
        webix.ajax().post(URL_PREFERENCIAS,{},function (response) {
            response = JSON.parse(response);
            obj_pref= formatObjPreferences(response[0]);
            $$('tipo_pago').define('options',obj_pref.tipos_pago);
            $$('tipo_pago').refresh();
            $$('tipo_pago_abonar').define('options',obj_pref.tipos_pago);
            $$('tipo_pago_abonar').refresh();
            if (obj_pref.descuentos.length>1){
                $$('id_descuento').define('options',obj_pref.descuentos);
                $$('id_descuento').refresh();
                obj_pref.descuentos.forEach(function(item) {
                    if (item.default == 1) {
                        $$('id_descuento').setValue(item.id)
                    }
                })
            }
            else{
                $$('id_descuento').hide();
            }


        });
        webix.ui(window_varios_apartados);
        webix.ui(window_apartados);
        form_clientes.on.onSubmit =save_cliente;
        var window_clientes ={
            view:'window',
            id:'window_clientes',
            head:{view:'toolbar',cols:[{view:'label',label:'Nuevo cliente',align:'center'},btn_close_window]},
            modal:true,
            position:'center',
            width:600,
            body:form_clientes
        };
        webix.ui(window_clientes);
        webix.ui(window_realizar_apartado);
        webix.ui(window_realizar_abono);
        webix.ui(window_comun);
        webix.ui(window_tipo_venta);
        webix.ui(window_abonos);
            $$('button_save_clientes').define('click',save_cliente);
            $$('generic_button_new').attachEvent("onItemClick",function(){
            $$('label_toolbar_header').define('label','Nuevo apartado');
            $$('label_toolbar_header').refresh();
            $$('window_apartados').show();
            $$('form_apartados').clear();
            $$('form_apartados').clearValidation();
            $$('id_cliente').focus();
                $$('datatable_ventas').clearAll();
                venta=[];
                calcularTotal();
                getPaquetes();
                actualizarArticulos();

        });
        webix.ui({
            view:"layout",
            container:"container",
            rows:[
                {
                    cols:[
                        {},
                        view_apartado,
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {
                            rows:[
                                {view:'label',label:'<b>Articulos del apartado</b>',align:'center'},
                                datatable_articulos
                            ]
                        },
                        {}
                    ]
                }
            ]
        });
        $$('codigo').focus();
        getPaquetes();
        function actualizarArticulos(){
            webix.ajax().post(URL_GET_ARTICULOS,{},function(response){
                response = JSON.parse(response);
                articulos = response;
                var articulosCombo =[];
                articulos.forEach(function(item,index){
                    articulosCombo.push({value:item.descripcion,id:item.id});
                });
                $$('id_articulo').define('options',articulosCombo);
                $$('id_articulo').refresh();
            });
        }
        actualizarArticulos();
        qz.websocket.connect()
    });
</script>
