<?php
class Index extends MY_Controller{

    function __construct(){
        parent::__construct();
        $this->verify();
    }
    public function index(){
           $data  = $this->session->userdata('log');
            $data['modulo'] = "Inicio";
            $this->load->view("header",$data);
            $this->load->view("index");
    }
    
}

