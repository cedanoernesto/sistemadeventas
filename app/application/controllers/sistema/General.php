<?php
class General extends MY_Controller{
  public function getEstados(){
    $this->json($this->unique_model->get_rows("id , nombre as value","sistema_estados"));
  }
  public function notificacion(){
    $id_negocio = $this->id_negocio();
    $this->json($this->unique_model->query("SELECT descripcion as value,estatus,fecha FROM sistema_notificaciones WHERE id_negocio=$id_negocio ORDER BY id desc LIMIT 10 "));
  }
  public function verNotificacion(){
    $this->unique_model->update("sistema_notificaciones",array('id_negocio' =>$this->id_negocio()),array('estatus' => 1));
  }
}
