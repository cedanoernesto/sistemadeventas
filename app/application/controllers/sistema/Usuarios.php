<?php
class Usuarios extends MY_Controller{

  public function __construct(){
    parent ::__construct();
      $this->verify();
      $this->access(10);
  }

  public function index(){
      $data = $this->session->userdata();
      $data['modulo'] ="Usuarios";
      $data['id_modulo'] ="5.10";
      $data['id_seccion'] =5;
      $this->log(10);
    $this->load->view('header',$data);
    $this->load->view('index');
    $this->load->view('sistema/usuarios');
  }

public function perfiles(){
  $this->json(
    $this->unique_model->get_rows_where("md5(id) as id, nombre as value","sistema_perfiles",[])
  );
}
public function save(){
    $session = $this->session->userdata('log');
    $post = $this->input->post();
    $post['id_negocio'] = $this->id_negocio();
  unset($post['repite_contrasena']);
    if (isset($post['usuario'])){
        $post['contrasena'] = md5($post['contrasena']);
        $post['id_perfil'] = $this->unique_model->get_id_md5("sistema_perfiles","id",$post['id_perfil'])['id'];
    }
  $this->unique_model->save("sistema_usuarios",$post);
}
  public function datatable(){
      $get =$this->input->get();
      $start =0;
      $count =15;
      if(isset($get['start'])){
          $start = $get['start'];
          $count = $get['count'];
      }
      $filter = (isset($get['filter']))?$get['filter']:[];
      $sort = (isset($get['sort']))?$get['sort']:[];
      /*****************************CONSULTA*********************************/
      $query = [
          "SELECT"=>["md5(sistema_usuarios.id) as id,sistema_usuarios.nombre,sistema_usuarios.usuario,sistema_estados.nombre as estatus,sistema_usuarios.estatus as estado,sistema_perfiles.nombre as nombre_perfil,md5(sistema_usuarios.id_perfil) as id_perfil"],
          "JOIN"=> [
              ['sistema_estados','sistema_estados.id=sistema_usuarios.estatus',''],
                    ['sistema_perfiles','sistema_perfiles.id=sistema_usuarios.id_perfil','']
          ],
          "FROM" =>["sistema_usuarios"],
          "LIKE" =>[['sistema_usuarios.estatus',1]],
          "WHERE" => [["sistema_usuarios.id_negocio",$this->id_negocio()]],
          "LIMIT" => [$count,$start],
          "ORDER BY" =>[],
          "RETURN" =>[]
      ];
      foreach ($filter as $index => $value) {
          if($value!=""){
              if($index=="estatus"){
                  $index = "sistema_usuarios.estatus";
                  unset($query["LIKE"][0]);
              }
              if ($index=="nombre")
                      $index="sistema_usuarios.nombre";
              else if ($index =="nombre_perfil")
                  $index = "sistema_usuarios.id_perfil";
              $query["LIKE"][]=[$index,$value];
          }
      }
      foreach ($sort as $index => $value) {
          $query["ORDER BY"]=[$index,$value];
      }
      $query2 = $query;
      unset($query2["LIMIT"]);
      array_push($query2["RETURN"],"num_rows");
      $num = $this->unique_model->get_query($query2);
      $result = $this->unique_model->get_query($query);
      $return = [
          'data' => $result,
          'total_count' =>$num,
          'pos' =>$start
      ];
      $this->json($return);
  }

}
