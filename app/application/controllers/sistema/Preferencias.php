<?php
class Preferencias extends MY_Controller{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $data = $this->session->userdata();
        $data['modulo'] = "Preferencias del sistema";
        $data['id_modulo'] =5.14;
        $data['id_seccion']=5;
        $this->log(14);
        $this->load->view("header",$data);
        $this->load->view("index");
        $this->load->view("sistema/preferencias");
    }
}
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 27/04/2016
 * Time: 17:16
 */