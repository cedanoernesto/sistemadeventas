<?php 
class Informacion extends MY_Controller{
    
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(11);
    }

    public function index(){
        $this->refresh_negocio();
        $data = $this->session->userdata();
        $data['modulo'] ="Información del negocio";
        $data['id_modulo'] ="5.11";
        $data['id_seccion'] =5;
        $this->log(11);
        $this->load->view('header',$data);
        $this->load->view('index');
        $this->load->view('sistema/informacion');
    }

    public function save(){
        $post = $this->input->post();
        $post['id'] = md5($this->id_negocio());
        $post['id_tema'] = $this->unique_model->get_id_md5("sistema_temas","id",$post['id_tema'])['id'];
        $this->unique_model->save("sistema_negocios",$post);
        $this->refresh_negocio();
    }
    public function getTemas(){
        $this->json($this->unique_model->get_rows_where("md5(id) as id, nombre as value","sistema_temas",array('estatus' => 1)));
    }

    public function informacion(){
        $this->json($this->negocio());
    }

    public function get_ext($cadena){
        $exp = explode(".",$cadena);
       return  $ext = end($exp);
    }
    public function do_upload(){
        $uniq = uniqid();
        $negocio = $this->negocio();
        $ext = $this->get_ext($_FILES['upload']['name']);
        $name = $negocio[0]['url'].'_logo'.'_'.$uniq.'.'.$ext;
        $config = array(
            'upload_path' => "./public/images/negocios",
            'allowed_types' => "gif|jpg|png|jpeg|",
            'overwrite' => TRUE,
            'max_size' => "2048000",
            'file_name' =>$name
            //'max_height' => "768",
           // 'max_width' => "1366"
        );
        $this->load->library('upload', $config);
        if($this->upload->do_upload('upload')){
            $this->unique_model->save("sistema_negocios",array('id' => md5($this->id_negocio()),'logo' => $name));
            $data = array('upload_data' => $this->upload->data());
            $this->json(array());
        }
        else {
            $error = array('error' => $this->upload->display_errors());
           // $this->load->view('file_view', $error);
        }
    }
}