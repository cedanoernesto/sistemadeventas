<?php
class Apartados_pendientes extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(3);
    }
    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Apartados pendientes";
        $data['id_seccion'] = 3;
        $data['id_modulo'] = 3.13;
        $this->log(3);
        $this->load->view("header",$data);
        $this->load->view("index",$data['modulo'] );
        $this->load->view("apartados/apartados_pendientes");
    }

    public function save(){
        $this->unique_model->save("ventas_apartados",$this->input->post());
    }

    public function datatable(){
        $get =$this->input->get();
        $start =0;
        $count =15;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort =array();
        if(isset($get['filter']))
            $filter =$get['filter'];
        if(isset($get['sort']))
            $sort = $get['sort'];
        /*****************************CONSULTA*********************************/
        $query = array(
            "SELECT"=>array("md5(ventas_apartados.id) as id,ventas_apartados.fecha,ventas_apartados.fecha_vencimiento_tolerancia,inventario_clientes.nombre,folio_apartado(ventas_apartados.id) as folio,ventas_apartados.fecha,ventas_apartados.total,ventas_apartados.saldo,ventas_apartados.fecha_vencimiento,sistema_estados.nombre as estatus,inventario_clientes.estatus as estado"),
            "JOIN"=> array(
                array('sistema_estados','sistema_estados.id=ventas_apartados.estatus',''),
                array('inventario_clientes','inventario_clientes.id=ventas_apartados.id_cliente',''),
            ),
            "FROM" =>array("ventas_apartados"),
            "LIKE" =>array(),
            "WHERE" => array(array('ventas_apartados.id_negocio',$this->id_negocio())),
            "LIMIT" => array($count,$start),
            "ORDER BY" =>array(),
            "RETURN" =>array()
        );
        foreach ($filter as $index => $value) {
            if($value!=""){
                if ($index=="nombre")
                    $index="inventario_clientes.nombre";
                if ($index=="folio")
                    $index="folio_apartado(ventas_apartados.id)";
                $query["LIKE"][]=array($index,$value);
            }
        }
        foreach ($sort as $index => $value) {
            $query["ORDER BY"]=array($index,$value);
        }
        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"],"num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        );
        $this->json($return);
    }
}
?>
