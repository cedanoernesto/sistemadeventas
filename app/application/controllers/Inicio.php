<?php
class Inicio extends MY_Controller{
    public function __construct(){
        parent::__construct();

    }
    public function index(){
        $this->verify();
        $data  = $this->session->userdata();
        $data['modulo'] = "Inicio";
        $this->load->view("header",$data);
        $this->load->view("index");
        //$this->log(11);
        if ($data['id_perfil']==1)
            $this->load->view("inicio_admin");
        else
            $this->load->view("inicio");
        $this->notificaciones_apartados();
    }

    public function ventas(){
        header("Access-Control-Allow-Origin: *");
        $post = $this->input->post();
        if ($post['tipo']==sha1('2')){
            $where="WEEK";
            $group ="DAYOFWEEK(ventas_ventas.fecha)";
            $tipo =2;
        }
        else if ($post['tipo']==sha1('3')){
            $where="MONTH";
            $group ="CEILING(DAY(ventas_ventas.fecha)/7)";
            $tipo =3;
        }
        else if ($post['tipo']==sha1('4')){
            $where="YEAR";
            $group ="MONTH(ventas_ventas.fecha)";
            $tipo =4;
        }
        $id_negocio = 1;
        $this->db->select("YEAR(NOW()) AS año,
                sistema_mes.nombre AS mes,
                CONCAT('Semana ', IF (CEILING(DAY(ventas_ventas.fecha)/7 )<1.1,1,CEILING(DAY(ventas_ventas.fecha)/7)),' de ', sistema_mes.nombre) AS semana,
                CONCAT(sistema_dias.nombre,' ',DAYOFMONTH(ventas_ventas.fecha),' de ',sistema_mes.nombre) AS dia,
                0 AS total_ventas,
                0 AS ganancia",false)
            ->join('sistema_mes','sistema_mes.id = MONTH(ventas_ventas.fecha)')
            ->join('sistema_dias','sistema_dias.id =DAYOFWEEK(ventas_ventas.fecha)')
            ->where("$where(ventas_ventas.fecha) = $where(NOW())")
            ->where("ventas_ventas.id_negocio = $id_negocio")
            ->group_by($group)
            ->limit(500);
        $ventas = $this->db->get('ventas_ventas')->result_array();
        $categorias = [];
        $_ventas =['name' =>'Ventas','data' => []];
        $_ganancias = ['name' =>'Ganancias/Perdidas','data' => []];
        foreach ($ventas as $venta){
            $venta['total_ventas'] = rand(500,4500);
            $venta['ganancia'] = rand(300,2500);
            if ($post['tipo']==sha1('2'))
                array_push($categorias,$venta['dia']);
            else if ($post['tipo']==sha1('3')){
                array_push($categorias,$venta['semana']);
                $venta['total_ventas']*=5;
                $venta['ganancia']*=3;
            }
            else if ($post['tipo']==sha1('4')){
                array_push($categorias,$venta['mes']);
                $venta['total_ventas']*=30;
                $venta['ganancia']*=15;
            }
            array_push($_ventas['data'],$venta['total_ventas']);
            array_push($_ganancias['data'],$venta['ganancia']);
        }
        $this->json(
            [
                'categorias' => $categorias,
                'ventas' => $_ventas,
                'ganancias' => $_ganancias
            ]
        );

    }


}
/**
 * Created by PhpStorm.
 * User: ernesto
 * Date: 03/12/2015
 * Time: 21:29
 */