<?php

class Paquetes extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->verify();
        $this->access(2);
    }

    public function index(){
        $data = $this->session->userdata();
        $data['modulo'] = "Paquetes";
        $data['id_seccion'] = 1;
        $data['id_modulo'] = 1.2;
        $this->log(2);
        $this->load->view("header", $data);
        $this->load->view("index", $data['modulo']);
        $this->load->view("inventario/paquetes");
    }

    public function save(){
        $post = $this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        foreach ($post as $index => $value) {
            //echo $index;
            if ($index == 'hora_inicio' ){
                $post[$index] = ($post[$index]=='')?'0000-01-01 00:00:00':$post[$index];
            }
            else if ($index == 'hora_fin'){
                $post[$index] = ($post[$index]=='')?'0000-01-01 23:59:59':$post[$index];
            }
            else{
                $post[$index] = ($post[$index]=='')?'0':$post[$index];
            }
        }
        if (isset($post['tipo_venta']))
        $post['tipo_venta'] = ($post['tipo_venta']==md5('1'))?1:2;
        if($post['request']==md5(1)){
            unset($post['request']);
            $post['id_articulo'] = $this->unique_model->get_id_md5("inventario_articulos", "id", $post['id_articulo'])['id'];
            if (!$this->unique_model->get_num_equals_id("inventario_paquetes", $post, $post['id'])){
                $this->unique_model->save("inventario_paquetes", $post);
            }

            else {
                $this->json(array('message' => 'Ya existe un paquete con esos datos', 'estatus' => 0));
            }
        }
        else if ($post['request']==md5(2)){
            unset($post['request']);
            $this->unique_model->save("inventario_paquetes", $post);
        }
    }

    public function datatable(){
        $get = $this->input->get();
        $start = 0;
        $count = 15;
        if (isset($get['start'])) {
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort = array();
        if (isset($get['filter']))
            $filter = $get['filter'];
        if (isset($get['sort']))
            $sort = $get['sort'];
        /*************CONSULTA***************/
        $query = array(
            "SELECT" => array("inventario_paquetes.*,inventario_tipo_cantidades.descripcion as nombre_tipo_cantidad,md5(inventario_paquetes.tipo_venta) tipo_venta,md5(inventario_paquetes.id_articulo) as id_articulo  ,md5(inventario_paquetes.id) as id,inventario_articulos.descripcion as nombre_articulo,inventario_paquetes.hora_inicio,inventario_paquetes.hora_fin,sistema_estados.nombre as estatus,inventario_paquetes.estatus as estado "),
            "FROM" => array("inventario_paquetes"),
            "JOIN" => array(
                array("inventario_articulos", "inventario_articulos.id=inventario_paquetes.id_articulo", ""),
                array("sistema_estados", "inventario_paquetes.estatus=sistema_estados.id", ""),
                array("inventario_tipo_cantidades","inventario_tipo_cantidades.id = inventario_paquetes.id_tipo_cantidad","")
            ),
            "LIKE" => array(),
           "WHERE" =>array(array('inventario_paquetes.id_negocio',$this->id_negocio()),array('inventario_paquetes.estatus',1)),
            "LIMIT" => array($count, $start),
            "ORDER BY" => array(),
            "RETURN" => array()
        );
        foreach ($filter as $index => $value) {
            if ($value != "") {
                if ($index == "estatus")
                    $index = "inventario_paquetes.estatus";
                if ($index == "nombre_articulo")
                    $index = "md5(inventario_articulos.id)";
                $query["LIKE"][] = array($index, $value);
            }
        }
        foreach ($sort as $index => $value) {
            $query["ORDER BY"] = array($index, $value);
        }
        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"], "num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' => $num,
            'pos' => $start
        );
        $this->unique_model->json($return);
    }

    public function selectArticulo(){
        $id_articulo = $this->input->post('id');
        $this->json($this->unique_model->query("SELECT inventario_articulos.precio_compra,IFNULL((inventario_articulos.precio_compra/inventario_articulos.cantidad_unidad),0) AS precio_unidad_compra, inventario_articulos.venta_unidad,inventario_tipo_venta.nombre AS tipo_venta,inventario_contenedor.nombre AS contenedor
            FROM inventario_articulos
            LEFT JOIN inventario_tipo_venta ON inventario_articulos.id_tipo_venta = inventario_tipo_venta.id
            LEFT JOIN inventario_contenedor ON inventario_articulos.id_contenedor = inventario_contenedor.id
            WHERE MD5(inventario_articulos.id) ='$id_articulo' "));
    }

    public function preferencias(){
        $this->json($this->unique_model->get_rows_where("maneja_hora","preferencias_paquetes",array('id_negocio' => $this->id_negocio(),'estatus' => 1)));
    }


}
