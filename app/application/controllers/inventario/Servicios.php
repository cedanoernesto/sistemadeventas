<?php
class Servicios extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(12);
    }
    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Servicios";
        $data['id_seccion'] = 1;
        $data['id_modulo'] = 1.12;
        $this->log(12);
        $this->load->view("header",$data);
        $this->load->view("index",$data['modulo'] );
        $this->load->view("inventario/servicios");
    }
    public function save(){
        $post =$this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        if (isset($post['estado']))
            unset($post['estado']);
        $post['comun'] =1;
        $this->unique_model->save("inventario_articulos",$post);
    }

    public function datatable(){
        $get =$this->input->get();
        $start =0;
        $count =15;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort =array();
        if(isset($get['filter']))
            $filter =$get['filter'];
        if(isset($get['sort']))
            $sort = $get['sort'];
        /*
        $this->db->select('inventario_articulos.descripcion,inventario_articulos.estatus as estado ,md5(inventario_articulos.id) as id,sistema_estados.nombre as estatus');
        $this->db->from('inventario_articulos');
        $this->db->join('sistema_estados','inventario_articulos.estatus=sistema_estados.id');
        $this->db->where(
            ['inventario_articulos.estatus',1],
            ['inventario_articulos.id_negocio',$this->id_negocio()],
            ['inventario_articulos.comun',1]
        );*/

        $query = [
            "SELECT"=>["inventario_articulos.descripcion,inventario_articulos.estatus as estado ,md5(inventario_articulos.id) as id,sistema_estados.nombre as estatus"],
            "FROM" =>["inventario_articulos"],
            "JOIN" => [
                ["sistema_estados","inventario_articulos.estatus=sistema_estados.id",""],
            ],
            "LIKE" =>[],
            "WHERE" => [['inventario_articulos.estatus',1],['inventario_articulos.id_negocio',$this->id_negocio()],['inventario_articulos.comun',1]],
            "LIMIT" => [$count,$start],
            "ORDER BY" =>[],
            "RETURN" =>[]
        ];
        if (isset($get['filter']['estatus'])){
            unset($query["WHERE"][0]);
        }
        foreach ($filter as $index => $value) {
            if($value!=""){
                if($index=="estatus"){
                    $index = "inventario_articulos.estatus";
                }
                if($index=="descripcion")
                    $index = "inventario_articulos.descripcion";
                $query["LIKE"][]=array($index,$value);
            }

        }
        foreach ($sort as $index => $value)
        $query["ORDER BY"]=array($index,$value);
        $query2 = $query;
        unset($query2["LIMIT"]);
         array_push($query2["RETURN"],"num_rows");
         $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        );
        $this->json($return);
    }
}
?>
