<?php
class Articulos extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(1);
    }
    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Articulos";
        $data['id_seccion'] = 1;
        $data['id_modulo'] = 1.1;
        $this->log(1);
        $this->load->view("header",$data);
        $this->load->view("index");
        $this->load->view("inventario/articulos");
    }

    /*public function getTiposVenta(){
        $this->json($this->unique_model->get_rows("id, nombre as value","inventario_tipo_venta"));
    }*/
    public function preferencias(){
        $response  = $this->unique_model->get_rows_where("*","preferencias_articulos",array('id_negocio' => $this->id_negocio()));
        unset($response[0]['id_negocio']);
        unset($response[0]['id']);
        $this->json($response);

    }
    public function getTiposVenta($id=0){
        $get = $this->input->get();
        $filter="";
        $limit = 5;
        if (isset($get['filter']['value']))
            $filter = $get['filter']['value'];
        $query = array(
            "SELECT" => array("md5(inventario_tipo_venta.id) as id,inventario_tipo_venta.nombre as value"),
            "FROM" => array("inventario_tipo_venta"),
            "JOIN" => array(
                array("inventario_tipo_venta_negocios","inventario_tipo_venta_negocios.id_tipo_venta = inventario_tipo_venta.id","")
            ),
            "WHERE" => array(
                array("inventario_tipo_venta_negocios.id_negocio",$this->id_negocio()),
                array("inventario_tipo_venta_negocios.estatus",1),
            ),
            "LIKE" => array(
                array("inventario_tipo_venta.nombre",$filter)
            )
        );
        $this->json($this->unique_model->get_query($query));
    }
    public function getProveedores($id=0){
        $get = $this->input->get();
        $filter="";
        $limit = 5;
        if (isset($get['filter']['value']))
            $filter = $get['filter']['value'];
        $query = array(
            "SELECT" => array("md5(id) as id,nombre as value"),
            "FROM" => array("compras_proveedores"),
            "WHERE" => array(
                array("id_negocio",$this->id_negocio()),
                array("	estatus",1),
            ),
            "LIKE" => array(
                array("nombre",$filter)
            )
        );
        $this->json($this->unique_model->get_query($query));
    }
    public function save(){
        $post =$this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        unset($post['value']);
        foreach ($post as $index => $value) {
            if ($index != 'codigo_barras'){
                $post[$index] = ($post[$index]=='')?0:$post[$index];
            }
        }
        $post['codigo_barras'] = trim($post['codigo_barras']);
        $us = (isset($post['unidades_sueltas'])?$post['unidades_sueltas']:0);
        if (isset($post['id_contenedor'] ))
            $post['id_contenedor']=$this->unique_model->get_id_md5("inventario_contenedor","id",$post['id_contenedor'])['id'];
        if (isset($post['id_categoria'] ))
            $post['id_categoria']=$this->unique_model->get_id_md5("inventario_categorias","id",$post['id_categoria'])['id'];
        if (isset($post['id_tipo_venta'] ))
            $post['id_tipo_venta']=$this->unique_model->get_id_md5("inventario_tipo_venta","id",$post['id_tipo_venta'])['id'];
        if (isset($post['id_proveedor'] ))
            $post['id_proveedor']=$this->unique_model->get_id_md5("compras_proveedores","id",$post['id_proveedor'])['id'];
        unset($post['unidades_sueltas']);
        $c=0;
        $d=0;
        $repeat =array();
        if ($post['id'] == "0") {
            $post['existencia_unidad']=($post['cantidad_unidad']*$post['existencia'])+$us;
            if ($post['codigo_barras'] != "") {
                $c = $this->unique_model->get_rows_where("id", "inventario_articulos", array('codigo_barras' => $post['codigo_barras'],'id_negocio' => $this->id_negocio(),'estatus' => 1));
                $c = count($c);
            }
            $d = $this->unique_model->get_rows_where("id", "inventario_articulos", array('descripcion' => $post['descripcion'],'id_negocio' => $this->id_negocio(),'estatus' => 1));
            $d = count($d);
        } else {
            if (isset($post['codigo_barras'])) {
                if ($post['codigo_barras'] != "") {
                    $c = $this->unique_model->get_num_equals_id("inventario_articulos", array('codigo_barras' => $post['codigo_barras'],'id_negocio' => $this->id_negocio(),'estatus' => 1), $post['id']);
                }
                $d = $this->unique_model->get_num_equals_id("inventario_articulos", array('descripcion' => $post['descripcion'],'id_negocio' => $this->id_negocio(),'estatus' => 1), $post['id']);
            }
        }
        if ($c>0)
            array_push($repeat,"Codigo de barras");
        if ($d>0)
            array_push($repeat,"Descripcion");
        $fields = implode(", ",$repeat);
        if(count($repeat)>0)
            $this->json(array('message' => "Ya existe un producto con estos campos: ".$fields,'estatus' =>0));
        else
            $id =$this->unique_model->save("inventario_articulos",$post);

    }
    public function getCantidades($id_articulo){
        $articulo=$this->unique_model->get_rows_where("existencia_unidad,cantidad_unidad","inventario_articulos",array('md5(id)' => $id_articulo))[0];
        $existencia=$articulo['existencia_unidad'];
        $cantidad_unidad=$articulo['cantidad_unidad'];
        $cantidad=$existencia/$cantidad_unidad;
        $cantidad_cerrada=floor($cantidad);
        $decimal= ($cantidad-$cantidad_cerrada)*$cantidad_unidad;
        $res = array();
        for($i=1; $i<=$cantidad_cerrada; $i++)
            array_push($res,array('cantidad' => $cantidad_unidad));
        if ($decimal!=0)
            array_push($res,array('cantidad' => $decimal));
        $this->json($res);
        //return $result;
    }

    public function getNumCantidades($id_articulo){
        $cant =array();
        $query = array(
            "SELECT" => array("cantidad"),
            "FROM" =>array("inventario_articulos_cantidades"),
            "WHERE" =>array(array("md5(id_articulo)",$id_articulo)),
            "GROUP BY"=> array("cantidad"),
            "ORDER BY" => array("cantidad","ASC")
        );
        $n =$this->unique_model->get_query($query);
        foreach($n as $index =>$value){
            $num=$this->unique_model->get_num_rows_where("inventario_articulos_cantidades",array("cantidad"=> $value['cantidad'],"md5(id_articulo)" => $id_articulo));
            $cant[]= array('cantidad' => $num,'piezas' => $value['cantidad']);
        }
        $this->json($cant);
    }

    public function saveCantidades(){
        $cantidades = json_decode($this->input->post()['cantidades'],true);
        foreach ($cantidades as $cantidad) {
            $this->unique_model->save("inventario_articulos_cantidades",$cantidad);
        }

    }

    public function selectContenedor(){
        $get = $this->input->get();
        $filter="";
        $limit = 5;
        if (isset($get['filter']['value']))
            $filter = $get['filter']['value'];
        $query =array(
            "SELECT" => array("md5(inventario_contenedor.id) as id, inventario_contenedor.nombre AS value"),
            "FROM" => array("inventario_contenedor"),
            "JOIN"=> array(
                array("inventario_contenedor_negocios","inventario_contenedor_negocios.id_contenedor = inventario_contenedor.id","")
            ),
            "LIKE" => array(array("nombre",$filter)),
            "WHERE" =>array(array("estatus",1),array('id_negocio',$this->id_negocio()))
        );
        $this->json($this->unique_model->get_query($query));
    }

    public function selectCategorias($id=0){
        $get = $this->input->get();
        $filter="";
        $limit = 5;
        if (isset($get['filter']['value']))
            $filter = $get['filter']['value'];
        $query =array(
            "SELECT" => array("md5(id) as id, nombre AS value"),
            "FROM" => array("inventario_categorias"),
            "LIMIT" =>array($limit),
            "LIKE" => array(array("nombre",$filter)),
            "WHERE" =>array(array("estatus",1),array('id_negocio',$this->id_negocio()))
        );
        $rows =$this->unique_model->get_query($query);
        if ($id!="0"){
            $query["WHERE"][]= array("md5(id)",$id);
            $row = $this->unique_model->get_query($query);
            array_push($rows,$row);
        }

        $this->json($rows);

    }

    public function select($id){
        $get = $this->input->get();
        $filter="";
        $limit = 5;
        if (isset($get['filter']['value']))
            $filter = $get['filter']['value'];
        $query =array(
            "SELECT" => array("md5(id) as id, descripcion AS value"),
            "FROM" => array("inventario_articulos"),
            //"LIMIT" =>array($limit),
            "LIKE" => array(array("descripcion",$filter)),
            "WHERE" =>array(array("estatus",1),array('inventario_articulos.id_negocio',$this->id_negocio()))
        );
        $rows =$this->unique_model->get_query($query);

        if ($id!="0"){
            $query["WHERE"][]= array("md5(inventario_articulos.id)",$id);
            $row = $this->unique_model->get_query($query);
            array_push($rows,$row);
        }
        $this->json($rows);

    }

    public function datatable(){
        $get =$this->input->get();
        $start =0;
        $count =15;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = (isset($get['filter']))?$get['filter']:[];
        $sort = (isset($get['sort']))?$get['sort']:[];
        /*****************************CONSULTA*********************************/
        $query = [
            "SELECT"=>["inventario_articulos.cantidad_mayoreo,inventario_articulos.cantidad_unidad,inventario_articulos.cantidad_unidad_mayoreo,inventario_articulos.codigo_barras,inventario_articulos.descripcion,inventario_articulos.estatus,inventario_articulos.existencia,inventario_articulos.existencia_minima,inventario_articulos.existencia_unidad,md5(inventario_articulos.id_categoria) as id_categoria,	md5(inventario_articulos.id_contenedor) as id_contenedor,md5(inventario_articulos.id_tipo_venta) as id_tipo_venta,inventario_articulos.precio_compra,inventario_articulos.precio_mayoreo,inventario_articulos.precio_unidad,inventario_articulos.precio_unidad_mayoreo,inventario_articulos.precio_venta,inventario_articulos.venta_unidad,md5(inventario_articulos.id_proveedor) as id_proveedor,inventario_articulos.estatus as estado ,md5(inventario_articulos.id) as id,inventario_tipo_venta.nombre as tipo_venta,sistema_estados.nombre as estatus"],
            "FROM" =>["inventario_articulos"],
            "JOIN" => [
                ["inventario_tipo_venta","inventario_articulos.id_tipo_venta=inventario_tipo_venta.id",""],
                ["sistema_estados","inventario_articulos.estatus=sistema_estados.id",""],
            ],
            "LIKE" =>[],
            "WHERE" => [['inventario_articulos.id_negocio',$this->id_negocio()],['inventario_articulos.comun',0]],
            "LIMIT" => [$count,$start],
            "ORDER BY" =>[],
            "RETURN" =>[]
        ];
        foreach ($filter as $index => $value) {
            if($value!=""){
                if($index=="estatus")
                    $index = "inventario_articulos.estatus";
                if($index=="descripcion")
                    $index = "inventario_articulos.descripcion";
                if($index=="tipo_venta"){
                    $index = "inventario_tipo_venta.nombre";
                }
                $query["LIKE"][]=[$index,$value];
            }
        }
        foreach ($sort as $index => $value){
            $query["ORDER BY"]=[$index,$value];
        }
        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"],"num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = [
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        ];
        $this->json($return);
    }
}

