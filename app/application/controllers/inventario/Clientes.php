<?php
class Clientes extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(3);
    }
    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Clientes";
        $data['id_seccion'] = 1;
        $data['id_modulo'] = 1.3;
        $this->log(3);
        $this->load->view("header",$data);
        $this->load->view("index",$data['modulo'] );
        $this->load->view("inventario/clientes");
    }
    public function save(){
        $post =$this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        if (isset($post['estado']))
            unset($post['estado']);
        $this->unique_model->save("inventario_clientes",$post);
    }
    public function selectClientes(){
        $get = $this->input->get();
        $filter="";
        $limit = 5;
        if (isset($get['filter']['value']))
            $filter = $get['filter']['value'];
        $query =array(
            "SELECT" => array("md5(id) as id, nombre AS value"),
            "FROM" => array("inventario_clientes"),
            "LIKE" => array(array("nombre",$filter)),
            "WHERE" =>array(array("estatus",1),array('id_negocio',$this->id_negocio())),
        );
        $this->json($this->unique_model->get_query($query));
    }
    public function datatable(){
        $get =$this->input->get();
        $start =0;
        $count =15;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort =array();
        if(isset($get['filter']))
            $filter =$get['filter'];
        if(isset($get['sort']))
            $sort = $get['sort'];
        /*****************************CONSULTA*********************************/
        $query = array(
            "SELECT"=>array("md5(inventario_clientes.id) as id,inventario_clientes.rfc,inventario_clientes.nombre,inventario_clientes.telefono,inventario_clientes.correo_electronico,inventario_clientes.domicilio,sistema_estados.nombre as estatus,inventario_clientes.estatus as estado"),
            "JOIN"=> array(
                array('sistema_estados','sistema_estados.id=inventario_clientes.estatus','')
            ),
            "FROM" =>array("inventario_clientes"),
            "LIKE" =>array(),
            "WHERE" => array(array('inventario_clientes.id_negocio',$this->id_negocio())),
            "LIMIT" => array($count,$start),
            "ORDER BY" =>array(),
            "RETURN" =>array()
        );
        foreach ($filter as $index => $value) {
            if($value!=""){
                if ($index=="nombre")
                        $index="inventario_clientes.nombre";
                $query["LIKE"][]=array($index,$value);
            }
        }
        foreach ($sort as $index => $value) {
            $query["ORDER BY"]=array($index,$value);
        }
        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"],"num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        );
        $this->json($return);
    }
}
?>
