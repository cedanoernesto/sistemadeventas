<?php
class categorias extends MY_Controller{
    public function __construct(){
        parent::__construct();
    }
    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Categorias";
        $data['id_seccion'] = 1;
        $data['id_modulo'] = 1.15;
        $this->log(15);
        $this->load->view("header",$data);
        $this->load->view("index",$data['modulo'] );
        $this->load->view("inventario/categorias");
    }

  
    public function save(){
        $post =$this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        if (isset($post['estado']))
            unset($post['estado']);
        $this->unique_model->save("inventario_categorias",$post);
    }
    public function datatable(){
        $get =$this->input->get();
        $start =0;
        $count =15;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort =array();
        if(isset($get['filter']))
            $filter =$get['filter'];
        if(isset($get['sort']))
            $sort = $get['sort'];
        /*****************************CONSULTA*********************************/
        $query = array(
            "SELECT"=>array("md5(inventario_categorias.id) as id,inventario_categorias.nombre,,sistema_estados.nombre as estatus,inventario_categorias.estatus as estado"),
            "JOIN"=> array(
                array('sistema_estados','sistema_estados.id=inventario_categorias.estatus','')
            ),
            "FROM" =>array("inventario_categorias"),
            "LIKE" =>array(),
            "WHERE" => array(array('inventario_categorias.id_negocio',$this->id_negocio())),
            "LIMIT" => array($count,$start),
            "ORDER BY" =>array(),
            "RETURN" =>array()
        );
        foreach ($filter as $index => $value) {
            if($value!=""){
                if ($index=="nombre")
                    $index="inventario_categorias.nombre";
                $query["LIKE"][]=array($index,$value);
            }
        }
        foreach ($sort as $index => $value) {
            $query["ORDER BY"]=array($index,$value);
        }
        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"],"num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        );
        $this->json($return);
    }

}