<?php
 class compras extends MY_Controller{
     public function index(){
         $data  = $this->session->userdata();
         $data['modulo'] = "Compra de articulos";
         $data['id_seccion'] = 2;
         $data['id_modulo'] = 2.6;
         $this->log(6);
         $this->load->view("header",$data);
         $this->load->view("index",$data['modulo'] );
         $this->load->view("compras/compras");
     }

     public function save(){
         $post = $this->input->post();
         $post['id_articulo']  = $this->unique_model->get_id_md5("inventario_articulos","id",$post['id_articulo'])['id'];
         $data = array(
           'existencia' =>$this->unique_model->get_rows_where("existencia","inventario_articulos",array('id' => $post['id_articulo']))[0]['existencia'],
           'existencia_unidad' =>  $this->unique_model->get_rows_where("existencia_unidad","inventario_articulos",array('id' => $post['id_articulo']))[0]['existencia_unidad'],
           'cantidad_unidad' =>$this->unique_model->get_rows_where("cantidad_unidad","inventario_articulos",array('id' => $post['id_articulo']))[0]['cantidad_unidad'],
           'precio_compra' =>$this->unique_model->get_rows_where("precio_compra","inventario_articulos",array('id' => $post['id_articulo']))[0]['precio_compra']
         );
         $post['total'] = $post['cantidad']*$post['precio_compra'];
         $post['id']=0;
         $post['id_negocio'] = $this->id_negocio();
         $data['existencia']+=$post['cantidad'];
         if ($post['venta_unidad']){
             $data['existencia_unidad']+= ($data['cantidad_unidad']*$post['cantidad']);
         }
         $this->unique_model->save("inventario_articulos",
             array(
                 'id'=> md5($post['id_articulo']),
                 'existencia' => $data['existencia'],
                 'existencia_unidad' => $data['existencia_unidad'],
                 'precio_compra' => $post['precio_compra'],
                 'precio_venta' => $post['precio_venta'],
                 'precio_mayoreo' => $post['precio_mayoreo'],
                 'id_negocio' => $this->id_negocio()
             )
         );
         unset($post['venta_unidad']);
         unset($post['precio_venta']);
         unset($post['precio_mayoreo']);
         $this->unique_model->save("compras_compras",$post);
     }
}
