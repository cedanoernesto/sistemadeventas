<?php
class Reporte extends MY_Controller{
    public function __construct(){
        parent::__construct();
    }

    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Reporte de compras";
        $data['id_seccion'] = 2;
        $data['id_modulo'] = 2.15;
        $this->log(15);
        $this->load->view("header",$data);
        $this->load->view("index",$data['modulo'] );
        $this->load->view("compras/reporte");
    }

    public function detalle($tipo,$par=0){
        if($tipo==1){
            $where ="compras_compras.id=$par";
        }
        else if ($tipo==2){
             $where ="DATE(compras_compras.fecha)=DATE('$par')";
        }
        else if ($tipo==3){
            $where ="WEEK(compras_compras.fecha)=WEEK('$par')";
        }
        $query =array(
          "SELECT" => array("inventario_articulos.descripcion,compras_compras.precio_compra,compras_compras.cantidad,compras_compras.total"),
            "FROM" =>array("compras_compras"),
            "JOIN" => array(
                array("inventario_articulos","inventario_articulos.id = compras_compras.id_articulo","")
            ),
            "WHERE" =>array(
                array($where),
                array("compras_compras.estatus",1)
            )
        );
        $this->json($this->unique_model->get_query($query));

    }
    public function datatable($tipo){
        $get =$this->input->get();
        if($tipo=='1'){
            $where="DATE";
            $field="CONCAT(HOUR(compras_compras.fecha),':',MINUTE(compras_compras.fecha),':',SECOND(compras_compras.fecha),' ',sistema_dias.nombre,' ',DAYOFMONTH(compras_compras.fecha),' de ',sistema_mes.nombre,' del ',YEAR(compras_compras.fecha))";
            $group="compras_compras.id";
        }
        else if ($tipo=='2'){
            $where="WEEK";
            $group ="DAYOFWEEK(compras_compras.fecha)";
            $field="CONCAT(sistema_dias.nombre,' ',DAYOFMONTH(compras_compras.fecha),' de ',sistema_mes.nombre,' del ',YEAR(compras_compras.fecha))";
        }
        else if ($tipo=='3'){
            $where="MONTH";
            $group ="CEILING(DAY(compras_compras.fecha)/7 )";
            $field ="CONCAT('Semana ', IF (CEILING(DAY(compras_compras.fecha)/7 )<1.1,1,CEILING(DAY(compras_compras.fecha)/7)),' de ', sistema_mes.nombre)";
        }
        else if ($tipo==sha1('4')){
            $where="YEAR";
            $group ="MONTH";
            $tipo =4;
            $flag_tipo=true;
        }
        $query=array(
            "SELECT" => array("
            compras_compras.id,
            SUM(compras_compras.total) AS total,
            DATE(compras_compras.fecha) AS _fecha,
            $tipo as tipo,
            $field  AS fecha,
            sistema_mes.nombre AS mes"),
            "FROM" => array("compras_compras"),
            "JOIN" => array(
                array("sistema_dias","sistema_dias.id= DAYOFWEEK(compras_compras.fecha)",""),
                array("sistema_mes","sistema_mes.id= MONTH(compras_compras.fecha)","")
            ),
            "WHERE" =>array(
                array($where."(compras_compras.fecha) = $where(NOW())"),
                array("compras_compras.estatus",1)
            ),
            "GROUP BY" =>array($group),
            "ORDER BY" => array("compras_compras.id","ASC")

        );


        /*****************************CONSULTA*********************************/
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
           // 'total_count' =>$num,
            //'pos' =>$start
        );
        $this->json($return);
    }
}