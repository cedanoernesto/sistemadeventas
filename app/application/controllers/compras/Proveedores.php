<?php
class Proveedores extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(4);
    }
    public function index(){
        $data  = $this->session->userdata();
        $data['modulo'] = "Proveedores";
        $data['id_seccion'] = 2;
        $data['id_modulo'] = 2.4;
        $this->log(4);
        $this->load->view("header",$data);
        $this->load->view("index",$data['modulo'] );
        $this->load->view("compras/proveedores");
    }
    public function save(){
        $post =$this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        $this->unique_model->save("compras_proveedores",$post);
    }
    public function datatable(){
        $get =$this->input->get();
        $start =0;
        $count =10;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort =array();
        if(isset($get['filter']))
            $filter =$get['filter'];
        if(isset($get['sort']))
            $sort = $get['sort'];
        /*****************************CONSULTA*********************************/
        $query = array(
            "SELECT"=>array("md5(compras_proveedores.id) as id,compras_proveedores.rfc,,compras_proveedores.nombre,compras_proveedores.telefono,compras_proveedores.correo_electronico,compras_proveedores.domicilio,sistema_estados.nombre as estatus,compras_proveedores.estatus as estado"),
            "FROM" =>array("compras_proveedores"),
            "JOIN"=>array(
                array("sistema_estados","sistema_estados.id=compras_proveedores.estatus",""),
            ),
            "LIKE" =>array(),
            "WHERE" => array(array('compras_proveedores.id_negocio',$this->id_negocio())),
            "LIMIT" => array($count,$start),
            "ORDER BY" =>array(),
            "RETURN" =>array()
        );
        foreach ($filter as $index => $value) {
            if($value!=""){
                if ($index == "nombre")
                    $index = "compras_proveedores.nombre";
                $query["LIKE"][]=array($index,$value);
            }
        }
        foreach ($sort as $index => $value) {
            $query["ORDER BY"]=array($index,$value);
        }
        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"],"num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        );
        $this->json($return);
    }
}
?>
