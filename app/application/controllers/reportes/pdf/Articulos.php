<?php
class Articulos extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(8);
    }

    public function articulos(){
        $id_negocio = $this->id_negocio();
        return $this->unique_model->query("SELECT * FROM inventario_articulos WHERE existencia<= existencia_minima AND id_negocio = $id_negocio");
    }

    public function index(){
        $negocio= $this->negocio();
        $data = $this->session->userdata();
        $articulos = $this->articulos();
        $this->load->library('fpdf');
        ob_end_clean();
        $this->pdf=new FPDF('P','mm','A4');
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $this->pdf->SetTitle("Reporte de articulos");
        $this->pdf->SetKeywords($isUTF8=true);
        $this->pdf->Image('public/images/negocios/'.$negocio[0]['logo'],15,8,33);
        $this->pdf->SetFont('Arial','B',20);
        $this->pdf->Cell(30);
        $this->pdf->Cell(140,10,$negocio[0]['nombre_negocio'],0,0,'C');
        $this->pdf->SetFont('Arial','',15);
        $this->pdf->Cell(-150,23,'Reporte de articulos en baja existencia',0,0,'C');
        $this->pdf->Ln(13);
        $this->pdf->SetFont('Arial','B',8);
        $this->pdf->Cell(135,10,iconv('UTF-8', 'ISO-8859-2', 'Fecha de emisión: ').date('d-m-Y H:i:s'),0,'L','C');
        $usuario = $this->unique_model->get_rows_where("nombre","sistema_usuarios",array('id' => $data['id']))[0]['nombre'];
        $this->pdf->Cell(1,10,"Emisor: ".$usuario." ",0,'L','C');
        $this->pdf->Cell(-157,20,"Cantidad de articulos: ".count($articulos),0,'L','C');
        //$this->pdf->Ln(4);
        $this->pdf->Ln(20);
        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetRightMargin(10);
        //$color=0,0,102;
        $this->pdf->SetFont('Arial','B', 10);
        //Seleccion de colores
        $this->pdf->SetFillColor(153, 204, 255);
        $this->pdf->SetDrawColor(153, 204, 255);
        $this->pdf->SetTextColor(10, 10, 10);
        $this->pdf->SetFont('Arial','B', 7);
        $this->pdf->Cell(5,7,'#','TBL',0,'C','1');
        $this->pdf->Cell(105,7,iconv('UTF-8', 'ISO-8859-2', 'Descripción'),'TBL',0,'C','1');
        $this->pdf->Cell(30,7,'Existencia minima','TBL',0,'C','1');
        $this->pdf->Cell(25,7,'Existencia','TBL',0,'C','1');
        $this->pdf->Cell(25,7,'Comprar','TBL',0,'C','1');
        $this->pdf->Ln(7);
        $c=0;
        foreach ($articulos as $articulo) {
            $this->pdf->SetTextColor(10, 10, 10);
            $c++;
            $this->pdf->Cell(5,5,$c,'BRL',0,'C',0);
            $this->pdf->Cell(105,5,$articulo['descripcion'],'BRL',0,'C',0);
            $this->pdf->Cell(30,5,$articulo['existencia_minima'],'BRL',0,'C',0);
            $this->pdf->Cell(25,5,$articulo['existencia'],'BRL',0,'C',0);
            $this->pdf->Cell(25,5,'','BRL',0,'C',0);
            $this->pdf->SetTextColor(10, 10, 10);
            //Se agrega un salto de linea
            $this->pdf->Ln(5);
        }
        $this->pdf->SetY(179);
        $this->pdf->SetFont('Arial','I', 8);
        $this->pdf->Cell(0,10,'Pagina '.$this->pdf->PageNo().'/{nb}',0,0,'C');
        $this->pdf->Output("Reporte de articulos".".pdf",'I');
    }
}