<?php
Class Ventas2 extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(8);
    }
    public function exists($array,$id){
        $flag = false;
        foreach ($array as $item =>$value){
            if ($value['id'] == $id)
                $flag=true;

        }
        return $flag;
    }

    public function preferencias(){
        $preferencias =array();
        $preferencias['corte'] = $this->unique_model->get_rows_where("maneja_multiple_corte","preferencias_corte_caja",array('id_negocio' => $this->id_negocio()))[0]['maneja_multiple_corte'];
        $preferencias['tipos_ventas']= $this->unique_model->get_rows_where("id_tipo_venta as id","ventas_tipo_venta_negocios",array('id_negocio'=> $this->id_negocio(), 'estatus' => 1));
        $preferencias['tipos_pagos']= $this->unique_model->get_rows_where("id_tipo_pago as id","ventas_tipo_pago_negocios",array('id_negocio'=> $this->id_negocio(), 'estatus' => 1));
        return $preferencias;
    }



    public function data($tipo,$_group,$fecha_inicio,$fecha_fin){
        $id_negocio = $this->id_negocio();
        $flag_tipo =false;
        /*
        SELECT YEAR(NOW()) AS año,sistema_mes.nombre AS mes,WEEK(ventas_ventas.fecha)-((MONTH(ventas_ventas.fecha)-1)*4 )AS semana,sistema_dias.nombre AS dia,calcular_total(2,ventas_ventas.fecha) AS total_ventas
        FROM ventas_ventas
        JOIN sistema_mes ON sistema_mes.id = MONTH(ventas_ventas.fecha)
        JOIN sistema_dias ON sistema_dias.id =DAYOFWEEK(ventas_ventas.fecha)
        WHERE WEEK(ventas_ventas.fecha) = WEEK(NOW())
        GROUP BY WEEK(ventas_ventas.fecha)
        */



        if($tipo==sha1('1')){
            $where="DATE";
            $flag_tipo=true;
        }
        else if ($tipo==sha1('2')){
            $where="WEEK";
            $group ="DAYOFWEEK";
            $tipo =2;
            $flag_tipo=true;
        }
        else if ($tipo==sha1('3')){
            $where="MONTH";
            $group ="WEEK";
            $tipo =3;
            $flag_tipo=true;
        }
        else if ($tipo==sha1('4')){
            $where="YEAR";
            $group ="MONTH";
            $tipo =4;
            $flag_tipo=true;
        }



        if($flag_tipo){
            if ($tipo == sha1('1')) {
                $ventas_categorias =$this->unique_model->query("
                SELECT inventario_categorias.nombre AS categoria,SUM(ventas_ventas.total) AS total,IFNULL(SUM(ventas_ventas_detalles.subtotal)-SUM(inventario_articulos.precio_compra*ventas_ventas_detalles.cantidad),0) AS ganancia
                FROM ventas_ventas
                JOIN ventas_ventas_detalles ON ventas_ventas_detalles.id_venta =ventas_ventas.id
                JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
                JOIN inventario_categorias  ON inventario_categorias.id =  inventario_articulos.id_categoria
                WHERE $where(ventas_ventas.fecha) = $where(NOW()) AND ventas_ventas.id_negocio= $id_negocio
                GROUP BY inventario_categorias.id
                ");
                $ventas = $this->unique_model->query("SELECT folio_venta(ventas_ventas.id) as folio,ventas_tipos.nombre as tipo,ventas_ventas.fecha,ventas_ventas.total,IF(ventas_ventas.tipo_pago=1,'Efectivo','Tarjeta') as tipo_pago
              FROM ventas_ventas
              JOIN ventas_tipos ON ventas_tipos.id= ventas_ventas.tipo
              WHERE $where(ventas_ventas.fecha) = $where(now()) AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1
              ORDER BY ventas_ventas.id");
            } else if ($tipo != sha1('5')) {
                $ventas = $this->unique_model->query("
                SELECT calcular_gastos($tipo,ventas_ventas.fecha,$id_negocio) as gastos, YEAR(NOW()) AS año,sistema_mes.nombre AS mes,concat('semana ', WEEK(ventas_ventas.fecha)-((MONTH(ventas_ventas.fecha)-1)*4 ),' de ',sistema_mes.nombre) AS semana,CONCAT(sistema_dias.nombre,' ',DAYOFMONTH(ventas_ventas.fecha),' de ',sistema_mes.nombre) AS dia,calcular_total($tipo,ventas_ventas.fecha,$id_negocio) AS total_ventas,calcular_ganancia($tipo,ventas_ventas.fecha,$id_negocio) AS ganancia
                FROM ventas_ventas
                JOIN sistema_mes ON sistema_mes.id = MONTH(ventas_ventas.fecha)
                JOIN sistema_dias ON sistema_dias.id =DAYOFWEEK(ventas_ventas.fecha)
                WHERE $where(ventas_ventas.fecha) = $where(NOW()) AND ventas_ventas.id_negocio = 2  AND ventas_ventas.estatus=1
                GROUP BY $group(ventas_ventas.fecha)");
                $ventas_categorias =$this->unique_model->query("
                SELECT inventario_categorias.nombre AS categoria,SUM(ventas_ventas.total) AS total,IFNULL(SUM(ventas_ventas_detalles.subtotal)-SUM(inventario_articulos.precio_compra*ventas_ventas_detalles.cantidad),0) AS ganancia
                FROM ventas_ventas
                JOIN ventas_ventas_detalles ON ventas_ventas_detalles.id_venta =ventas_ventas.id
                JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
                JOIN inventario_categorias  ON inventario_categorias.id =  inventario_articulos.id_categoria
                WHERE $where(ventas_ventas.fecha) = $where(NOW()) AND ventas_ventas.id_negocio= $id_negocio
                GROUP BY inventario_categorias.id
                ");
            }
            $ganancia_cantidad = $this->unique_model->query("
              SELECT SUM(ventas_ventas_detalles.subtotal)- (SUM(inventario_articulos.precio_compra/inventario_articulos.cantidad_unidad)*ventas_ventas_detalles.cantidad) AS ganancia
              FROM ventas_ventas_detalles
              JOIN ventas_ventas ON ventas_ventas.id = ventas_ventas_detalles.id_venta
              JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
              WHERE ventas_ventas_detalles.tipo_venta=1 AND $where(ventas_ventas.fecha) = $where(now()) AND ventas_ventas.id_negocio = $id_negocio");
            $ganancia_normal = $this->unique_model->query("
            SELECT sum(ventas_ventas_detalles.subtotal)-sum(inventario_articulos.precio_compra*ventas_ventas_detalles.cantidad) AS ganancia
            FROM ventas_ventas_detalles
            JOIN ventas_ventas ON ventas_ventas.id = ventas_ventas_detalles.id_venta
            JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
            WHERE ventas_ventas_detalles.tipo_venta=2 AND $where(ventas_ventas.fecha) = $where(now()) AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1");
            $ganancia_copias = $this->unique_model->query("SELECT IFNULL(sum(ventas_ventas.total),0) AS ganancia
            FROM ventas_ventas
            WHERE ventas_ventas.tipo=3 AND $where(ventas_ventas.fecha) = $where(now()) AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1");
        }
        else{
            if ($_group==sha1('2')){
                $group ="DAYOFWEEK";
                $tipo =2;
            }

            else if ($_group==sha1('3')){
                $group ="WEEK";
                $tipo =3;
            }

            else if ($_group==sha1('4')){
                $group ="MONTH";
                $tipo =4;
            }
            $ventas_categorias =$this->unique_model->query("
                SELECT inventario_categorias.nombre AS categoria,SUM(ventas_ventas.total) AS total,IFNULL(SUM(ventas_ventas_detalles.subtotal)-SUM(inventario_articulos.precio_compra*ventas_ventas_detalles.cantidad),0) AS ganancia
                FROM ventas_ventas
                JOIN ventas_ventas_detalles ON ventas_ventas_detalles.id_venta =ventas_ventas.id
                JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
                JOIN inventario_categorias  ON inventario_categorias.id =  inventario_articulos.id_categoria
                WHERE DATE(ventas_ventas.fecha) >= '$fecha_inicio' AND DATE(ventas_ventas.fecha) <= '$fecha_fin' AND ventas_ventas.id_negocio= $id_negocio
                GROUP BY inventario_categorias.id
                ");
            if ($_group == sha1('1')) {
                $ventas = $this->unique_model->query("SELECT folio_venta(ventas_ventas.id) as folio,ventas_tipos.nombre as tipo,ventas_ventas.fecha,ventas_ventas.total,IF(ventas_ventas.tipo_pago=1,'Efectivo','Tarjeta') as tipo_pago
              FROM ventas_ventas
              JOIN ventas_tipos ON ventas_tipos.id= ventas_ventas.tipo
              WHERE  ventas_ventas.fecha >= '$fecha_inicio' AND ventas_ventas.fecha<= '$fecha_fin' AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1
              ORDER BY ventas_ventas.id");
            } else{
                $ventas = $this->unique_model->query("
                SELECT calcular_gastos($tipo,ventas_ventas.fecha,$id_negocio) as gastos, YEAR(NOW()) AS año,sistema_mes.nombre AS mes,concat('semana ', WEEK(ventas_ventas.fecha)-((MONTH(ventas_ventas.fecha)-1)*4 ),' de ',sistema_mes.nombre) AS semana,CONCAT(sistema_dias.nombre,' ',DAYOFMONTH(ventas_ventas.fecha),' de ',sistema_mes.nombre) AS dia,calcular_total($tipo,ventas_ventas.fecha,$id_negocio) AS total_ventas,calcular_ganancia($tipo,ventas_ventas.fecha,$id_negocio) AS ganancia
                FROM ventas_ventas
                JOIN sistema_mes ON sistema_mes.id = MONTH(ventas_ventas.fecha)
                JOIN sistema_dias ON sistema_dias.id =DAYOFWEEK(ventas_ventas.fecha)
                WHERE DATE(ventas_ventas.fecha) >= '$fecha_inicio' AND DATE(ventas_ventas.fecha) <= '$fecha_fin' AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1
                GROUP BY $group(ventas_ventas.fecha)");
            }

            $ganancia_cantidad = $this->unique_model->query("
              SELECT SUM(ventas_ventas_detalles.subtotal)- (SUM(inventario_articulos.precio_compra/inventario_articulos.cantidad_unidad)*SUM(ventas_ventas_detalles.cantidad)) AS ganancia
              FROM ventas_ventas_detalles
              JOIN ventas_ventas ON ventas_ventas.id = ventas_ventas_detalles.id_venta
              JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
              WHERE ventas_ventas_detalles.tipo_venta=1 AND DATE(ventas_ventas.fecha) >= '$fecha_inicio' AND DATE(ventas_ventas.fecha) <= '$fecha_fin' AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1");
            $ganancia_normal = $this->unique_model->query("
            SELECT sum(ventas_ventas_detalles.subtotal)-sum(inventario_articulos.precio_compra*ventas_ventas_detalles.cantidad) AS ganancia
            FROM ventas_ventas_detalles
            JOIN ventas_ventas ON ventas_ventas.id = ventas_ventas_detalles.id_venta
            JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
            WHERE ventas_ventas_detalles.tipo_venta=2 AND DATE(ventas_ventas.fecha) >= '$fecha_inicio' AND DATE(ventas_ventas.fecha) <= '$fecha_fin' AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1");
            $ganancia_copias = $this->unique_model->query("SELECT IFNULL(sum(ventas_ventas.total),0) AS ganancia
            FROM ventas_ventas
            WHERE ventas_ventas.tipo=3 AND DATE(ventas_ventas.fecha) >= DATE('$fecha_inicio') AND DATE(ventas_ventas.fecha) <= DATE('$fecha_fin') AND ventas_ventas.id_negocio = $id_negocio  AND ventas_ventas.estatus=1");
        }
        $ganancia_ventas = $ganancia_cantidad[0]['ganancia'] + $ganancia_normal[0]['ganancia'];
        $data = array(
            'numero_ventas'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo' =>1,'tipo_pago'=>1),sha1($tipo),$fecha_inicio,$fecha_fin),
            'numero_copias'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo' =>3),sha1($tipo),$fecha_inicio,$fecha_fin),
            'numero_apartados'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo' =>2,'tipo_pago'=> 1),sha1($tipo),$fecha_inicio,$fecha_fin),
            'numero_tarjeta'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo_pago'=>2),sha1($tipo),$fecha_inicio,$fecha_fin),
            'numero_total'=>$this->unique_model->get_num_where('ventas_ventas',array(),sha1($tipo),$fecha_inicio,$fecha_fin),
            'total_ventas' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo' =>1,'tipo_pago'=>1),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['total'],
            'total_copias' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo' =>3),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['total'],
            'total_apartados' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo' =>2,'tipo_pago'=> 1),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['total'],
            'total_tarjetas' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago' =>2),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['total'],
            'gastos' => $this->unique_model->get_sum_where('gasto','ventas_gastos',array(),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['gasto'],
            'gastos_detalle' => $this->unique_model->query("SELECT descripcion,gasto,fecha FROM ventas_gastos  WHERE DATE(fecha) >= DATE('$fecha_inicio')  AND DATE(fecha) <= date('$fecha_fin') AND id_negocio = $id_negocio "),
            'fondo' => $this->unique_model->get_sum_where('fondo','ventas_fondos',array(),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['fondo'],
            'total' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago <>' => 2),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['total'],
            'total_caja' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago <>'=>2),sha1($tipo),$fecha_inicio,$fecha_fin)[0]['total'],
            'ganancia_ventas' => $ganancia_ventas,
            'ganancia_copias' => $ganancia_copias[0]['ganancia'],
            'ganancia_total' => $ganancia_ventas+$ganancia_copias[0]['ganancia'],
            'ventas' => $ventas,
            'ventas_categorias' => $ventas_categorias
        );
        return $data;
    }

    public function index(){
        $negocio= $this->negocio();
        $get = $this->input->get();
        $preferencias = $this->preferencias();
        /*if (strlen($get['tipo'])!=40)
         redirect('reportes/ventas/');*/
        if($get['tipo']==sha1('1'))
            $tipo_reporte="diario";
        else if ($get['tipo']==sha1('2'))
            $tipo_reporte="de la semana";
        else if ($get['tipo']==sha1('3'))
            $tipo_reporte="del mes";
        else if ($get['tipo']==sha1('4'))
            $tipo_reporte="anual";
        else if ($get['tipo']==sha1('5'))
            $tipo_reporte="especifico";
        $fecha_inicio =(isset($get['fecha_inicio']))?$get['fecha_inicio']:'';
        $fecha_fin =(isset($get['fecha_fin']))?$get['fecha_fin']:'';
        $group =(isset($get['group']))?$get['group']:'';
        $corte  = $this->data($get['tipo'],$group,$fecha_inicio,$fecha_fin);
        $data = $this->session->userdata();
        $this->load->library('fpdf');
        ob_end_clean();
        $this->pdf=new FPDF('P','mm','A4');
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $this->pdf->SetTitle('Reporte de ventas '.$tipo_reporte);
        $this->pdf->SetKeywords($isUTF8=true);
        if ($negocio[0]['logo'] !="")
            $this->pdf->Image('public/images/negocios/'.$negocio[0]['logo'],15,8,33);
        $this->pdf->SetFont('Arial','B',20);
        $this->pdf->Cell(30);
        $this->pdf->Cell(140,10,$negocio[0]['nombre_negocio'],0,0,'C');
        $this->pdf->SetFont('Arial','',15);
        $this->pdf->Cell(-140,23,'Reporte de ventas '.$tipo_reporte,0,0,'C');
        $this->pdf->Ln(13);
        $this->pdf->SetFont('Arial','B',8);
        $this->pdf->Cell(135,10,iconv('UTF-8', 'ISO-8859-2', 'Fecha de emisión: ').date('d-m-Y H:i:s'),0,'L','C');
        $usuario = $this->unique_model->get_rows_where("nombre","sistema_usuarios",array('id' => $data['id']))[0]['nombre'];
        $this->pdf->Cell(1,10,"Emisor: ".$usuario." ",0,'L','C');
        if ($this->exists($preferencias['tipos_pagos'],1))
            $this->pdf->Cell(-240,40,"Ventas (Efectivo): ".$corte['numero_ventas']." ",0,'L','C');
       // $this->pdf->Cell(310,40,"Ventas de copias: ".$corte['numero_copias']." ",0,'L','C');
       // $this->pdf->Cell(-230,40,"Pago de partados: ".$corte['numero_apartados']." ",0,'L','C');
        //$this->pdf->Cell(310,40,"Pago con tarjeta: ".$corte['numero_tarjeta']." ",0,'L','C');
        $this->pdf->Cell(310,40,"Ventas totales: ".$corte['numero_total']." ",0,'L','C');
        $this->pdf->Ln(7);
        $this->pdf->Cell(30,40,"Total (Efectivo): ".$corte['total_ventas']." ",0,'L','C');
       // $this->pdf->Cell(40,40,"Total de copias: ".$corte['total_copias']." ",0,'L','C');
       // $this->pdf->Cell(40,40,"Total de apartados: ".$corte['total_apartados']." ",0,'L','C');
       // $this->pdf->Cell(40,40,"Total de tarjetas: ".$corte['total_tarjetas']." ",0,'L','C');
        $this->pdf->Cell(40,40,"Total por ventas: ".$corte['total']." ",0,'L','C');
        $this->pdf->Ln(7);
        $this->pdf->SetFont('Arial','B',10);
        $this->pdf->setTextColor(153, 204, 255);
        $this->pdf->Cell(0,0,'Totales de venta',0,0,'C');
        $this->pdf->setTextColor(10, 10, 10);
        $this->pdf->Ln(1);
        $this->pdf->setTextColor(153, 204, 255);
        $this->pdf->Cell(0,0,'___________________________________________________________________________________________________',0,0,'C');
        $this->pdf->setTextColor(10,10,10);
        $this->pdf->Ln(20);
        if ($get['tipo']==sha1('1')):
            $this->pdf->SetFont('Arial','B',10);
            $this->pdf->setTextColor(153, 204, 255);
            $this->pdf->Cell(0,0,'Detalles de caja',0,0,'C');
            $this->pdf->setTextColor(10,10,10);
            $this->pdf->Ln(1);
            $this->pdf->setTextColor(153, 204, 255);
            $this->pdf->Cell(0,0,'___________________________________________________________________________________________________',0,0,'C');
            $this->pdf->setTextColor(10,10,10);
            $this->pdf->Cell(-250,10,"Gastos: ".$corte['gastos']." ",0,'L','C');
            $this->pdf->Cell(310,10,"Fondo: ".$corte['fondo']." ",0,'L','C');
            $total =number_format( ($corte['total_caja']+$corte['fondo'])-$corte['gastos'],2,'.','');
            $this->pdf->Cell(-250,10,"Total: ".$total." ",0,'L','C');
        endif;
        $this->pdf->Ln(10);
        if($get['tipo']==sha1('1')):
            $this->pdf->Cell(0,0,'Detalles de gastos',0,0,'C');
            $this->pdf->SetLeftMargin(45);
            //$color=0,0,102;
            $this->pdf->Ln(5);
            $this->pdf->SetFont('Arial','B', 10);
            //Seleccion de colores
            $this->pdf->SetFillColor(153, 204, 255);
            $this->pdf->SetDrawColor(153, 204, 255);
            $this->pdf->SetTextColor(10, 10, 10);
            $this->pdf->SetFont('Arial','B', 7);
            $this->pdf->Cell(60,7,iconv('UTF-8', 'ISO-8859-2', 'Descripción: '),'TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Gasto','TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Fecha - Hora','TBL',0,'C','1');
            $this->pdf->Ln(7);
            $i=0;
            foreach ($corte['gastos_detalle'] as $venta) {
                $this->pdf->SetTextColor(10, 10, 10);
                $i++;
                $this->pdf->Cell(60,5,$venta['descripcion'],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,$venta['gasto'],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,$venta['fecha'],'BRL',0,'C',0);
                $this->pdf->SetTextColor(10, 10, 10);
                $this->pdf->Ln(5);
            }

            $this->pdf->Ln(8);
        endif;
        $this->pdf->SetLeftMargin(10);
        $this->pdf->SetRightMargin(10);

        if ($get['tipo']==sha1('1') || $get['tipo']==sha1('2') || $get['tipo']==sha1('3') || $get['tipo']==sha1('4') || $get['tipo']==sha1('5') ):
            $this->pdf->SetFont('Arial','B',12);
            $this->pdf->setTextColor(153, 204, 255);
            $this->pdf->Cell(0,0,'Ganancias',0,0,'C');
            $this->pdf->setTextColor(10, 10, 10);
            $this->pdf->Ln(1);
            $this->pdf->SetFont('Arial','B',10);
            $this->pdf->setTextColor(153, 204, 255);
            $this->pdf->Cell(0,0,'___________________________________________________________________________________________________',0,0,'C');
            $this->pdf->setTextColor(10,10,10);
            $this->pdf->SetFont('Arial','B',12);
            $this->pdf->Cell(-220,10,"Ventas: ".number_format($corte['ganancia_ventas'],2,'.','')." ",0,'L','C');
           // $this->pdf->Cell(330,10,"Copias: ".number_format($corte['ganancia_copias'],2,'.','')." ",0,'L','C');
            $ganancia_total =(number_format(($corte['ganancia_total']-$corte['gastos']),2,'.','')<=0)?"0.00":number_format(($corte['ganancia_total']-$corte['gastos']),2,'.','');
            $this->pdf->Cell(300,10,"Total: ".$ganancia_total." ",0,'L','C');

            if ($ganancia_total==0){
                $this->pdf->Ln(13);
                $this->pdf->SetFont('Arial','B',12);
                $this->pdf->setTextColor(255, 0,20);
                $this->pdf->Cell(0,0,'Perdidas',0,0,'C');
                $this->pdf->Ln(8);
                $this->pdf->Cell(0,0,'Perdida: '.number_format(($corte['ganancia_total']-$corte['gastos'])*-1,2,'.',''),0,0,'C');
            }
        endif;

        $this->pdf->Ln(5);
        $this->pdf->setTextColor(10, 10, 10);
        //$this->pdf->Cell(340,40,"Numero de ventas de copias: ".$corte['numero_copias']." ",0,'L','C');
        $this->pdf->Ln(10);
        if ($get['detalle'] == sha1('1') && $get['tipo'] == sha1('1') || $group==sha1('1') && $get['tipo'] != sha1('2') && $get['tipo'] != sha1('3') && $get['tipo'] != sha1('4')):
            $this->pdf->Cell(0,0,'Detalles de ventas '.$tipo_reporte,0,0,'C');
            $this->pdf->Ln(5);
            $this->pdf->SetLeftMargin(25);
            $this->pdf->SetRightMargin(10);
            //$color=0,0,102;
            $this->pdf->SetFont('Arial','B', 10);
            //Seleccion de colores
            $this->pdf->SetFillColor(153, 204, 255);
            $this->pdf->SetDrawColor(153, 204, 255);
            $this->pdf->SetTextColor(10, 10, 10);
            $this->pdf->SetFont('Arial','B', 7);
            $this->pdf->Cell(20,7,'Folio','TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Fecha - Hora','TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Tipo','TBL',0,'C','1');
            $this->pdf->Cell(50,7,'Tipo de pago','TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Total','TBL',0,'C','1');
            $this->pdf->Ln(7);
            //iconv('UTF-8', 'ISO-8859-2', 'Descripción')
            $c=0;
            foreach ($corte['ventas'] as $venta) {
                $this->pdf->SetTextColor(10, 10, 10);
                $c++;
                $this->pdf->Cell(20,5,$venta['folio'],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,$venta['fecha'],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,$venta['tipo'],'BRL',0,'C',0);
                $this->pdf->Cell(50,5,$venta['tipo_pago'],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,'$'.$venta['total'],'BRL',0,'C',0);
                $this->pdf->SetTextColor(10, 10, 10);
                $this->pdf->Ln(5);
            }
        endif;

        if ($get['detalle'] == sha1('1') && $get['tipo'] == sha1('2') ||  $get['tipo'] == sha1('3') ||  $get['tipo'] == sha1('4') || $group==sha1('2') || $group==sha1('3') || $group==sha1('4')):
            $col="dia";
            if ($get['tipo'] == sha1('2') || $group== sha1('2') )
                $col = "dia";
            else  if ($get['tipo'] == sha1('3') || $group== sha1('3'))
                $col = "semana";
            else if ($get['tipo'] == sha1('4') || $group== sha1('4'))
                $col= "mes";
            $this->pdf->Ln(10);
            $this->pdf->Cell(0,0,'Detalles de ventas '.$tipo_reporte,0,0,'C');
            $this->pdf->Ln(5);
            $this->pdf->SetLeftMargin(35);
            $this->pdf->SetRightMargin(10);
            //$color=0,0,102;
            $this->pdf->SetFont('Arial','B', 10);
            //Seleccion de colores
            $this->pdf->SetFillColor(153, 204, 255);
            $this->pdf->SetDrawColor(153, 204, 255);
            $this->pdf->SetTextColor(10, 10, 10);
            $this->pdf->SetFont('Arial','B', 7);
            $this->pdf->Cell(50,7,$col,'TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Total por ventas','TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Gastos','TBL',0,'C','1');
            $this->pdf->Cell(30,7,'Ganancias','TBL',0,'C','1');
            $this->pdf->Ln(7);
            //iconv('UTF-8', 'ISO-8859-2', 'Descripción')
            $c=0;
            $tv=0;
            $g=0;
            $gt=0;
            $gg=0;
            if ($get['tipo'] != sha1(1)):
            foreach ($corte['ventas'] as $venta) {
                $this->pdf->SetTextColor(10, 10, 10);
                $c++;
                $tv+= $venta['total_ventas'];
                $g+= $venta['ganancia'];
                $gt+= $venta['gastos'];
                $this->pdf->Cell(50,5,$venta[$col],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,number_format($venta['total_ventas'],2,'.',''),'BRL',0,'C',0);
                $this->pdf->Cell(30,5,number_format($venta['gastos'],2,'.',''),'BRL',0,'C',0);
                $this->pdf->Cell(30,5,number_format($venta['ganancia'],2,'.',''),'BRL',0,'C',0);
                $this->pdf->SetTextColor(10, 10, 10);
                $this->pdf->Ln(5);
            }
            endif;
            $this->pdf->SetTextColor(10, 10, 10);
            $this->pdf->Cell(50,5,"Total",'BRL',0,'C',0);
            $this->pdf->Cell(30,5,number_format($tv,2,'.',''),'BRL',0,'C',0);
            $this->pdf->Cell(30,5,number_format($gt,2,'.',''),'BRL',0,'C',0);
            $this->pdf->Cell(30,5,number_format($g,2,'.',''),'BRL',0,'C',0);
            $this->pdf->SetTextColor(10, 10, 10);
            $this->pdf->Ln(5);

        endif;
        $this->pdf->Ln(10);
        $this->pdf->SetFont('Arial','B', 12);
        $this->pdf->Cell(150,0,'Detalles de ventas por categorias',0,0,'C');

        $this->pdf->Ln(5);
        $this->pdf->SetLeftMargin(50);
        $this->pdf->SetRightMargin(10);
        //$color=0,0,102;
        $this->pdf->SetFont('Arial','B', 10);
        //Seleccion de colores
        $this->pdf->SetFillColor(153, 204, 255);
        $this->pdf->SetDrawColor(153, 204, 255);
        $this->pdf->SetTextColor(10, 10, 10);
        $this->pdf->SetFont('Arial','B', 7);
        $this->pdf->Cell(50,7,'Categoria','TBL',0,'C','1');
        $this->pdf->Cell(30,7,'Total','TBL',0,'C','1');
        $this->pdf->Cell(30,7,'Ganancias','TBL',0,'C','1');
        $this->pdf->Ln(7);
        if (count($corte['ventas_categorias'] )>0):
            foreach ($corte['ventas_categorias'] as $venta) {
                $this->pdf->SetTextColor(10, 10, 10);
                $this->pdf->Cell(50,5,$venta['categoria'],'BRL',0,'C',0);
                $this->pdf->Cell(30,5,number_format($venta['total'],2,'.',''),'BRL',0,'C',0);
                $this->pdf->Cell(30,5,number_format($venta['ganancia'],2,'.',''),'BRL',0,'C',0);
                $this->pdf->SetTextColor(10, 10, 10);
                $this->pdf->Ln(5);
            }
        endif;
        $this->pdf->SetY(270);
        $this->pdf->SetFont('Arial','I', 8);
        $this->pdf->Cell(0,0,'Pagina '.$this->pdf->PageNo().'/{nb}',0,0,'C');
        $this->pdf->Output("Reporte de ventas $tipo_reporte".".pdf",'I');

    }

}
