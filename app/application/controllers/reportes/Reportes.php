<?php
class Reportes extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(5);

    }
    public function index(){
        $data = $this->session->userdata();
        $data['modulo'] = "Generar reportes";
        $data['id_modulo'] =4.8;
        $data['id_seccion']=8;
        $this->log(8);
        $this->load->view("header",$data);
        $this->load->view("index");
        $this->load->view("reportes/reportes");
    }
}
