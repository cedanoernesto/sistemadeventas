<?php
    class Errors extends CI_Controller{

        function __construct(){
            parent::__construct();
        }

        public function error_access(){
            $data = array('heading' =>'No tiene permiso para estar aqui!','message' =>'Actualmente no tiene permiso para estar en este sitio, contacte al administrador para mas detalles.');
            $this->load->view("errors/html/error_db",$data);
        }
    }

?>