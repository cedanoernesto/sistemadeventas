<?php
class Tickets_anteriores extends MY_Controller{

    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(16);
    }
    public function index(){
        $data = $this->session->userdata();
        $data['modulo'] = "Corte de caja";
        $data['id_modulo'] = 3.9;
        $data['id_seccion'] = 3;
        $this->log(9);
        $this->load->view("header", $data);
        $this->load->view("index");
        $this->load->view("ventas/tickets_anteriores");
    }

    public function data(){
        $post = $this->input->post();

        if ($post['fecha'] == ''){
            $fecha  =  "'".date('Y-m-d')."''";
        }
        else {
            $fecha =  "'".$post['fecha']."'";
        }
        $id_negocio  = $this->id_negocio();
        $ganancia_cantidad = $this->unique_model->query("
        SELECT SUM(ventas_ventas_detalles.subtotal)- (SUM(inventario_articulos.precio_compra/inventario_articulos.cantidad_unidad)*ventas_ventas_detalles.cantidad) AS ganancia
            FROM ventas_ventas_detalles
            JOIN ventas_ventas ON ventas_ventas.id = ventas_ventas_detalles.id_venta
            JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
            WHERE ventas_ventas_detalles.tipo_venta=1 AND DATE(ventas_ventas.fecha) = DATE($fecha) AND ventas_ventas.id_negocio =$id_negocio AND ventas_ventas.estatus=1");
        $ganancia_normal = $this->unique_model->query("
        SELECT sum(ventas_ventas_detalles.subtotal)-sum(inventario_articulos.precio_compra*ventas_ventas_detalles.cantidad) AS ganancia
            FROM ventas_ventas_detalles
            JOIN ventas_ventas ON ventas_ventas.id = ventas_ventas_detalles.id_venta
            JOIN inventario_articulos ON inventario_articulos.id = ventas_ventas_detalles.id_articulo
            WHERE ventas_ventas_detalles.tipo_venta=2 AND DATE(ventas_ventas.fecha) = DATE($fecha) AND ventas_ventas.id_negocio =$id_negocio AND ventas_ventas.estatus=1");
        $ganancia_copias = $this->unique_model->query("SELECT IFNULL(sum(ventas_ventas.total),0) AS ganancia
        FROM ventas_ventas
        WHERE ventas_ventas.tipo=3 AND DATE(ventas_ventas.fecha) = DATE(now()) AND ventas_ventas.id_negocio =$id_negocio AND ventas_ventas.estatus=1");
        $gastos = $this->unique_model->get_sum_where("gasto","ventas_gastos",array(),sha1('1'),'','')[0]['gasto'];
        $ganancia_ventas = $ganancia_cantidad[0]['ganancia'] + $ganancia_normal[0]['ganancia'];
        $data = array(
            'numero_ventas'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo' =>1,'tipo_pago'=>1),sha1('5'),$fecha,''),
            'numero_copias'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo' =>3),sha1('1'),$fecha,''),
            'numero_apartados'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo' =>2,'tipo_pago'=> 1),sha1('5'),$fecha,''),
            'numero_tarjeta'=>$this->unique_model->get_num_where('ventas_ventas',array('tipo_pago'=>2),sha1('5'),$fecha,''),
            'numero_total'=>$this->unique_model->get_num_where('ventas_ventas',array(),sha1('5'),$fecha,''),
            'total_ventas' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo' =>1,'tipo_pago'=>1),sha1('5'),$fecha,'')[0]['total'],
            'total_copias' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo' =>3),sha1('5'),$fecha,'')[0]['total'],
            'total_apartados' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo' =>2,'tipo_pago'=> 1),sha1('5'),$fecha,'')[0]['total'],
            'total_tarjetas' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago' =>2),sha1('5'),$fecha,'')[0]['total'],
            'gastos' => $this->unique_model->get_sum_where('gasto','ventas_gastos',array(),sha1('5'),$fecha,'')[0]['gasto'],
            'salidas' => $this->unique_model->get_sum_where('salida','ventas_salida',array(),sha1('5'),$fecha,'')[0]['salida'],
            'fondo' => $this->unique_model->get_sum_where('fondo','ventas_fondos',array(),sha1('5'),$fecha,'')[0]['fondo'],
            'total' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago <>'=>2),sha1('5'),$fecha,'')[0]['total'],
            'total_caja' => $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago <>'=>2),sha1('5'),$fecha,'')[0]['total'],
            'total_cortes' => $this->unique_model->get_sum_where('monto','ventas_corte_caja',array(),sha1('5'),$fecha,'')[0]['monto'],
            'ganancia_ventas' => $ganancia_ventas,
            'ganancia_copias' => $ganancia_copias[0]['ganancia'],
            'ganancia_total' => $ganancia_ventas+$ganancia_copias[0]['ganancia']
        );
        $this->json($data);
        return $data;
    }

    public function ventas($fecha){
        $fecha = "'".$fecha."'";
        $get =$this->input->get();
        $start =0;
        $count =10;
        if(isset($get['start'])){
            $start = $get['start'];
            $count = $get['count'];
        }
        $filter = array();
        $sort =array();
        if(isset($get['filter']))
            $filter =$get['filter'];
        if(isset($get['sort']))
            $sort = $get['sort'];
        /*****************************CONSULTA*********************************/
        $query = array(
            "SELECT"=>array("md5(ventas_ventas.id) as ide,folio_venta(ventas_ventas.id) as folio,ventas_tipos.nombre as tipo,fecha ,total"),
            "FROM" =>array("ventas_ventas"),
            "JOIN"=>array(
                array("ventas_tipos","ventas_tipos.id =ventas_ventas.tipo","")
            ),
            "LIKE" =>array(),
            "WHERE" => array(
                array("date(fecha)=date($fecha)"),
                array('ventas_ventas.id_negocio',$this->id_negocio()),
                array('ventas_ventas.estatus',1),
            ),
            //"LIMIT" => array($count,$start),
            "ORDER BY" =>array(),
            "RETURN" =>array()
        );
        foreach ($filter as $index => $value){
            if ($index=="folio")
                $index="concat(folio_venta(ventas_ventas.id))";
            if ($index=="tipo")
                $index="ventas_tipos.id";
            $query["LIKE"][]=array($index,$value);
        }

        foreach ($sort as $index => $value)
            $query["ORDER BY"]=array($index,$value);

        $query2 = $query;
        unset($query2["LIMIT"]);
        array_push($query2["RETURN"],"num_rows");
        $num = $this->unique_model->get_query($query2);
        $result = $this->unique_model->get_query($query);
        $return = array(
            'data' => $result,
            'total_count' =>$num,
            'pos' =>$start
        );
        $this->json($return);
    }

    public function venta(){
        $this->json($this->unique_model->get_rows_where("descripcion,cantidad,precio,subtotal","ventas_ventas_detalles",array('md5(id_venta)' => $this->input->post('id'))));
    }

    public function save(){
        $post = $this->input->post();
        $post['id_negocio'] = $this->id_negocio();
        $post['id']=0;
        $this->unique_model->save('ventas_fondos',$post);
    }

    public function check(){
        echo  0;
        //$this->unique_model->get_num_where("ventas_fondos",array(),sha1(1),"","");
    }

    public function gastos(){
        $id_negocio = $this->id_negocio();
        $this->json($this->unique_model->query("SELECT CONCAT(gasto,' - ',descripcion,' - ',fecha) AS value FROM ventas_gastos WHERE DATE(fecha) = DATE(NOW()) AND id_negocio = $id_negocio"));
    }
    public function salidas(){
        $id_negocio = $this->id_negocio();
        $this->json($this->unique_model->query("
              SELECT CONCAT(salida,' - ',fecha) AS value 
              FROM ventas_salida 
              WHERE DATE(fecha) = DATE(NOW()) AND id_negocio = $id_negocio"));
    }

    public function imprimirCorte(){
        $data['total_ventas'] = $this->unique_model->get_sum_where('total','ventas_ventas',array('tipo_pago <>'=>2),sha1('1'),'','')[0]['total'];
        $data['gastos'] = $this->unique_model->get_sum_where('gasto','ventas_gastos',array(),sha1('1'),'','')[0]['gasto'];
        $data['total'] = $data['total_ventas'] - $data['gastos'];
        $data['datos_ticket'] = $this->unique_model->get_rows_where("*","sistema_ticket",['id_negocio' => $this->id_negocio()])[0];
        $data['modulo'] = 'Corte de caja';
        $data['fecha'] = date('Y-m-d H:i:s');
        $this->json($data);
    }

    public function preferencias(){
        $preferencias =array();
        $preferencias['corte'] = $this->unique_model->get_rows_where("maneja_multiple_corte","preferencias_corte_caja",array('id_negocio' => $this->id_negocio()))[0]['maneja_multiple_corte'];
        $preferencias['tipos_ventas']= $this->unique_model->get_rows_where("id_tipo_venta as id","ventas_tipo_venta_negocios",array('id_negocio'=> $this->id_negocio(), 'estatus' => 1));
        $preferencias['tipos_pagos']= $this->unique_model->get_rows_where("id_tipo_pago as id","ventas_tipo_pago_negocios",array('id_negocio'=> $this->id_negocio(), 'estatus' => 1));
        $this->json($preferencias);
    }

    public function saveMultiple(){
        $this->unique_model->save('ventas_corte_caja',array('id' => 0,'monto' => $this->input->post('monto_retirar_corte_caja'), 'id_negocio'=> $this->id_negocio()));

    }

    public function getMultiple(){
        $this->json($this->unique_model->get_query(
            array(
                "SELECT" => array("id,concat(monto, ' - ' ,fecha) as value"),
                "FROM" =>array("ventas_corte_caja"),
                "WHERE" => array(
                    array("DATE(fecha) = DATE(NOW())"),
                    array("id_negocio",$this->id_negocio())
                )
            )
        ));

    }

    public function getTiposVenta(){
        $this->json(
            $this->unique_model->get_query(
                array("SELECT" => "ventas_tipo_venta.id,ventas_tipo_venta.nombre",)
            )
        );
    }


}
