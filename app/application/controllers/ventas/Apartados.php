 <?php

class Apartados extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->verify();
        $this->access(7);
    }

    public function index()
    {
        $data = $this->session->userdata();
        $data['modulo'] = "Apartados";
        $data['id_modulo'] = 6.7;
        $data['id_seccion'] = 6;
        $this->log(7);
        $this->load->view("header", $data);
        $this->load->view("index");
        $this->load->view("ventas/apartados");
    }

    public function save(){
        $data = $this->session->userdata();
        $post = $this->input->post();
        $detalles = json_decode($post['detalles'],true);
        $post =json_decode($post['venta'],true);

        //$post['id_articulo'] = $this->unique_model->get_id_md5("inventario_articulos", "id", $post['id_articulo'])['id'];
        $post['id']=0;
        $post['id_cliente'] = $this->unique_model->get_id_md5("inventario_clientes", "id", $post['id_cliente'])['id'];
        $post['saldo'] = $post['total'] - $post['pago_inicial'];
        $post['id_negocio'] = $this->id_negocio();
        $post['pago'] = $post['pago'];
        $fecha = new DateTime();
        $fecha->add(new DateInterval('P30D'));
        $post['fecha_vencimiento'] = $fecha->format('Y-m-d');
        $fecha->add(new DateInterval('P7D'));
        $post['fecha_vencimiento_tolerancia'] = $fecha->format('Y-m-d');

        $post['id_usuario'] = $data['id'];
        $pago = $post['pago'];
        $post['cambio'] = $post['pago'] - $post['pago_inicial'];
        $pago_inicial = $post['pago_inicial'];
        $tipo_pago = $post['tipo_pago'];
        $id_descuento = $post['id_descuento'];
        $cambio = $pago_inicial - $post['pago_inicial'];

        unset($post['pago_inicial']);
        unset($post['pago']);
        unset($post['cambio']);
        unset($post['tipo_pago']);
        unset($post['id_descuento']);

        $id = $this->unique_model->save('ventas_apartados', $post);
        foreach ($detalles as $index => $detalle){
            $id_articulo =$this->unique_model->get_id_md5("inventario_articulos","id",$detalles[$index]['id_articulo'])['id'];
           unset($detalles[$index]['ui']);
           unset($detalles[$index]['paquete']);
           unset($detalles[$index]['comun']);
            $detalles[$index]['id_apartado'] = $id;
            $detalles[$index]['id_articulo'] = $id_articulo;
             $this->unique_model->save("ventas_apartados_detalles",$detalles[$index]);
        }
        $id_venta= $this->unique_model->save('ventas_ventas',array('id'=>0,'tipo_pago' => $tipo_pago,'tipo' =>2,'total' => $pago_inicial,'pago' => $pago,'cambio' => $pago-$pago_inicial,'id_usuario' =>$data['id'],'id_negocio' => $this->id_negocio(),'id_descuento' => $id_descuento));
        $this->unique_model->save("ventas_ventas_detalles",array('id' =>0,'descripcion' => 'ABONO DE APARTADO','id_venta' => $id_venta,'cantidad' => 1,'precio' => $pago_inicial,'subtotal' =>$pago_inicial));
        $this->unique_model->save('ventas_apartados_abonos', array('id' => 0, 'pago' => $pago, 'id_apartado' => $id, 'cambio' => $pago - $pago_inicial, 'abono' => $pago_inicial));
        $this->json($this->ticket(md5($id)));
    }
    public function ticket($md5, $saldo = 0){
        $venta = $this->unique_model->query("
        SELECT ventas_apartados.*,ventas_apartados_abonos.abono,ventas_apartados_abonos.pago,ventas_apartados_abonos.cambio,folio_apartado(ventas_apartados.id) AS folio,inventario_clientes.nombre AS cliente,sistema_usuarios.nombre
        FROM ventas_apartados
        JOIN inventario_clientes ON inventario_clientes.id = ventas_apartados.id_cliente
        JOIN sistema_usuarios ON sistema_usuarios.id = ventas_apartados.id_usuario
        JOIN ventas_apartados_abonos  ON ventas_apartados_abonos.id_apartado =ventas_apartados.id
        WHERE ventas_apartados_abonos.id = (SELECT MAX(id) FROM ventas_apartados_abonos WHERE md5(ventas_apartados.id)='$md5')")[0];
        $datos_ticket = $this->unique_model->get_rows_where("*","sistema_ticket",array('id_negocio' => $this->id_negocio() ));
        $data = [
            'modulo' => 'Comprobante de apartado',
            'venta' => $venta,
            'detalles' => $this->unique_model->get_rows_where("*","ventas_apartados_detalles",array('md5(id_apartado)' => $md5)),
            'numero_abonos' => count($this->unique_model->get_rows_where("id", "ventas_apartados_abonos", array('md5(id_apartado)' => $md5))),
            'datos_ticket' => $datos_ticket[0]
        ];
        $data['venta']['_saldo'] = $saldo;
        return $data;
    }

    public function articulos(){
        $post = $this->input->post();
        $this->json($this->unique_model->get_rows_where("*","ventas_apartados_detalles",array('md5(id_apartado)' =>  $post['id'])));

    }
    public function search(){
        $post = $this->input->post();
        $id_negocio = $this->id_negocio();
        $apartado = $this->unique_model->query("
            SELECT md5(ventas_apartados.id) as id,ventas_apartados_abonos.abono,ventas_apartados.fecha,ventas_apartados.saldo,ventas_apartados.total,folio_apartado(ventas_apartados.id) as folio,inventario_clientes.nombre AS cliente
            FROM ventas_apartados
            JOIN inventario_clientes ON inventario_clientes.id = ventas_apartados.id_cliente
            JOIN ventas_apartados_abonos  ON ventas_apartados_abonos.id_apartado =ventas_apartados.id
            WHERE ventas_apartados_abonos.id= (SELECT MAX(id) FROM ventas_apartados_abonos WHERE (ventas_apartados.codigo='$post[codigo]' OR md5(ventas_apartados.id_cliente) ='$post[id_cliente]')AND ventas_apartados_abonos.id_apartado =  ventas_apartados.id  AND ventas_apartados.estatus=0 AND NOW()<=ventas_apartados.fecha_vencimiento) AND ventas_apartados.id_negocio = $id_negocio");

        $this->json($apartado);
    }
    public function pay(){
        $data = $this->session->userdata();
        $post = $this->input->post();
        $apartado = $this->unique_model->get_rows_where("id_cliente,id,saldo,total", "ventas_apartados", array('md5(id)' => $post['id_apartado']))[0];
        $id_apartado = $apartado['id'];
        $tipo_pago=$post['tipo_pago'];
        unset($post['tipo_pago']);
        $saldo = $apartado['saldo'] - $post['abono'];
        if ($saldo<=0)
            $saldo=0;
        $estatus=(!$saldo)?1:0;
        $this->unique_model->save('ventas_apartados', array('id' => $post['id_apartado'], 'saldo' => $saldo, 'estatus' => $estatus));
        $id_venta = $this->unique_model->save('ventas_ventas',array('id' => 0, 'tipo_pago' => $tipo_pago,'tipo' => 2, 'total' => $post['abono'],'pago' => $post['pago'] ,'cambio' =>$post['cambio'], 'id_usuario' => $data['id'],'id_negocio' => 1));
        $this->unique_model->save("ventas_ventas_detalles",array('id' =>0,'descripcion' => 'ABONO DE APARTADO','id_venta' => $id_venta,'cantidad' => 1,'precio' => $post['abono'],'subtotal' =>$post['abono']));
        /*Si se termino el abono empieza un proceso para dar de baja al cliente y decrementar la existencia de los articulos incluidos en el mismo.*/
        $num_apartados = $this->unique_model->get_rows_where('*','ventas_apartados',['id_cliente' => $apartado['id_cliente']]);
        if ($estatus){
            if (!$num_apartados){
                $this->unique_model->save("inventario_clientes",array('id' => md5($apartado['id_cliente']), 'estatus' => 3));
            }
            $articulos = $this->unique_model->get_rows_where("*","ventas_apartados_detalles",array('id_apartado' => $id_apartado));
            foreach ($articulos as $index => $articulo){
                $articulos[$index]['id'] =0;
                $articulos[$index]['id_venta'] =$id_venta;
                unset($articulos[$index]['id_apartado']);
                $this->unique_model->save("ventas_ventas_detalles",$articulos[$index]);
                    $this->article($articulo);
            }
        }
        unset($post['abono_apartado']);
        $post['id'] = 0;
        $post['id_apartado'] = $id_apartado;
        $id = $this->unique_model->save("ventas_apartados_abonos", $post);
        $this->json($this->ticket(md5($id_apartado),$apartado['saldo'] ));
    }

    public function article($value){
        $articulo = $this->unique_model->get_rows_where("existencia,cantidad_unidad,existencia_unidad","inventario_articulos",array('id'=> $value['id_articulo']))[0];
        $existencia  = $articulo['existencia'];
         $cantidad_unidad  = $articulo['cantidad_unidad'];
        if ($value['tipo_venta']==0){
                $existencia-=$value['cantidad'];
                $save =array('id' =>$value['id_articulo'], 'existencia' =>$existencia);
        }
        else{
            $existencia_unidad  = $articulo['existencia_unidad'];
            if ($value['tipo_venta']==1){
                $existencia_unidad-=$value['cantidad'];
                $existencia =floor($existencia_unidad/$cantidad_unidad);
            }
            else{
                $existencia-=$value['cantidad'];
                $existencia_unidad =($existencia_unidad-($cantidad_unidad*$value['cantidad']));
            }
            $save =array('id' =>$value['id_articulo'], 'existencia_unidad' =>$existencia_unidad,'existencia' => $existencia);
        }
        $this->unique_model->save("inventario_articulos",$save);
    }

    public function abonos(){
        $this->json($this->unique_model->get_rows_where("folio_apartado(id_apartado) as folio,fecha,abono,pago,cambio", "ventas_apartados_abonos", array('md5(id_apartado)' => $this->input->post('id'))));
    }
}
