<?php
class Ventas extends MY_Controller{
    public function __construct(){
        parent::__construct();
        $this->verify();
        $this->access(5);

    }
    public function index(){
        $data = $this->session->userdata();
        $data['modulo'] = "Ventas";
        $data['id_modulo'] =3.5;
        $data['id_seccion']=3;
        $data['id_negocio'] = $this->id_negocio();
        $this->log(5);
        $this->load->view("header",$data);
        $this->load->view("index");
        $this->load->view("ventas/ventas");
    }
    public function session(){
        echo 1;
    }
    public function cancelarVenta(){
        $id_venta = $this->unique_model->save('ventas_ventas',['id' => $this->input->post('id'),'estatus' =>0],0);
        $detalles  = $this->unique_model->query("SELECT ventas_ventas_detalles.id_articulo,ventas_ventas_detalles.cantidad,ventas_ventas_detalles.tipo_venta  FROM ventas_ventas_detalles JOIN ventas_ventas ON ventas_ventas.id=ventas_ventas_detalles.id_venta  WHERE md5(ventas_ventas_detalles.id_venta) ='$id_venta' AND ventas_ventas.estatus=0");
        $message = "Se regresaron a inventario: ";
        foreach ($detalles as $detalle){
            $articulo  = $this->unique_model->get_rows_where('descripcion,venta_unidad,existencia,existencia_unidad,cantidad_unidad','inventario_articulos', ['id' => $detalle['id_articulo']])[0];

            $update = ['id' => md5($detalle['id_articulo'])];
            $cantidad =$detalle['cantidad'];
            switch ($detalle['tipo_venta']){
                case 1:
                    $update['existencia']=floor(($articulo['existencia_unidad']+$detalle['cantidad'])/$articulo['cantidad_unidad']);
                    $update['existencia_unidad']=$articulo['existencia_unidad']+=$detalle['cantidad'];
                    break;
                case 2:
                    $update['existencia']=$articulo['existencia']+$detalle['cantidad'];
                    if ($articulo['venta_unidad']){
                        $update['existencia_unidad']=$articulo['existencia_unidad']+=($detalle['cantidad']*$articulo['cantidad_unidad']);
                        $cantidad = $detalle['cantidad']*$articulo['cantidad_unidad'];
                    }
                    break;
            }
            $message.="<br>".$cantidad."-".$articulo['descripcion'];
            $this->unique_model->save('inventario_articulos',$update,0);
            $this->json([
                'estatus' =>1,
                'message' => $message
            ]);
        }
    }


    public function buscarVenta(){
        $venta = $this->unique_model->get_rows_where("md5(id) as id","ventas_ventas",['folio_venta(id)' => $this->input->post('folio_cancelacion'),'estatus' =>1]);
        $venta = (count($venta))?$venta[0]:$venta;
        if(count($venta)){
            $json = [
                'id' => $venta['id'],
                'url' => base_url()."ventas/ventas/ticket/".$venta['id'],
                'estatus' => 1
            ];
        }
        else{
            $json = [
                'message' => 'Folio de venta no encontrado',
                'estatus' => 0
            ];
        }
        $this->json($json);

    }

    public function checkFondo(){
        $id_negocio = $this->id_negocio();
        $this->json(count($this->unique_model->query("SELECT id FROM ventas_fondos WHERE DATE(fecha) =DATE(NOW()) AND  id_negocio = $id_negocio")));
    }
    /*
        public function selectArticulos(){
            $get = $this->input->get();
            $filter="";
            $limit = 5;
            if (isset($get['filter']['value']))
                $filter = $get['filter']['value'];
            $query =array(
                "SELECT" => array("md5(id) as id, concat(descripcion,'-',codigo_barras) AS value"),
                "FROM" => array("inventario_articulos"),
                "LIMIT" =>array($limit),
                "LIKE" => array(array("descripcion",$filter,"both")),
                "WHERE" =>array(array("estatus",1),array('id_negocio',$this->id_negocio())),
                "LIMIT" => array(5)
            );
            $this->json($this->unique_model->get_query($query));
        }*/

    public function getPaquetes(){
        $precio = $this->unique_model->get_rows_where("precio,descripcion,cantidad_exacta,id_tipo_cantidad,tipo_venta,md5(id_articulo) as id_articulo,cantidad_maxima,cantidad_minima,hora_inicio,hora_fin","inventario_paquetes",['id_negocio' => $this->id_negocio(), 'estatus' =>1 ]);
        $this->json($precio);
    }
    public function getArticulosComun(){
        $precio = $this->unique_model->get_rows_where("md5(id) as id,precio,descipcion","inventario_articulo_comun",['id_negocio' => $this->id_negocio()]);
        $this->json($precio);
    }
    public function getArticulos(){
        $id_negocio = $this->id_negocio();
        $query = "
        SELECT MD5(inventario_articulos.id) AS id,inventario_articulos.cantidad_mayoreo,inventario_articulos.descripcion as value,inventario_articulos.cantidad_unidad_mayoreo,inventario_articulos.comun,inventario_articulos.cantidad_mayoreo,inventario_articulos.venta_unidad,inventario_articulos.id_tipo_venta,inventario_articulos.codigo_barras,inventario_articulos.precio_compra,inventario_articulos.precio_unidad_mayoreo,inventario_articulos.precio_unidad,inventario_articulos.cantidad_unidad,inventario_articulos.existencia,inventario_articulos.existencia_unidad,inventario_articulos.precio_venta,inventario_articulos.precio_mayoreo,inventario_articulos.venta_unidad,inventario_articulos.descripcion,inventario_tipo_venta.nombre AS tipo_venta,inventario_contenedor.nombre AS contenedor
        FROM inventario_articulos
        LEFT JOIN inventario_tipo_venta ON inventario_tipo_venta.id= inventario_articulos.id_tipo_venta
        LEFT JOIN inventario_contenedor ON inventario_contenedor.id= inventario_articulos.id_contenedor
        WHERE inventario_articulos.estatus=1 AND inventario_articulos.id_negocio = $id_negocio";
        $articulo = $this->unique_model->query($query);
        $this->json($articulo);
    }
    public function getArticulo(){
        $post= $this->input->post();
        $id =(isset($post['id']))?$post['id']:0;
        $codigo=0;
        if (isset($post['codigo'])){
            $art = $this->unique_model->get_rows_where('md5(id) as id','inventario_articulos',['codigo_barras' => $post['codigo']]);
            if (count($art)>0)
                $id = $art[0]['id'];
        }
        $venta_unidad=0;
        $query = "
        SELECT MD5(inventario_articulos.id) AS id,inventario_articulos.venta_unidad,inventario_articulos.precio_compra,inventario_articulos.precio_unidad_mayoreo,inventario_articulos.precio_unidad,inventario_articulos.cantidad_unidad,inventario_articulos.existencia,inventario_articulos.precio_venta,inventario_articulos.precio_mayoreo,inventario_articulos.venta_unidad,inventario_articulos.descripcion,inventario_tipo_venta.nombre AS tipo_venta,inventario_contenedor.nombre AS contenedor
        FROM inventario_articulos
        LEFT JOIN inventario_tipo_venta ON inventario_tipo_venta.id= inventario_articulos.id_tipo_venta
        LEFT JOIN inventario_contenedor ON inventario_contenedor.id= inventario_articulos.id_contenedor
        WHERE md5(inventario_articulos.id) = '$id' ";
        if ($id!=0)
            $tipo = $this->unique_model->get_rows_where('venta_unidad','inventario_articulos',['md5(id)' => $post['id']]);
        if (isset($tipo[0]['venta_unidad']))
            $venta_unidad=$tipo[0]['venta_unidad'];
        if ($venta_unidad){
            $query ="SELECT MD5(inventario_articulos.id) AS id,inventario_articulos.precio_unidad_mayoreo,inventario_articulos.precio_compra,inventario_articulos.venta_unidad,inventario_articulos.precio_unidad,inventario_articulos.cantidad_unidad,inventario_articulos.existencia,inventario_articulos.precio_venta,inventario_articulos.precio_mayoreo,inventario_articulos.venta_unidad,inventario_articulos.descripcion,inventario_tipo_venta.nombre AS tipo_venta,inventario_contenedor.nombre AS contenedor,inventario_articulos.existencia_unidad as existencia_unidad
                FROM inventario_articulos
                LEFT JOIN inventario_tipo_venta ON inventario_tipo_venta.id= inventario_articulos.id_tipo_venta
                LEFT JOIN inventario_contenedor ON inventario_contenedor.id= inventario_articulos.id_contenedor
                 WHERE md5(inventario_articulos.id) = '$id'";
        }
        $articulo = $this->unique_model->query($query);
        $this->json($articulo);
    }
    public function save(){
        $session = $this->session->userdata();
        $post = $this->input->post();
        $venta = json_decode($post['venta'],true);
        $detalles = json_decode($post['venta_detalles'],true);
        $venta['id_usuario']=$session['id'];
        $venta['id_negocio']=$this->id_negocio();
        $venta['id']=0;
        $venta['importe']=$venta['total']*1.16;
        $venta['iva']=$venta['total']*.16;
        $id_venta= $this->unique_model->save("ventas_ventas",$venta);
        $id_articulo=0;
        foreach ($detalles as $index => $value){
            $regla = $this->unique_model->get_rows_where("*","ventas_reglas",['id_negocio' => $this->id_negocio(), 'estatus' =>1,'md5(id_articulo)' => $value['id_articulo']]);
            if (count($regla)>0){
                $regla = $regla[0];
                $existencia = $this->unique_model->get_rows_where("existencia","inventario_articulos",['id' => $regla['id_articulo_afectar']])[0]['existencia'];
                if ($regla['operador']==2)
                    $existencia-=($regla['cantidad_articulo_afectar']*$value['cantidad']);
                $this->unique_model->save("inventario_articulos",['id' => $regla['id_articulo_afectar'],'existencia' => $existencia],0);
            }
            $comun = $value['comun'];
            unset($detalles[$index]['comun']);
            unset($detalles[$index]['ui']);
            $detalles[$index]['id']=0;
            $detalles[$index]['id_venta']=$id_venta;
            if ($value['id_articulo']!="0"){
                $id_articulo =$this->unique_model->get_id_md5("inventario_articulos","id",$detalles[$index]['id_articulo'])['id'];
            }
            $detalles[$index]['id_articulo']=$id_articulo;
            $this->unique_model->save("ventas_ventas_detalles",$detalles[$index]);
            if ($value['id_articulo']!="0"){

                $existencia  = $this->unique_model->get_rows_where("existencia","inventario_articulos",['md5(id)'=> $value['id_articulo']])[0]['existencia'];
                $cantidad_unidad  = $this->unique_model->get_rows_where("cantidad_unidad","inventario_articulos",['md5(id)'=> $value['id_articulo']])[0]['cantidad_unidad'];
                if ($value['venta_unidad']==0){
                    if($comun==0 && $existencia>0){
                        $existencia-=$value['cantidad'];
                        $save =['id' =>$value['id_articulo'], 'existencia' =>$existencia];
                    }
                }
                else{
                    $existencia_unidad  = $this->unique_model->get_rows_where("existencia_unidad","inventario_articulos",['md5(id)'=> $value['id_articulo']])[0]['existencia_unidad'];
                    if ($value['tipo_venta']==1){
                        $existencia_unidad-=$value['cantidad'];
                        $existencia =floor($existencia_unidad/$cantidad_unidad);
                    }
                    else{
                        $existencia-=$value['cantidad'];
                        $existencia_unidad =($existencia_unidad - ($cantidad_unidad*$value['cantidad']));
                    }
                    $save =['id' =>$value['id_articulo'], 'existencia_unidad' =>$existencia_unidad,'existencia' => $existencia];
                }
                if ($comun==0 && $existencia > 0){
                    $this->unique_model->save("inventario_articulos",$save);
                }
            }
        }
        $data = $this->ticket(md5($id_venta));
        //$array =['url' =>base_url('ventas/ventas/ticket').'/'.md5($id_venta).'/'.md5(1)];
        //$venta
        $this->json($data);
        $this->notificaciones();
    }

    public function ticket($id_venta){
        $venta = $this->unique_model->query("
        SELECT ventas_ventas.tipo_pago AS id_tipo_pago, ventas_ventas.descuento,ventas_tipo_pago.nombre AS tipo_pago, ventas_descuentos_negocios.nombre AS tipo_descuento, ventas_ventas.fecha,ventas_ventas.importe,ventas_ventas.iva,ventas_ventas.tipo,folio_venta(ventas_ventas.id) as folio,sistema_usuarios.nombre,ventas_ventas.total,ventas_ventas.pago,ventas_ventas.cambio
        FROM ventas_ventas
        JOIN sistema_usuarios ON  ventas_ventas.id_usuario = sistema_usuarios.id
        JOIN ventas_descuentos_negocios ON ventas_descuentos_negocios.id = ventas_ventas.id_descuento
        JOIN ventas_tipo_pago ON ventas_tipo_pago.id = ventas_ventas.tipo_pago
        WHERE md5(ventas_ventas.id) = '$id_venta'");
        $detalles  = $this->unique_model->get_rows_where("descripcion,cantidad,subtotal,precio","ventas_ventas_detalles",['md5(id_venta)' => $id_venta]);
        $datos_ticket = $this->unique_model->get_rows_where("*","sistema_ticket",['id_negocio' => $this->id_negocio()])[0];
        $data = [
            'venta' => $venta[0],
            'detalles'=> $detalles,
            'datos_ticket' => $datos_ticket,
        ];
        $this->json($data);
        return $data;


    }



    public function getPrecioCopia(){
        $cantidad = $this->input->post('cantidad');
        $precio = $this->unique_model->get_rows_where("precio","ventas_copias",['min_cantidad <=' => $cantidad,'max_cantidad >=' => $cantidad]);
        $this->json($precio);
    }
    public function saveGasto(){
        $post=$this->input->post();
        $post['id'] = 0;
        $post['id_negocio'] = $this->id_negocio();
        $this->unique_model->save("ventas_gastos",$post);
    }
    public function saveSalida(){
        $post=$this->input->post();
        $ventas = $this->unique_model->get_sum_where('total','ventas_ventas',['tipo' =>1,'tipo_pago'=>1],sha1('1'),'','')[0]['total'];
        $gastos =$this->unique_model->get_sum_where('gasto','ventas_gastos',[],sha1('1'),'','')[0]['gasto'];
        $fondo = $this->unique_model->get_sum_where('fondo','ventas_fondos',[],sha1('1'),'','')[0]['fondo'];
        $salidas = $this->unique_model->get_sum_where('salida','ventas_salida',[],sha1('1'),'','')[0]['salida'];
        $caja = ($ventas + $fondo) - ($fondo + $salidas);
        $status = $caja >= $post['salida'];
        $post['id'] = 0;
        $post['id_negocio'] = $this->id_negocio();
        if ($status){
            $this->unique_model->save('ventas_salida',$post);
        }
        else {
            $this->json([
                'estatus' => 0,
                'message' => 'Dinero en caja insuficiente.'
            ]);
        }
    }

    public function saveFondo(){
        $post = $this->input->post();
        $post['id']=0;
        $post['id_negocio'] = $this->id_negocio();
        $this->unique_model->save('ventas_fondos',$post);
    }

    public function preferencias(){
        $query =[
            "SELECT"=>  ["preferencias_ventas.maneja_copias,,preferencias_ventas.codigo_barras_automatico,preferencias_ventas.codigo_barras_automatico_default,preferencias_ventas.inventario_estricto,preferencias_ventas.maneja_gastos,preferencias_ventas.nueva_nota,preferencias_ventas.boton_mayoreo,preferencias_paquetes.maneja_hora,preferencias_ventas.maneja_ticket"],
            "FROM" =>["preferencias_ventas"],
            "JOIN" => [
                ["preferencias_paquetes","preferencias_paquetes.id_negocio =preferencias_ventas.id_negocio",""]
            ],
            "WHERE" => [
                ["preferencias_ventas.id_negocio" , $this->id_negocio()]
            ]
        ];
        $preferencias = $this->unique_model->get_query($query);
        $tipos_pago = [
            "SELECT"=>  ["ventas_tipo_pago.id, ventas_tipo_pago.nombre AS value"],
            "FROM" =>["ventas_tipo_pago"],
            "JOIN" => [
                ["ventas_tipo_pago_negocios","ventas_tipo_pago_negocios.id_tipo_pago = ventas_tipo_pago.id",""]
            ],
            "WHERE" => [
                ["ventas_tipo_pago_negocios.id_negocio" , $this->id_negocio()],
                ["ventas_tipo_pago_negocios.estatus" ,1]

            ]
        ];
        $preferencias[0]['tipos_pago'] = $this->unique_model->get_query($tipos_pago);

        $descuentos = [
            "SELECT"=>  ["id,descuento, default,nombre as value"],
            "FROM" =>["ventas_descuentos_negocios"],
            "WHERE" => [
                ["ventas_descuentos_negocios.id_negocio" , $this->id_negocio()],
                ["ventas_descuentos_negocios.estatus" ,1]

            ],
            "ORDER BY" => ["descuento","ASC"]
        ];
        $preferencias[0]['descuentos'] = $this->unique_model->get_query($descuentos);
        $this->json($preferencias);

    }
}
