<?php
class Login extends MY_Controller{

    public function __construct(){
        parent::__construct();
    }

    public function index($negocio=""){
        $n= $this->unique_model->query("SELECT sistema_negocios.*, sistema_temas.nombre as tema FROM sistema_negocios JOIN sistema_temas on sistema_negocios.id_tema= sistema_temas.id WHERE sistema_negocios.url='$negocio'");
        if ($this->session->userdata("log")){
           redirect('inicio');
        }
        else if (count($n)>0){
            $data  = $this->session->userdata('log');
            $data['modulo'] = "Inicio de sesión";
            $data['log'] = 0;
            $data['nombre_negocio'] = $n[0]['nombre_negocio'];
            $data['logo'] = $n[0]['logo'];
            $data['tema'] = $n[0]['tema'];
            $data['id_negocio'] = md5(md5($n[0]['id']));
            $this->load->view("header",$data);
            $this->load->view("index");
            $this->load->view("login_view");
        }
    }

    public function getAccess(){
       /* $ip = $this->getRealIp();
            $intentos = $this->unique_model->query("SELECT COUNT(*)  AS num
FROM sistema_sesiones
WHERE TIMESTAMPDIFF(MINUTE,fecha,NOW()) <=15 AND estatus=0 AND ip  = '$ip'");*/
      //  print_r($intentos);
            //if ($intentos<=5){
                $estatus=0;
                $post = $this->input->post();
                $post['contrasena']=md5($post['contrasena']);
                $post['estatus'] = 1;
                $id_negocio = $post['id_negocio'];
                $id_usuario=0;
                $usuario= $post['usuario'];
                $contrasena= $post['contrasena'];
                unset($post['id_negocio']);
                $post['md5(md5(sistema_usuarios.id_negocio))'] = $id_negocio;
                $response  = $this->unique_model->get_rows_where("id,usuario,id_perfil","sistema_usuarios",$post);

                if(count($response)){
                    $profile  =$this->unique_model->get_rows_where("nombre","sistema_perfiles",array('id' =>$response[0]['id_perfil']));
                    $this->json(array('message'=>"Bienvenido: ".$response[0]['usuario'],'status'=>1,'url' =>base_url('inicio')));
                    $negocio= $this->unique_model->query("SELECT sistema_negocios.*, sistema_temas.nombre as tema FROM sistema_negocios JOIN sistema_temas on sistema_negocios.id_tema= sistema_temas.id WHERE md5(md5(sistema_negocios.id)) ='$id_negocio'");
                    $estatus=1;
                    $usuario="";
                    $contrasena="";
                    $id_usuario=$response[0]['id'];
                    $sess = array(
                        'id' => $response[0]['id'],
                        'usuario' => $response[0]['usuario'],
                        'perfil' =>$profile[0]['nombre'],
                        'id_perfil' => $response[0]['id_perfil'],
                        'log' =>1,
                        'tema' => $negocio[0]['tema'],
                        'nombre_negocio' => $negocio[0]['nombre_negocio'],
                        'logo' => $negocio[0]['logo'],
                        'id_negocio' => $negocio[0]['id']
                    );
                    $this->session->set_userdata($sess);
                }
                else{
                    $this->unique_model->json(array('message'=>"Usuario o contraseña incorrectos.",'status'=>0));
                }
                $this->unique_model->save("sistema_sesiones",array(
                    'id' =>0,
                    'id_usuario' => $id_usuario,
                    'ip' => $this->getRealIp(),
                    'estatus' => $estatus,
                    'usuario' => $usuario,
                    'contrasena' => $contrasena
                ),0);
           // }

    }

    public function sessDestroy(){
        $negocio = $this->negocio();
        $this->session->sess_destroy();
        redirect(base_url($negocio[0]['url']));
    }

    public function getModules($id_module){
        if($this->session->userdata('log')){
            $data = $this->session->userdata();
            $menu = array();
            $id_negocio = $this->id_negocio();
            $seccions = $this->unique_model->query("SELECT id,nombre as value, concat('".base_url('public/images/modules/')."','/',sistema_secciones.icono) as icono FROM sistema_secciones ORDER BY sistema_secciones.orden ASC");
            foreach ($seccions as $index => $seccion){
                $modules= $this->unique_model->query("
                    SELECT concat('".$seccion['id']."','.',sistema_modulos.id) as id,concat('".base_url('public/images/modules/')."','/',sistema_modulos.icono) as icono, sistema_modulos.nombre as value,sistema_modulos.id_seccion,concat('".base_url()."',sistema_modulos.url) as url
                    FROM sistema_modulos
                    JOIN sistema_modulos_perfiles ON sistema_modulos_perfiles.id_modulo = sistema_modulos.id
                    WHERE sistema_modulos_perfiles.id_modulo  IN (SELECT id_modulo FROM `sistema_modulos_negocios` WHERE id_negocio =$id_negocio ) AND id_seccion = $seccion[id] AND sistema_modulos_perfiles.id_perfil = $data[id_perfil] AND sistema_modulos_perfiles.estatus=1 AND sistema_modulos_perfiles.id_negocio = $id_negocio
                ");
                $seccions[$index]['data']= $modules;
                if (!count($seccions[$index]['data']))
                    unset($seccions[$index]);
            }
            foreach ($seccions as $item)
                array_push($menu,$item);
            $this->json($menu);
        }
    }

}
?>
