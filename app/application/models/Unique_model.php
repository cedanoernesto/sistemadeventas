<?php
class Unique_model extends CI_Model
{
	public function __construct(){
		parent::__construct();
	}
    #start
    public function get($table){
    	return $this->db->get($table)->result_array();
    }
    #end
    public function get_query($configs){
       foreach ($configs as $config => $valor){
       		if (count($valor)>0 && isset($configs['SELECT']) && $config=="SELECT"){
       			foreach ($valor as $confi){
       			$this->db->select($confi);
				}
			}
			else if (count($valor)>0 && isset($configs['FROM']) && $config=="FROM"){
				$this->db->from($configs['FROM'][0]);
			}
			else if (count($valor)>0 && $config == "JOIN"){
				foreach ($valor as $confi) {
				    $this->db->join($confi[0],$confi[1],$confi[2]);
				}
			}
            else if(count($valor)>0 && $config == "GROUP BY"){
            	$this->db->group_by($configs['GROUP BY'][0]);
            }
			else if (count($valor)>0 && $config == "WHERE"){
				foreach ($valor as $confi) {
					if (isset($confi[1]))
						$this->db->where(array($confi[0] => $confi[1]));
					else
						$this->db->where($confi[0]);
				}
			}
			else if (count($valor)>0 && $config == "WHERE IN"){
				foreach ($valor as $valo => $val){
					$this->db->where_in($val[0],$val[1]);
				}
			}
			else if (count($valor)>0 && $config == "WHERE NOT IN"){
				foreach ($valor as $valo => $val){
					$this->db->where_not_in($valo,$val);
				}
			}
			else if (count($valor)>0 && $config == "DISTINCT"){
				$this->db->distinct();
			}
			else if (count($valor)>0 && $config == "LIKE"){
				foreach ($valor as $confi) {
					$this->db->like(array($confi[0]=>$confi[1]));
				}
			}
			else if (count($valor)>0 && $config == "OR LIKE"){
				foreach ($valor as $confi) {
					$this->db->or_like(array($confi[0]=>$confi[1]));
				}
			}
			 else if(count($valor)>0 && $config == "ORDER BY"){
					 $this->db->order_by($configs['ORDER BY'][0],$configs['ORDER BY'][1]);

            }
             else if(count($valor)>0 && $config == "LIMIT"){
                 if (count($valor)==1){
                     $this->db->limit($configs['LIMIT'][0]);
                 }
           		 else {
           		     $this->db->limit($configs['LIMIT'][0],$configs['LIMIT'][1]);
           		 }
            }
   		}
                if (count($valor)>0 && $config == "RETURN"){
						if ($configs['RETURN'][0]=="num_rows"){
		                	 return $this->db->get()->num_rows();
		               	}
		               	else if ($configs['RETURN'][0]=="result"){
	                	 return $this->db->get()->result();
	                	}
                }
                 else{
                	return $this->db->get()->result_array();
                }
    }

	public function query($query){
		return $this->db->query($query)->result_array();
	}
 	public function get_id_md5($table,$field,$id){
	 	$this->db->select("$field as $field");
	 	$this->db->where("md5(".$field.")",$id);
	 	return $this->db->get($table)->row_array();
 	}
 	public function get_num_rows($table){
 		return $this->db->get($table)->num_rows();
 	}

 	public function get_num_rows_where($table,$fields){
         return $this->db->get_where($table,$fields)->num_rows();
 	}

 	public function get_num_equals_id($tabla,$fields,$id){
 		$this->db->select("id");
		unset($fields['id']);
 		$this->db->where($fields);
 		$this->db->where("md5(id) <>",$id);
 	    return $this->db->get($tabla)->num_rows();
 	}
 	public function get_row($select,$table,$fields){
 		$this->db->select($select);
 		$this->db->from($table);
 		$this->db->where($fields);
 		return $this->db->get()->row_array();
 	}

 	public function get_rows($select,$table){
 		$this->db->select($select);
 		$this->db->from($table);
 		return $this->db->get()->result_array();
 	}

 	public function get_rows_where($select,$table,$fields){
 		$this->db->select($select);
 		$this->db->from($table);
 		$this->db->where($fields);
 		return $this->db->get()->result_array();
 	}

 	public function get_max_row($select,$table){
 		$this->db->select_max($select);
 		$this->db->from($table);
 		return $this->db->get()->row_array();
 	}

	public function get_sum_where($field,$table,$where,$tipo,$fecha_inicio,$fecha_fin){
		$session = $this->session->userdata();
		$this->db->select("IFNULL(SUM($field),0) as $field ");
		if ($tipo==sha1('1'))
		$this->db->where("DATE(fecha) = DATE(NOW())");
		else if ($tipo==sha1('2'))
			$this->db->where("WEEK(fecha) = WEEK(NOW())");
		else if ($tipo==sha1('3'))
			$this->db->where("MONTH(fecha) = MONTH(NOW())");
		else if ($tipo==sha1('4'))
			$this->db->where("MONTH(fecha) = MONTH(NOW())");
		else if ($tipo==sha1('5'))
			$this->db->where("DATE(fecha) = DATE($fecha_inicio)");

		else {
			$this->db->where("DATE(fecha) >=",$fecha_inicio);
			$this->db->where("DATE(fecha) <=",$fecha_fin);
		}
		$this->db->where($where);
		$this->db->where('id_negocio',$session['id_negocio']);
		$this->db->where('estatus',1);
		return $this->db->get($table)->result_array();
	}
	public function get_num_where($table,$where,$tipo,$fecha_inicio,$fecha_fin){
		$session = $this->session->userdata();
		if ($tipo==sha1('1'))
			$this->db->where("DATE(fecha) = DATE(NOW())");
		else if ($tipo==sha1('2'))
			$this->db->where("WEEK(fecha) = WEEK(NOW())");
		else if ($tipo==sha1('3'))
			$this->db->where("MONTH(fecha) = MONTH(NOW())");
		else if ($tipo==sha1('4'))
			$this->db->where("MONTH(fecha) = MONTH(NOW())");
        else if ($tipo==sha1('5'))
            $this->db->where("DATE(fecha) = DATE($fecha_inicio)");
		else {
			$this->db->where("DATE(fecha) >=",$fecha_inicio);
			$this->db->where("DATE(fecha) <=",$fecha_fin);
		}
		$this->db->where($where);
		$this->db->where('id_negocio',$session['id_negocio']);
		$this->db->where('estatus',1);
		return $this->db->get($table)->num_rows();
	}

	public function save($tabla,$datos,$tipo=1){
		$id=0;
		$datos['id'] =(!isset($datos['id']))?0:$datos['id'];
		if ($datos['id']==md5("0") || $datos['id']=="0"){
			unset($datos['id']);
			$this->db->insert($tabla,$datos);
			$id=$this->db->insert_id();
		}
		else{
			if ($datos['id']!="*")
				$this->db->where("md5(id)",$datos['id']);
				$id=$datos['id'];
				unset($datos['id']);
			$this->db->update($tabla,$datos);
		}
			if ($tipo)
				$this->json(array('id' =>$id,'id_md5' => md5($id),'message'=>'Datos guardados correctamente','estatus' =>1));
			return $id;
	}
	public function update($tabla,$where,$datos){
			$this->db->where($where);
			$this->db->update($tabla,$datos);
	}
		public function json($data){
 		      $this->output
      			->set_content_type('application/json')
      			->set_output(json_encode($data));
 	}


	
}
