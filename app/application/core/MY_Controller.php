<?php
class MY_Controller extends CI_Controller
{
  function __construct(){
    parent::__construct();
    date_default_timezone_set('America/Mazatlan');
    //$this->notificaciones();
    $this->notificaciones_apartados();
  }
protected function verify(){
  if (!$this->session->userdata('log')){
    redirect(base_url());
  }
 /* else{
    $negocio = $this->negocio();
    redirect(base_url($negocio[0]['url']));
  }*/

}

  protected function notificaciones(){
    $this->unique_model->query("SELECT notificaciones(".$this->id_negocio().")");
    $this->unique_model->query("SELECT notificaciones_agotado(".$this->id_negocio().")");
  }
  protected function log($id_modulo){
    $this->unique_model->save("sistema_acceso_modulos",array(
        'id' =>0,
        'id_accion'=> 1,
        'id_negocio' => $this->id_negocio(),
        'id_modulo' => $id_modulo,
        'id_usuario' => $this->id_usuario(),
        'ip' => $this->getRealIp()
    ),0);
  }
protected function is_ajax(){
  if(!$this->input->is_ajax_request())
    redirect('login');
  }
  protected function json($array){
        $this->output
        ->set_content_type('application/json')
        ->set_output(json_encode($array));
  }
  protected function access($id_modulo){
    if(!$this->unique_model->get_num_rows_where("sistema_modulos_perfiles",array('id_modulo' => $id_modulo,'id_perfil'=> $this->session->userdata('id_perfil'),'estatus' => 1)))
          redirect('inicio');
  }

  protected function negocio(){
    $session = $this->session->userdata();
    return  $this->unique_model->query("SELECT sistema_negocios.*, md5(sistema_negocios.id_tema) as id_tema,sistema_temas.nombre as tema FROM sistema_negocios JOIN sistema_temas on sistema_negocios.id_tema= sistema_temas.id WHERE sistema_negocios.id=$session[id_negocio]");
  }

  protected function id_negocio(){
    $session = $this->session->userdata();
    return $session['id_negocio'];
  }

  protected function id_usuario(){
    $session = $this->session->userdata();
    return $session['id'];
  }

  protected function refresh_negocio(){
    $session = $this->session->userdata();
    $negocio  =$this->unique_model->query("SELECT sistema_negocios.*, md5(sistema_negocios.id_tema) as id_tema,sistema_temas.nombre as tema FROM sistema_negocios JOIN sistema_temas on sistema_negocios.id_tema= sistema_temas.id WHERE sistema_negocios.id=$session[id_negocio]");
    $this->session->unset_userdata("tema");
    $this->session->set_userdata(array(
            'tema' => $negocio[0]['tema'],
            'nombre_negocio' => $negocio[0]['nombre_negocio'],
        )
    );
  }

  protected function getRealIP(){

    if( isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '' )
    {
      $client_ip =
          ( !empty($_SERVER['REMOTE_ADDR']) ) ?
              $_SERVER['REMOTE_ADDR']
              :
              ( ( !empty($_ENV['REMOTE_ADDR']) ) ?
                  $_ENV['REMOTE_ADDR']
                  :
                  "unknown" );
      $entries = preg_split('/[, ]/', $_SERVER['HTTP_X_FORWARDED_FOR']);

      reset($entries);
      while (list(, $entry) = each($entries))
      {
        $entry = trim($entry);
        if ( preg_match("/^([0-9]+\.[0-9]+\.[0-9]+\.[0-9]+)/", $entry, $ip_list) )
        {
          // http://www.faqs.org/rfcs/rfc1918.html
          $private_ip = array(
              '/^0\./',
              '/^127\.0\.0\.1/',
              '/^192\.168\..*/',
              '/^172\.((1[6-9])|(2[0-9])|(3[0-1]))\..*/',
              '/^10\..*/');
          $found_ip = preg_replace($private_ip, $client_ip, $ip_list[1]);
          if ($client_ip != $found_ip)
          {
            $client_ip = $found_ip;
            break;
          }
        }
      }
    }
    else
    {
      $client_ip =
          ( !empty($_SERVER['REMOTE_ADDR']) ) ?
              $_SERVER['REMOTE_ADDR']
              :
              ( ( !empty($_ENV['REMOTE_ADDR']) ) ?
                  $_ENV['REMOTE_ADDR']
                  :
                  "unknown" );
    }
    return $client_ip;
  }
  public function notificaciones_apartados(){
    $apartados =$this->unique_model->get_rows_where("id,id_cliente,fecha_vencimiento,fecha_vencimiento_tolerancia","ventas_apartados",array('estatus' =>0,'notificacion' => 0));
    foreach ($apartados as $apartado){
      $fecha_vencimiento = date_create($apartado['fecha_vencimiento']);
      $fecha_actual = date_create(date('Y-m-d'));
      $cliente  = $this->unique_model->get_rows_where("nombre","inventario_clientes",array('id' => $apartado['id_cliente']))[0]['nombre'];
      $dif = $fecha_actual->diff($fecha_vencimiento);
      $dif =$dif->days;
      if ($dif==7 || $dif==3 || $dif==1){
        if ($dif ==7 || $dif==3)
          $descripcion ="El apartado del cliente: $cliente esta a $dif dia(s) de vencerse.";
        else
          $descripcion ="El apartado del cliente: $cliente vence el dia de mañana.";
        $registrado = $this->unique_model->get_rows_where("id","sistema_notificaciones_apartados",array('dia' => $dif,'id_apartado' => $apartado['id'],'tipo' =>1));
        if (!count($registrado)){
          $this->unique_model->save("sistema_notificaciones_apartados",array('id' =>0, 'dia' => $dif, 'id_apartado' =>$apartado['id'],'tipo'=>1),0);
          $this->unique_model->save("sistema_notificaciones",array('id'=>0,'descripcion' =>$descripcion,'id_negocio' =>$this->id_negocio()),0);
        }
      }
      else if ($dif==0){
        $registrado = $this->unique_model->get_rows_where("id","sistema_notificaciones_apartados",array('dia' => $dif,'id_apartado' => $apartado['id'],'tipo' =>1));
        //echo count($registrado);
        if (!count($registrado)){
          $this->unique_model->save("sistema_notificaciones_apartados",array('id' =>0, 'dia' => $dif, 'id_apartado' =>$apartado['id'],'tipo'=>1),0);
          $this->unique_model->save("sistema_notificaciones",array('id'=>0,'descripcion' =>"El apartado del cliente: $cliente vence hoy.",'id_negocio' =>$this->id_negocio()),0);
        }
        $fecha_vencimiento = date_create($apartado['fecha_vencimiento_tolerancia']);
        $fecha_actual = date_create(date('Y-m-d'));
        $dif = $fecha_actual->diff($fecha_vencimiento);
        $dif =$dif->days;
        if ($dif==7 || $dif==3 || $dif==1){
          if ($dif ==7 || $dif==3)
            $descripcion ="El apartado del cliente: $cliente se dara de baja en $dif dias.";
          else
            $descripcion ="El apartado del cliente: $cliente se dara de baja el dia de mañana.";
          $registrado = $this->unique_model->get_rows_where("id","sistema_notificaciones_apartados",array('dia' => $dif,'id_apartado' => $apartado['id'],'tipo' =>2));
          if (!count($registrado)){
            $this->unique_model->save("sistema_notificaciones_apartados",array('id' =>0, 'dia' => $dif, 'id_apartado' =>$apartado['id'],'tipo'=>2),0);
            $this->unique_model->save("sistema_notificaciones",array('id'=>0,'descripcion' =>$descripcion,'id_negocio' =>$this->id_negocio()),0);
          }
        }
        else if ($dif==0){
          $registrado = $this->unique_model->get_rows_where("id","sistema_notificaciones_apartados",array('dia' => $dif,'id_apartado' => $apartado['id'],'tipo' =>2));
          if (!count($registrado)){
            $this->unique_model->save("sistema_notificaciones_apartados",array('id' =>0, 'dia' => $dif, 'id_apartado' =>$apartado['id'],'tipo'=>2),0);
            $this->unique_model->save("sistema_notificaciones",array('id'=>0,'descripcion' =>"El apartado del cliente: $cliente se dio de baja.",'id_negocio' =>$this->id_negocio()),0);
            $this->unique_model->save("ventas_apartados",array('id' => md5($apartado['id']),'estatus' => 2),0);
            $this->unique_model->save("inventario_clientes",array('id' => md5($apartado['id_cliente']), 'estatus' => 3),0);
          }
        }

      }
      // $this->unique_model->save("sistema_notificaciones",array('id'=>0,'descripcion' =>"El apartado del cliente: $cliente vence hoy.",'id_negocio' =>$this->id_negocio()),0);
      // $this->unique_model->save("ventas_apartados",array('id' => md5($apartado['id']),'estatus' => 2),0);
      // $this->unique_model->save("inventario_clientes",array('id' => md5($apartado['id_cliente']), 'estatus' => 3));
    }
  }

}
