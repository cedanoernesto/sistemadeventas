var generic_pager={
  template:"{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
  container:"pager",
  id:"pager",
  animate:{
    subtype:"out"
  },
  css:"pager",
  size:15,
  group:5
};
var generic_pager_option={
  template:"{common.first()} {common.prev()} {common.pages()} {common.next()} {common.last()}",
  id:"pager_option",
  css:"pager",
  size:10,
  group:5
};

var alert_error={
  type:"alert-error",
  text:"Ingrese los campos requeridos.",
  ok:"Aceptar"
};

var red_required={
  view:"label",
  label:"*",
  css:"red_required",
  width:15
};

var label_required={
  cols:[
    red_required,
    {view:"label",label:"  Campos requeridos",width:150}
  ]
};


var btn_close_window={
  view:"button",
  type:"image",
  image:BASE_URL_ICONS+"/cancel24.png",
  width:40,
  borderless:true,
  click:function(){
    this.getTopParentView().hide();
  }
};

var toolbar_header={
  view:"toolbar",
  cols:[
    {view:"label",label:"Modulo",id:"label_toolbar_header",align:"center"},
    btn_close_window
  ]
};

var generic_window={
  view:"window",
  id:"generic_window",
  width:600,
  height:400,
  modal:true,
  head:toolbar_header,
  position:"center",
}
var generic_toolbar={
  view:"toolbar",
  container:"toolbar",
  cols:[
    {view:'icon',css:'logo',width:76,height:65},
    {view:'label',label:"Sistema de ventas",width:155,css:'label_sistema'},
    {view:"label",label:"",id:"label_date",css:"label_date",width:400},
    {},
    {
      align:'center',
      rows:[
        {height:1},
        {view:"label",label:"",id:"label_clock",css:"label_clock",width:250},
      ]
    },
    {},
    {
      rows:[
        {height:1},
        {view:"button",label:"Nuevo",width:110,id:"generic_button_new",type:"imageButton",image:BASE_URL_ICONS+"new.png"},
      ]
    }
  ]
};
var calendar={
  view:"window",
  id:"calendar",
  width:600,
  height:400,
  body:{view:"calendar"},
  move:true,
  head:{
    view:"toolbar",
    cols:[
      {view:"label",label:"Calendario",align:"center"},
      {
        view:"icon",
        icon:"times-circle",
        width:70,
        click:function(){
          this.getTopParentView().hide();
        }
      }
    ]
  },
  position:"center",
};

