URL_SAVE_CATEGORIAS= BASE_URL +"inventario/categorias/save";
function validate(){
    if($$('form_categorias').validate()){
        $$('cargando').show();
        var data = $$('form_categorias').getValues();
        delete data['estatus'];
        delete data['$css'];
        webix.ajax().post(URL_SAVE_CATEGORIAS,data,function(response){
            $$('cargando').hide();
            response =JSON.parse(response);
            var m ={
                text:response.message,
                ok:"Aceptar"
            }
            if(!response.estatus)
                m.type="alert-error";
            else{
                $$('generic_window').hide();
                $$('datatable_categorias').clearAll();
                $$('datatable_categorias').load(URL_GET_CATEGORIAS);
            }
            webix.alert(m);
        });
    }
    else
        webix.alert(alert_error);
}

var form_categorias={
    view:"form",
    id:"form_categorias",
    elements:[
        {rows:[
            {cols:[
                {view:"text",id:"id",name:"id",hidden:true,value:"0"},
                {view:"text",label:"Nombre de la categoria",id:"nombre",name:"nombre",required:true},

            ]},
            {cols:[
                label_required,
                {view:"button",width:150,id:"btn_change_estatus",click:function(){
                    var text = "¿Desea activar este cliente?"
                    var estado=1;
                    var item = $$('datatable_categorias').getSelectedItem();
                    if (item.estado==1){
                        text= "¿Desea desactivar esta categoria?";
                        estado=2;
                    }
                    changeStatus(URL_SAVE_CATEGORIAS,{id:item.id,estatus:estado},text,function(response){
                        response =JSON.parse(response);
                        webix.alert(response.message);
                        $$('generic_window').hide();
                        $$('datatable_categorias').load(URL_GET_CATEGORIAS);
                    });
                }},
                {},
                {view:"button",type:"imageButton",id:'button_save_categorias',image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
            ]}
        ]}
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "nombre":webix.rules.isNotEmpty,
    },
    on:{
        onSubmit:validate,
    }
};
toolbar_header['cols'][0].label="Información de la categoria";
generic_window.width=500;
generic_window.body=form_categorias;
