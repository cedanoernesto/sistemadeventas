URL_TIPOS_VENTAS= BASE_URL +"inventario/articulos/getTiposVenta";
URL_CATEGORIAS= BASE_URL +"inventario/articulos/selectCategorias";
URL_CANTIDADES =BASE_URL +"inventario/articulos/getCantidades";
URL_PROVEEDORES=BASE_URL +"inventario/articulos/getProveedores";
URL_CONTENEDOR =BASE_URL +"inventario/articulos/selectContenedor/0";
URL_SAVE_ARTICULOS= BASE_URL +"inventario/articulos/save";
URL_CANTIDADES_RESUMEN =BASE_URL + "inventario/articulos/getNumCantidades";
URL_SAVE_CANTIDADES =BASE_URL + "inventario/articulos/saveCantidades";

function nuevo_articulo(){
  $$('generic_window').show();
  $$('descripcion').focus();
  $$('form_articulos').clear();
  $$('form_articulos').clearValidation();
  $$('btn_change_estatus').hide();
  $$('id').setValue("0");
  onCheck();
}

function set0(id) {
  if ($$(id).getValue()=='' || isNaN($$(id).getValue())){
    $$(id).setValue('0');
  }

}

function cantidadUnidad(){
  if ($$('venta_unidad').getValue()){
    $$('existencia_unidad').setValue(parseFloat($$('existencia').getValue())*parseFloat($$('cantidad_unidad').getValue()));
  }

}


function validateCheckEmpty(value){
  console.log(value);
  if ($$('venta_unidad').getValue() && $$('id_contenedor').getText()==""){
    return false;
  }
  else{
    return true;
  }

}
function validateCheck(value){
  if ($$('venta_unidad').getValue() && !webix.rules.isNumber(value)){
    return false;
  }

  else{
    return true;
  }

}
function validate(){
  if($$('form_articulos').validate()){
    $$('cargando').show();
    var data =$$('form_articulos').getValues();
    if(data['id']!=0 || clonar){
      delete data['estatus'];
      delete data['tipo_venta'];
      delete data['estado'];
      delete data['$css'];
      delete data['unidades_sueltas'];
    }
    if (!obj_pref.maneja_proveedor)
      delete data['id_proveedor'];
    if (id_tipo_venta!=0)
      data['id_tipo_venta'] = id_tipo_venta;
    data['id']=(data['id'] =='')?0: data['id'];
    webix.ajax().post(URL_SAVE_ARTICULOS,data,function(response){
      $$('cargando').hide();
      response =JSON.parse(response);
      var m ={
        text:response.message,
        ok:"Aceptar"
      }
      if(!response.estatus)
        m.type="alert-error";
      else{
        $$('generic_window').hide();
        $$('datatable_articulos').clearAll();
        $$('datatable_articulos').load(URL_GET_ARTICULOS);
        $$('datatable_articulos').unselect($$('datatable_articulos').getSelectedId());
      }
      webix.alert(m);
    });
  }
  else
    webix.alert(alert_error);
}
function onCheck(){
  if($$('venta_unidad').getValue()){
    $$('id_contenedor').focus();
    $$('cantidad_unidad').define("disabled",false);
    $$('precio_unidad').define("disabled",false);
    $$('precio_unidad_mayoreo').define("disabled",false);
    $$('id_contenedor').define("disabled",false);
    $$('cantidad_unidad_mayoreo').define("disabled",false);
    $$('unidades_sueltas').define("disabled",false);
  }
  else{
    $$('cantidad_unidad').setValue("");
    $$('precio_unidad').setValue("");
    $$('cantidad_unidad').define("disabled",true);
    $$('precio_unidad').define("disabled",true);
    $$('precio_unidad_mayoreo').define("disabled",true);
    $$('id_contenedor').define("disabled",true);
    //$$('existencia_unidad').define("disabled",true);
    $$('unidades_sueltas').define("disabled",true);
    $$('cantidad_unidad_mayoreo').define("disabled",true);
  }
}

function sendDatatable(){
  var data =[];
  $$('datatable_cantidades').eachRow(function(row){
    var item = this.getItem(row);
    data.push({
      id:item.id,
      cantidad:item.cantidad
    });
  });

  $$('cargando').show();
  webix.ajax().post(URL_SAVE_CANTIDADES,{cantidades:data},function(response){
    response =JSON.parse(response);
    if(response.estatus){
      $$('cargando').hide();
      $$('window_cantidades').hide();
      webix.alert(response.message);
    }
  });

}

var contextmenu_articulos={
  view:"contextmenu",
  id:"cmenu",
  data:[
    {id:1,value:'Editar'},
    {id:2,value:'Clonar'},
    /*"Delete",*/
    {$template:"Separator" },
    {id:3,value:"Cancelar"}
  ],
  on:{
    onItemClick:function(id){
      id = parseInt(id);
      var item  = $$('datatable_articulos').getSelectedItem();
      switch (id){
        case 1:
          nuevo_articulo();
          $$('form_articulos').setValues(item);
          onCheck();
          break;
        case 2:
          nuevo_articulo();
          var item  = $$('datatable_articulos').getSelectedItem();
          clonar=0;
          if (item!=undefined &&  obj_pref.config_clonar){
            clonar=1;
            $$('form_articulos').setValues(item);
            $$('id').setValue('0');
          }
          break;
        case 3:
          $$('cmenu').hide();
          $$('datatable_articulos').unselect($$('datatable_articulos').getSelectedId());
          $$('datatable_articulos').refresh();
          break;

      }
    }
  }
};
var datatable_cantidades={
  view:"datatable",
  id:"datatable_cantidades",
  select:"row",
  editable:true,
  width:200,
  columns:[
    {id:"id",hidden:true},
    {id:"cantidad",header:["Cantidad",{content:"serverFilter"}],fillspace:true,editor:"text"}
  ]
};

var datatable_cantidades_resumen={
  view:"datatable",
  id:"datatable_cantidades_resumen",
  width:250,
  columns:[
    {id:"cantidad",header:"Cantidad de cajas",fillspace:true},
    {id:"piezas",hedaer:"Piezas",width:70}
  ]
};
var window_cantidades ={
  view:"window",
  id:"window_cantidades",
  width:600,
  height:400,
  modal:true,
  head:{view:"toolbar",cols:[{view:"label",label:"Cantidades"},{
    view:"icon",
    icon:"times-circle",
    width:70,
    click:function(){
      this.getTopParentView().hide();
    }
  }]},
  body:{
    rows:[
      {
        cols:[
          datatable_cantidades,
          datatable_cantidades_resumen
        ]
      },
      {cols:[
        {},
        {view:"button",label:"Aceptar",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100,click:sendDatatable }
      ]}
    ]
  },
  position:"center"
};

var form_articulos={
  view:"form",
  id:"form_articulos",
  elements:[
    {rows:[
      {cols:[
        {view:"text",id:"id",name:"id",hidden:true,value:"0"},
        {view:"textarea",label:"Descripción",id:"descripcion",name:"descripcion",required:true,invalidMessage:required,on:{
          onChange:function(nv,ov){
            this.setValue(nv.toUpperCase());
          }
        }},
        {width:30},
        {width:30},
        {
          id:'cont_id_tipo_venta',
          width:200,
          rows:[
            {view:"combo",label:"Tipo de venta",name:"id_tipo_venta",id:"id_tipo_venta",invalidMessage:required,width:200,required:true}
          ]
        },
        {width:30}
      ]},
      {cols:[
        {view:"text",label:"Precio de compra",id:"precio_compra",name:"precio_compra",invalidMessage:valid_number,width:150,required:true,on:{
          onBlur:function(){
            if (obj_pref.aplicar_iva && this.getValue!='' && webix.rules.isNumber(this.getValue())){
              if (!clonar && $$('id').getValue()=='0')
                this.setValue((parseFloat(this.getValue())*1.16).toFixed(2));
            }

            if(webix.rules.isNumber(this.getValue()) &&  $$('precio_venta').getValue()==''){
              var np = (obj_pref.sugerir_precio_venta!="0")?((parseFloat(this.getValue())*obj_pref.sugerir_precio_venta)).toFixed(2):'';
              if (!clonar && $$('id').getValue()=='0')
                $$('precio_venta').setValue(np);
            }

              if ($$('precio_mayoreo').getValue()==''){
                  if (obj_pref.desc_mayoreo!=0)
                      $$('precio_mayoreo').setValue((parseFloat($$('precio_compra').getValue())*obj_pref.desc_mayoreo).toFixed(2));
                  else
                      $$('precio_mayoreo').setValue($$('precio_compra').getValue());
              }
          }
        }},
        {width:20},
        {view:"text",label:"Precio de venta (Pùblico)",id:"precio_venta",invalidMessage:valid_number,name:"precio_venta",width:180,required:true,on:{
          onBlur:function(){
          }
        }},
        {width:20},
        {view:"text",label:"Precio mayoreo",id:"precio_mayoreo",name:"precio_mayoreo",invalidMessage:valid_number,required:true,
        on:{
            onBlur:function(){
                if (this.getValue()==''){
                  this.setValue($$('precio_venta').getValue())
                }

            }
        }},
        {view:"text",label:"Cantidad para mayoreo",id:"cantidad_mayoreo",name:"cantidad_mayoreo",invalidMessage:valid_number,required:true,on:{
          onBlur:function(){
            if (this.getValue()=="")
              this.setValue("0");
          }
        }},
      ]},
      {cols:[
        {view:"text",label:"Existencia actual",id:"existencia",name:"existencia",invalidMessage:valid_number,required:true,
          on:{
            onBlur:function () {
              cantidadUnidad(this.getValue());
              set0('existencia');
            }
          }
        },
        {width:20},
        {view:"text",label:"Existencia min",id:"existencia_minima",invalidMessage:valid_number,name:"existencia_minima",required:true,on:{
          onBlur:function () {
            set0('existencia_minima');
          }
        }},
        {view:"combo",label:"Categoria",id:"id_categoria",invalidMessage:required,name:"id_categoria",required:true,options:URL_CATEGORIAS},
        {width:20},
        {view:"text",label:"Codigo de barras",id:"codigo_barras",name:"codigo_barras",width:300,invalidMessage:"Solo valores numéricos"},
      ]},
      {
        id:'cont_id_proveedor',
        cols:[
          {view:'combo',label:'Proveedor',id:'id_proveedor',name:'id_proveedor',required:true,invalidMessager:required,options:URL_PROVEEDORES},
          {}
        ]
      },
      {view:"fieldset",
        label:"Venta por unidad",
        id:'cont_venta_unidad',
        body:{
          rows:[
            {cols:[
              {view:"checkbox",label:"Venta unitaria",id:"venta_unidad",name:"venta_unidad",width:100,align:"center",click:"onCheck()"},
              {view:"combo",label:"Contenedor",id:"id_contenedor",name:"id_contenedor",width:180,options:URL_CONTENEDOR,disabled:true,invalidMessage:required,on:{
                onChange:function(){
                  var tipo =$$('id_tipo_venta').getText()+'S';
                  $$('cantidad_unidad').define('label','Cantidad de ' +tipo + ' por ' + this.getText());
                  $$('cantidad_unidad').refresh();
                  $$('precio_unidad_mayoreo').define('label','Precio de mayoreo por ' + $$('id_tipo_venta').getText());
                  $$('precio_unidad_mayoreo').refresh();
                  $$('precio_unidad').define('label','Precio por ' + $$('id_tipo_venta').getText());
                  $$('precio_unidad').refresh();
                  $$('existencia_unidad').define('label','Existencia total de '+$$('id_tipo_venta').getText()+'S');
                  $$('existencia_unidad').refresh();
                  $$('unidades_sueltas').define('label',$$('id_tipo_venta').getText() +'S' +' sueltas');
                  $$('unidades_sueltas').refresh();
                }
              }},
              {view:"text",label:"Cantidad de piezas",id:"cantidad_unidad",name:"cantidad_unidad",width:250,disabled:true,invalidMessage:valid_number,on:{
                onChange:cantidadUnidad
              }},
              {width:20},
              {view:"text",label:"Precio/unidad",id:"precio_unidad",name:"precio_unidad",width:150,disabled:true,invalidMessage:valid_number,on:{
                onChange:function(){
                  $$('precio_unidad_mayoreo').setValue(this.getValue());
                }
              }},
              {width:100}
            ]},
            {cols:[
              {view:"text",label:"Precio may/unidad",id:"precio_unidad_mayoreo",name:"precio_unidad_mayoreo",width:220,disabled:true,invalidMessage:valid_number},
              {view:"text",label:"Cantidad para mayoreo",id:"cantidad_unidad_mayoreo",name:"cantidad_unidad_mayoreo",width:170,disabled:true,invalidMessage:valid_number,on:{
                onBlur:function(){
                  if (this.getValue()=="")
                    this.setValue("0");

                }
              }},
              {view:"text",label:"Unidades sueltas",id:"unidades_sueltas",name:"unidades_sueltas",width:180,invalidMessage:valid_number,disabled:false,on:{
                onchange:function(){
                  if (this.getValue()=="")
                    this.setValue("0");
                  if ($$('venta_unidad').getValue() && this.getValue()!="" && !isNaN(this.getValue()))
                    $$('existencia_unidad').setValue(parseFloat($$('existencia_unidad').getValue())+ parseFloat(this.getValue()));
                }
              }},
              {view:"counter",label:"Existencia/contenedores",id:"existencia_unidad",name:"existencia_unidad",width:400,invalidMessage:valid_number},
              /*{view:"button",label:"Ver cantidades",width:130,height:50,click:function(){
               $$('window_cantidades').show();
               $$('datatable_cantidades').clearAll();
               $$('datatable_cantidades').load(URL_CANTIDADES+"/"+$$('id').getValue());
               $$('datatable_cantidades_resumen').clearAll();
               $$('datatable_cantidades_resumen').load(URL_CANTIDADES_RESUMEN+"/"+$$('id').getValue());
               },disabled:true,id:"btn_ver_cantidades"}*/

            ]},
          ]
        }
      },
      {cols:[
        label_required,
        {view:"button",type:"danger",label:"Desactivar producto",width:150,id:"btn_change_estatus",click:function(){
          var text = "¿Desea activar este producto?"
          var estado=1;
          var item = $$('datatable_articulos').getSelectedItem();
          if (item.estado==1){
            text= "¿Desea desactivar este producto? \n Al hacer esto no podra utilizarlo mas adelante y todos los paquetes dependientes de el se desactivaran automaticamente.";
            estado=2;
          }
          changeStatus(URL_SAVE_ARTICULOS,{id:item.id,estatus:estado},text,function(response){
            response =JSON.parse(response);
            webix.alert(response.message);
            $$('generic_window').hide();
            $$('datatable_articulos').load(URL_GET_ARTICULOS);
          });
        }},
        {},
        {view:"button",type:"imageButton",image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
      ]}
    ]}
  ],
  elementsConfig:{
    labelPosition:"top"
  },
  rules:{
    "codigo_barras":isNumeric,
    "descripcion":webix.rules.isNotEmpty,
    "id_categoria":webix.rules.isNotEmpty,
    "id_tipo_venta":webix.rules.isNotEmpty,
    "precio_compra":webix.rules.isNumber,
    "precio_venta":webix.rules.isNumber,
    "existencia":webix.rules.isNumber,
    "existencia":webix.rules.isNumber,

    "existencia_minima":webix.rules.isNumber/*function(value){
     if (webix.rules.isNumber(value) && parseInt(value)<=parseInt($$('existencia').getValue()))
     return true;
     else
     return false;
     }*/,
    "cantidad_unidad":validateCheck,
    "cantidad_unidad_mayoreo":validateCheck,
    "precio_unidad":validateCheck,
    "precio_unidad_mayoreo":validateCheck,
    "unidades_sueltas":validateCheck,
    "existencia_unidad":validateCheck,
    "id_contenedor":validateCheckEmpty,
  },
  on:{
    onSubmit:validate,
  }
};
toolbar_header['cols'][0].label="Información del artículo";
generic_window.width=850;
generic_window.height=900;
generic_window.body=form_articulos;
