URL_SAVE_SERVICIOS= BASE_URL +"inventario/servicios/save";
function validate(){
    if($$('form_servicios').validate()){
        $$('cargando').show();
        var data = $$('form_servicios').getValues();
        delete data['estatus'];
        delete data['$css'];
        $$('descripcion').setValue($$('descripcion').getValue().toUpperCase());
        webix.ajax().post(URL_SAVE_SERVICIOS,data,function(response){
            $$('cargando').hide();
            response =JSON.parse(response);
            var m ={
                text:response.message,
                ok:"Aceptar"
            }
            if(!response.estatus)
                m.type="alert-error";
            else{
                $$('generic_window').hide();
                $$('datatable_servicios').clearAll();
                $$('datatable_servicios').load(URL_GET_SERVICIOS);
            }
            webix.alert(m);
        });
    }
    else
        webix.alert(alert_error);
}

var form_servicios={
    view:"form",
    id:"form_servicios",
    elements:[
        {rows:[
            {cols:[
                {view:"text",id:"id",name:"id",hidden:true,value:"0"},
                {view:"text",label:"Descripción",id:"descripcion",name:"descripcion",required:true,on:{
                    onkeyPress:function (val) {
                        this.setValue(this.getValue().toUpperCase());
                    }
                }},

            ]},
            {cols:[
                label_required,
                {view:"button",width:150,id:"btn_change_estatus",click:function(){
                    var text = "¿Desea activar este servicio?"
                    var estado=1;
                    var item = $$('datatable_servicios').getSelectedItem();
                    if (item.estado==1){
                        text= "¿Desea desactivar este servicio?";
                        estado=2;
                    }
                    changeStatus(URL_SAVE_SERVICIOS,{id:item.id,estatus:estado},text,function(response){
                        response =JSON.parse(response);
                        webix.alert(response.message);
                        $$('generic_window').hide();
                        $$('datatable_servicios').load(URL_GET_SERVICIOS);
                    });
                }},
                {},
                {view:"button",type:"imageButton",id:'button_save_servicios',image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
            ]}
        ]}
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "descripcion":webix.rules.isNotEmpty,
    },
    on:{
        onSubmit:validate,
    }
};
toolbar_header['cols'][0].label="Información del servicio";
generic_window.width=600;
generic_window.body=form_servicios;
