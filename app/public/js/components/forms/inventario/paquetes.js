URL_GET_ARTICULOS =BASE_URL+"inventario/articulos/select";
URL_GET_ARTICULO =BASE_URL+"inventario/paquetes/selectArticulo";
URL_SAVE_PAQUETES =BASE_URL +"inventario/paquetes/save";
var precio_compra=0;
var precio_unidad_compra=0;
var precio_compra_g=0;
var venta_unidad=0;
function validate(){
  if ($$('form_paquetes').validate()){
    var data =$$('form_paquetes').getValues();
    delete  data['estatus'];
    delete  data['nombre_articulo'];
    delete  data['estado'];
    delete  data['$css'];
    delete  data['nombre_tipo_cantidad'];
    data.request="c4ca4238a0b923820dcc509a6f75849b";
    webix.ajax().post(URL_SAVE_PAQUETES,data,function(response){
      if (!venta_unidad)
          data['tipo_venta']='c81e728d9d4c2f636f067f89cc14862c';
      response =JSON.parse(response);
        if (response.estatus){
          webix.alert(response.message);
          $$('datatable_paquetes').load(URL_GET_PAQUETES);
          $$('generic_window').hide();
         // location.reload();
        }
      else{
          webix.alert({
            type:"alert-error",
            text:response.message
          });
        }
    });

  }
  else
    webix.alert(alert_error);
  $$('datatable_paquetes').unselect($$('datatable_paquetes').getSelectedId());
}

function validarCantidad(value){
  if (webix.rules.isNumber(value) && parseFloat($$('cantidad_minima').getValue())<=parseFloat($$('cantidad_maxima').getValue()))
      return true;
}


function  cargarArticulos(){
  webix.ajax().post(URL_GET_ARTICULOS+ "/" +"0",{},function(response){
      response = JSON.parse(response);
      $$('id_articulo').define("options",response);
      $$('id_articulo').refresh();
  });
}

function validateHora(){
  var hora_inicio = new Date($$('hora_inicio').getValue());
  var hora_fin = new Date($$('hora_fin').getValue());
  if (hora_fin>hora_inicio)
      return true;
  else
      return false;
}

var form_paquetes={
view:"form",
id:"form_paquetes",
elements:[
  {rows:[
    {view:"text",name:"id",id:"id",hidden:true},
    {view:'text',id:'descripcion',name:"descripcion",label:'Descripción del paquete',required:true},
    {view:"combo",label:"Articulo",id:"id_articulo",name:"id_articulo",options:URL_GET_ARTICULOS+"/0",required:true,on:{
      onChange:function(value){
        var item = $$('datatable_paquetes').getSelectedItem();
        webix.ajax().post(URL_GET_ARTICULO,{id:value},function(response){
          response =JSON.parse(response);
          precio_compra = parseFloat(response[0].precio_compra);
          precio_unidad_compra =parseFloat(response[0].precio_unidad_compra);
          if (response[0].venta_unidad==1){
            $$('tipo_venta').show();
            venta_unidad=1;
            precio_compra_g = precio_compra;
            $$('tipo_venta').define('options',[
              {id:'c4ca4238a0b923820dcc509a6f75849b',
                value:response[0].tipo_venta
              },
              {id:'c81e728d9d4c2f636f067f89cc14862c',
                value:response[0].contenedor
              }
            ]);
            $$('tipo_venta').refresh();
          }
          else {
            $$('tipo_venta').hide();
            $$('tipo_venta').define('options',[
              {id:'c81e728d9d4c2f636f067f89cc14862c',
                value:response[0].tipo_venta
              }
            ]);
            $$('tipo_venta').refresh();
           // $$('tipo_venta').setValue('c81e728d9d4c2f636f067f89cc14862c');
            if ($$('id').getValue()=="0")
            $$('precio').setValue(response[0].precio_compra);
            else
              $$('precio').setValue(item.precio);

          }
            if ($$('tipo_venta').getValue()=='c81e728d9d4c2f636f067f89cc14862c')
                precio_compra_g = precio_compra;
          else  if ($$('tipo_venta').getValue()=='c4ca4238a0b923820dcc509a6f75849b')
              precio_compra_g = precio_unidad_compra;

          //$$('tipo_venta').setValue('');
        });
      }
    }},

    {
      cols:[
        {
          view:'radio',
          id:'id_tipo_cantidad',
          name:'id_tipo_cantidad',
          label:'Tipo de cantidades para aplicar',
          value:1,
          options:[
            {id:1,value:'Rangos'},
            {id:2,value:'Exacto'},
          ],
          on:{
            onChange:function(val){
              if (val==1){
                $$('cont_rangos').show();
                $$('cantidad_minima').show();
                $$('cantidad_maxima').show();
                $$('cont_exacto').hide();
                $$('cantidad_exacta').hide();
              }
              else if (val==2){
                $$('cont_exacto').show();
                $$('cantidad_exacta').show();


                $$('cont_rangos').hide();
                $$('cantidad_minima').hide();
                $$('cantidad_maxima').hide();

              }
            }
          }
        },
        {
          view:"template",
          css:'help_block',
          borderless:true,
          template:"Al seleccionar el tipo de cantidad si selecciona  <b>Rangos</b> el precio a aplicar debe ser por unidad de la otra manera si es por tipo <b>Exacto</b> el precio debera ser para el total del paquete."
        }
      ]
    },
    {cols:[
      {
        view:'radio',
        id:'tipo_venta',
        name:'tipo_venta',
        label:'Aplicar a',
        options:[
          { id:'c81e728d9d4c2f636f067f89cc14862c',
            value:'UNIDAD'}
        ],
        on:{
          onChange:function(val){
            //if ($$('precio').getValue()==""){
            if (val=='c81e728d9d4c2f636f067f89cc14862c')
              precio_compra_g =parseFloat(precio_compra);
            else  if (val=='c4ca4238a0b923820dcc509a6f75849b')
              precio_compra_g =parseFloat(precio_unidad_compra);
            $$('precio').setValue(precio_compra_g.toFixed(2));
          }
          //}
        }
      },
      {view:"text",label:"Precio",name:"precio",id:"precio",required:true,invalidMessage:"Ingrese solo numeros"},
    ]
    },
    {
      id:'cont_exacto',
      cols:[
        {
          view:'text',
          id:'cantidad_exacta',
          name:'cantidad_exacta',
          label:'Cantidad de articulos'

        },
        {}
      ]
    },
    {
      id:'cont_rangos',
      cols:[
        {view:"text",label:"Cantidad minima",id:"cantidad_minima",name:"cantidad_minima",labelWidth:120,invalidMessage:"Ingrese solo numeros"},
        {view:"text",label:"Cantidad maxima", id:"cantidad_maxima",name:"cantidad_maxima",labelWidth:120,invalidMessage:"Ingrese solo numeros"}
      ]
    },
    {
      id:'cont_maneja_hora',
      view:'fieldset',
      label:'Horarios de aplicación',
      body:{
        cols:[
          {label:'Hora de inicio',view:'datepicker',type:'time',id:'hora_inicio',name:'hora_inicio',invalidMessage:"La hora de inicio tiene que ser menor que la hora de fin"},
          {label:'Hora de fin',view:'datepicker',type:'time',id:'hora_fin',name:'hora_fin',invalidMessage:"La hora de fin tiene que ser mayor que la hora de inicio"},
        ]
      }

    },
    {cols:[
        label_required,
      {view:"button",width:150,id:"btn_change_estatus",click:function(){
        var text = "¿Desea activar este paquete?"
        var estado=1;
        var item = $$('datatable_paquetes').getSelectedItem();
        if (item.estado==1){
          text= "¿Desea desactivar este paquete? \n al hacerlo no podra utilizarse el mismo en ventas";
          estado=2;
        }
        changeStatus(URL_SAVE_PAQUETES,{id:item.id,estatus:estado,request:'c81e728d9d4c2f636f067f89cc14862c'},text,function(response){
          response =JSON.parse(response);
          webix.alert(response.message);
          $$('generic_window').hide();
          $$('datatable_paquetes').load(URL_GET_PAQUETES);
        });
      }},
      {},
      {view:"button",label:"Aceptar",width:150,click:"validate()",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100}
    ]}
  ]}
],
rules:{
  "id_articulo":webix.rules.isNotEmpty,
  "precio":function(val){
    if (webix.rules.isNumber(val) && parseFloat(val)>= precio_compra_g)
        return true;
    else
        webix.alert({type:'alert-error',text:'El nuevo precio no puede ser menor que <b>'+ precio_compra_g.toFixed(2)+'</b></b> ya que provocaria perdidas.'})
  },
  //"cantidad_minima":validarCantidad,
  //"cantidad_maxima":validarCantidad,
  //"tipo_venta":webix.rules.isChecked,
  "hora_inicio":validateHora,
  "hora_fin":validateHora,
  "descripcion":webix.rules.isNotEmpty
},
on:{
  onSubmit:validate
},
  elementsConfig:{
    labelPosition:"top"
  }
};

generic_window.body=form_paquetes;
toolbar_header['cols'][0].label="Información del paquete";
generic_window.header=toolbar_header;
generic_window.body = form_paquetes;
generic_window.width=600;
generic_window.height=600;

