URL_SAVE_CLIENTES= BASE_URL +"inventario/clientes/save";
function validate(){
    if($$('form_clientes').validate()){
        $$('cargando').show();
        var data = $$('form_clientes').getValues();
        delete data['estatus'];
        delete data['$css'];
        webix.ajax().post(URL_SAVE_CLIENTES,data,function(response){
            $$('cargando').hide();
            response =JSON.parse(response);
            var m ={
                text:response.message,
                ok:"Aceptar"
            }
            if(!response.estatus)
                m.type="alert-error";
            else{
                $$('generic_window').hide();
                $$('datatable_clientes').clearAll();
                $$('datatable_clientes').load(URL_GET_CLIENTES);
            }
            webix.alert(m);
        });
    }
    else
        webix.alert(alert_error);
}

var form_clientes={
    view:"form",
    id:"form_clientes",
    elements:[
        {rows:[
            {cols:[
                {view:"text",id:"id",name:"id",hidden:true,value:"0"},
                {view:"text",label:"Nombre completo",id:"nombre",name:"nombre",required:true},
                {view:"text",label:"Teléfono",id:"telefono",name:"telefono",required:true,width:200,invalidMessage:"Ingrese un numero valido"},

            ]},
            {cols:[
                {view:"text",label:"Correo electrónico",id:"correo_electronico",name:"correo_electronico",width:250,invalidMessage:"Escriba un correo valido"},
                {view:"text",label:"RFC",id:"rfc",name:"rfc",width:250,invalidMessage:"Escriba un rfc valido"},
            ]},
            {view:"text",label:"Domicilio",id:"domicilio",name:"domicilio",width:750},
            {cols:[
                label_required,
                {view:"button",width:150,id:"btn_change_estatus",click:function(){
                    var text = "¿Desea activar este cliente?"
                    var estado=1;
                    var item = $$('datatable_clientes').getSelectedItem();
                    if (item.estado==1){
                        text= "¿Desea desactivar este cliente?";
                        estado=2;
                    }
                    changeStatus(URL_SAVE_CLIENTES,{id:item.id,estatus:estado},text,function(response){
                        response =JSON.parse(response);
                        webix.alert(response.message);
                        $$('generic_window').hide();
                        $$('datatable_clientes').load(URL_GET_CLIENTES);
                    });
                }},
                {},
                {view:"button",type:"imageButton",id:'button_save_clientes',image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
            ]}
        ]}
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "nombre":webix.rules.isNotEmpty,
        "telefono":webix.rules.isNumber,
        "correo_electronico":isEmail
    },
    on:{
        onSubmit:validate,
    }
};
toolbar_header['cols'][0].label="Información del cliente";
generic_window.width=750;
generic_window.body=form_clientes;
