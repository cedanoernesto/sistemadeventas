URL_SAVE_APARTADOS_PENDIENTES= BASE_URL +"apartados/apartados_pendientes/save";
function validate(){
    if($$('form_apartados_pendientes').validate()){
        $$('cargando').show();
        var data = $$('form_apartados_pendientes').getValues();
        delete data['estatus'];
        delete data['$css'];
        webix.ajax().post(URL_SAVE_APARTADOS_PENDIENTES,{fecha_vencimiento_tolerancia:data.fecha_vencimiento_tolerancia,id:data.id},function(response){
            $$('cargando').hide();
            response =JSON.parse(response);
            var m ={
                text:response.message,
                ok:"Aceptar"
            }
            if(!response.estatus)
                m.type="alert-error";
            else{
                $$('generic_window').hide();
                $$('datatable_apartados_pendientes').clearAll();
                $$('datatable_apartados_pendientes').load(URL_GET_APARTADOS_PENDIENTES);
            }
            webix.alert(m);
        });
    }
    else
        webix.alert(alert_error);
}

var form_apartados_pendientes={
    view:"form",
    id:"form_apartados_pendientes",
    elements:[
        {rows:[
            {cols:[
                {view:"text",id:"id",name:"id",hidden:true,value:"0"},
                {view:"datepicker",label:"Fecha de tolerancia",id:"fecha_vencimiento_tolerancia",name:"fecha_vencimiento_tolerancia",required:true,format:'%Y-%m-%d'},
            ]},
        ]},
        {
            cols:[
                {},
                {view:"button",type:"imageButton",id:'button_save_apartados_pendientes',image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
            ]
        }
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "fecha_vencimiento_tolerancia":webix.rules.isNotEmpty,
    },
    on:{
        onSubmit:validate,
    }
};
toolbar_header['cols'][0].label="Información del apartado";
generic_window.width=450;
generic_window.body=form_apartados_pendientes;
