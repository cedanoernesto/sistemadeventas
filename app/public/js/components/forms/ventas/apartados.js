URL_GET_ARTICULOS =BASE_URL+"ventas/ventas/getArticulos";
URL_GET_ARTICULO =BASE_URL+"ventas/ventas/getArticulo";
URL_GET_CLIENTES=BASE_URL+"inventario/clientes/selectClientes";
URL_SAVE_APARTADOS=BASE_URL+"ventas/apartados/save";
URL_SEARCH_APARTADOS=BASE_URL+"ventas/apartados/search";
URL_PAY_APARTADOS=BASE_URL+"ventas/apartados/pay";
URL_ABONOS=BASE_URL+"ventas/apartados/abonos";
URL_ARTICULOS=BASE_URL+"ventas/apartados/articulos";
var art ={};
var total=0;
var saldo=0;
var tipo_venta=0;
var pago_inicial=0;
 var datatable_articulos ={
     view:'datatable',
     id:'datatable_articulos',
     width:900,
     columns:[
         {id:'descripcion',header:'Descripción',fillspace:true},
         {id:'precio',header:'Precio',width:60},
         {id:'cantidad',header:'Cantidad',width:70},
         {id:'subtotal',header:'Sub',width:60},
     ]
 };
var datatable_apartados={
    view:'datatable',
    id:'datatable_apartados',
    columns:[
        {id:'folio',header:'Folio',fillspace:true},
        {id:'fecha',header:'Fecha',width:200},
        {id:'abono',header:'Abono',template:"<b>#abono#</b>"},
        {id:'pago',header:'Pago'},
        {id:'cambio',header:'Cambio'},
    ]

};
var window_realizar_apartado={
    view:"window",
    width:550,
    id:"window_realizar_apartado",
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Cobrar apartado",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    modal:true,
    position:"center",
    body:{
        view:"form",
        id:"form_realizar_venta",
        elements:[
            {rows:[
                {
                    view:"radio",label:"Tipo de pago",value:1,name:"tipo_pago",id:"tipo_pago",labelPosition:"top",options:[],
                    on:{
                        onChange:function(nv,vv){
                            if (nv==1){
                                $$('pago').define("disabled",false);
                                $$('pago_inicial').define("disabled",false);
                                $$('pago').focus();
                                $$('pago').setValue('');
                            }
                            else{
                                $$('pago').setValue(pago_inicial);
                                $$('pago').define("disabled",true);
                                $$('pago_inicial').define("disabled",true);
                            }
                        }
                    }
                },
                {
                    view:"radio",label:"Descuento",value:4,name:"id_descuento",id:"id_descuento",labelPosition:"top",options:[],
                    on:{
                        onChange:function(nv,vv){
                            var options=  this.data.options;
                            var desc = 0;
                            options.forEach(function (item) {
                                if (nv == item.id){
                                    desc = parseFloat(item.descuento)/100;
                                }
                            });
                            if (desc!=0){
                                total_g_descuento= (total_g*(1- desc)).toFixed(2);
                            }
                            else{
                                total_g_descuento= total_g.toFixed(2);
                            }
                            total_g_descuento = Math.ceil(total_g_descuento).toFixed(2);
                            $$('label_realizar_venta_total').define('label',total_g_descuento);
                            $$('label_realizar_venta_total').refresh();
                            $$('pago').focus();
                        }
                    }
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Total: ",css:"total",width:170},
                        {view:"label",label:"",id:"label_realizar_venta_total",css:"total",width:110},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Pago inicial: ",css:"total",width:170},
                        {view:"text",id:"pago_inicial",name:"pago_inicial",width:110,on:{
                           onChange:function(val){
                                if (parseFloat(val) > parseFloat(total_g_descuento)){
                                    webix.alert("El pago inicial no puede ser mayor que el total del apartado",function(){
                                        $$('pago_inicial').setValue('');
                                        $$('pago_inicial').focus();
                                    });
                                  /*  var pago = parseFloat(val).toFixed(2);
                                    $$('pago_inicial').setValue(parseFloat(total_g_descuento).toFixed(2));
                                    $$('pago').setValue(pago);*/
                                }
                            },
                                onkeypress:function (val) {
                                        if (val==13)
                                            $$('pago').focus();
                                }
                        }},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"text",label:"Pago con:",id:'pago',name:'pago',labelPosition:"top",width:150,on:{
                            onKeyPress:function(key){
                                if (key>=48 && key<=57 || key ==8 || key==27)
                                    event.returnValue=true;
                                else  if (key==13)
                                   apartar();
                                else if (key==110)
                                    event.returnValue=true;
                                else
                                    event.returnValue=false;
                            }
                        }},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Cambio: ",css:"total",width:120},
                        {view:"label",label:"0.00",id:"label_realizar_venta_cambio",css:"total",width:100},
                        {}
                    ]
                },
                {cols:[
                    {},
                    {view:"button",label:"Aceptar",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100,id:'btn_realizar',click:"apartar()"}
                ]}
            ]}
        ],
        on: {
            //onSubmit: apartar
        }
    }
}
var window_realizar_abono={
    view:"window",
    id:"window_realizar_abono",
    width:400,
    head:{
        view:"toolbar",
        cols:[
            {
                view:"toolbar",
                cols:[
                    {view:"label",label:"Abonar a apartado",align:"center"},
                    {
                        view:"button",
                        type:"image",
                        image:BASE_URL_ICONS+"/cancel24.png",
                        width:40,
                        borderless:true,
                        click:function(){
                            this.getTopParentView().hide();
                        }
                    }
                ]
            }

        ]
    },
    modal:true,
    position:"center",
    body:{
        view:"form",
        elements:[
            {rows:[
                {
                    view:"radio",label:"Tipo de pago",value:1,id:"tipo_pago_abonar",labelPosition:"top",options:[],
                    on:{
                        onChange:function(nv,vv){
                            if (nv==1){
                                $$('pago_abono').define("disabled",false);
                                $$('pago_abono').focus();
                                $$('pago_abono').setValue('');
                            }
                            else{
                                $$('pago_abono').setValue($$('abono_apartado').getValue());
                                $$('pago_abono').define("disabled",true);
                            }
                        }
                    }
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Total: ",css:"total",width:100},
                        {view:"label",label:"",id:"label_realizar_abono_total",css:"total",width:250},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"text",label:"Pago con:",id:'pago_abono',name:'pago_abono',labelPosition:"top",width:150,on:{
                            onKeyPress:function(key){
                                if (key>=48 && key<=57 || key ==8 || key==27)
                                    event.returnValue=true;
                                    else if (key==13)
                                        pagar_abono();
                                else
                                    event.returnValue=false;
                            }
                        }},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Cambio: ",css:"total",width:120,align:''},
                        {view:"label",label:"0.00",id:"label_realizar_abono_cambio",css:"total",width:150},
                        {}
                    ]
                },
                {cols:[
                    {},
                    {view:"button",label:"Aceptar",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100,id:'btn_realizar_abono',click:"pagar_abono()"}
                ]}
            ]}
        ]
    }
}



window_varios_apartados ={
    view:'window',
    id:'window_varios_apartados',
    modal:true,
    position:'center',
    width:500,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Seleccione apartado",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:{
        view:"list",
        id:"lista_apartado",
        select:true,
        on:{
            onItemClick:function(id){
            var item = this.getItem(id);
                llenar(item);
                $$('window_varios_apartados').hide();
                $$('pago').define('disabled',false)
                $$('abono_apartado').setValue('');
                $$('abono_apartado').focus();
                $$('mostrar_abonos').define('disabled',false);
            }
        }
    }
}


 var view_apartado ={
     rows:[
         {
             view:'form',
             id:'buscar_form',
             width:900,
             elements:[
                 {
                     rows:[
                         {
                             cols:[
                                 {view:'text',id:'codigo',name:'codigo',label:'Folio',invalidMessage:"Debe de especificar un folio."},
                                 {view:'combo',label:'Cliente',name:'id_cliente',id:'buscar_id_cliente',options:URL_GET_CLIENTES},
                                 {rows:[
                                     {view:'button',label:'Buscar',type:'imageButton',image:BASE_URL_ICONS+'search.png',width:100,click:"buscar()"},
                                     {}
                                 ]}
                             ]
                         }
                     ]
                 }
             ],
             rules:{
                 //'codigo': webix.rules.isNotEmpty
             },
             on:{
                 onSubmit:buscar
             }
         },
         {height:10},
         {
             view:'fieldset',
             label:'Datos del apartado',
             body:{
                 rows:[
                     {
                         cols:[
                             {view:'label',label:'Folio:'},
                             {view:'label',label:'Fecha de la compra: '},
                             {view:'label',label:'Cliente: '},

                         ]
                     },
                     {
                         cols:[
                             {view:'label',id:'label_folio',css:'existencia'},
                             {view:'label',id:'label_fecha',css:'existencia'},
                             {view:'label',id:'label_cliente',css:'existencia'},

                         ]
                     },
                     {
                         cols:[
                             {view:'label',label:'Total: '},
                             {view:'label',label:'Ultimo abono: '},
                             {view:'label',label:'Saldo: '},
                             {view:'label',label:'Abono: ',width:120}

                         ]
                     },
                     {
                         cols:[
                             {view:'label',id:'label_buscar_total',css:'existencia'},
                             {view:'label',id:'label_buscar_ultimo_abono',css:'existencia'},
                             {view:'label',id:'label_buscar_saldo',css:'existencia'},
                             {
                                 view:'form',
                                 id:'form_abono_apartado',
                                 borderless:true,
                                 elements:[
                                     {rows:[
                                         {view:'text',id:'id_apartado',name:'id_apartado',hidden:true},
                                         {view:'text',id:'abono_apartado',name:'abono_apartado',invalidMessage:valid_number,width:120,required:true}
                                     ]}
                                 ],
                                 on:{
                                   onSubmit:abonar
                                 },
                                 rules:{
                                     "abono_apartado":webix.rules.isNumber
                                 }
                             }
                         ]
                     },

                     {height:10},
                     {
                         cols:[
                             {},
                             {view:'button',disabled:true,id:'mostrar_abonos',label:'Mostrar abonos',type:'imageButton',image:BASE_URL_ICONS+'gastos.png',width:150,click:"$$('window_abonos').show()",align:'center'},
                             {},
                             {view:'button',label:'Aceptar',type:'imageButton',image:BASE_URL_ICONS+'success.png',width:100,click:abonar}
                         ]
                     }
                 ]
             }
         },

     ]
 }



var form_apartados ={
    view:"form",
    id:"form_apartados",
    elements:[
        {width:1130,
            rows:[
                {view:"text",id:"id",name:"id",hidden:true,value:0},
                {
                    cols:[
                        {view:"combo",id:"id_cliente",name:"id_cliente",label:"Cliente",width:450,options:URL_GET_CLIENTES,invalidMessage:required,on:{
                            onKeyPress:function(key){
                                if(key==13)
                                    $$('id_articulo').focus();
                            },
                            onChange:function(){
                                $$('id_articulo').focus();
                            }
                        }},
                        {
                            rows:[
                                {},
                                {view:"button",label:"Nuevo Cliente",type:"imageButton",image:BASE_URL_ICONS+"new.png",width:150,click:function(){
                                    $$('window_clientes').show();
                                    $$('btn_change_estatus').hide();
                                    $$('form_clientes').clearValidation();
                                    $$('nombre').setValue('');
                                    $$('telefono').setValue('');
                                    $$('correo_electronico').setValue('');
                                    $$('id').setValue(0);
                                    $$('nombre').focus();
                                }}
                            ]
                        }
                    ]
                },
                form_ventas,
                datatable_ventas,
                {cols:[
                    {
                        cols:[
                            {},
                            {view:"label",label:"Total: ",css:"precio",width:80},
                            {view:"label",id:"label_total",label:"0.00 ",css:"total"}
                        ]
                    },
                    {},
                    {cols:[
                        {},
                        {view:"button",label:"Realizar apartado",click:"cobrarApartado()",type:"imageButton",image:BASE_URL_ICONS+"realizar_venta.png",width:195},
                        {view:"button",label:"Cancelar apartado",click:"cancelarVenta()",type:"imageButton",image:BASE_URL_ICONS+"cancelar_venta.png",width:180},

                        {}
                    ]},
                ]}
              /*  {view:"combo",id:"id_articulo",name:"id_articulo",width:500,options:URL_GET_ARTICULOS,label:"Articulo",width:450,invalidMessage:required,
                    on:{
                        onChange:function(nv){
                            webix.ajax().post(URL_GET_ARTICULO,{id:nv},function(response){
                                response = JSON.parse(response);
                                art.precio_venta = response[0]['precio_venta'];
                                art.id_articulo =response[0]['id'];
                                $$('label_precio').define('label','Precio: $'+art.precio_venta);
                                $$('label_precio').refresh();
                                var sugerido = Math.round(art.precio_venta*.30);
                                $$('pago_inicial').setValue(sugerido);
                                calcularSaldo();
                                calcularCambioNuevo();
                            });
                            $$('pago_inicial').focus();
                        },
                        onKeyPress:function(val){
                            if (val==13)
                                $$('pago_inicial').focus();
                        }
                    }}

                {
                    cols:[
                        {view:"label",id:"label_precio",label:"Precio: ",css:"precio"},
                        {view:"text",label:"Pago inicial",id:"pago_inicial",name:"pago_inicial",invalidMessage:valid_number,on:{
                            onKeyPress:function(key){
                                if (key >= 48 && key <= 57)
                                event.returnValue=true;
                                else if (key==8)
                                    event.returnValue=true;
                                else if (key==13)
                                        nuevo_apartado();
                                else
                                    event.returnValue=false;
                            }
                        }},
                        {width:10},
                        {view:"label",id:"label_saldo",label:"Saldo: ",css:"precio"},
                    ]
                },
                {
                    cols:[
                       /* {view:'text',label:'Pago',id:'pago_nuevo',width:150},
                        {view:"label",id:"label_nuevo_cambio",label:"Cambio: ",css:"precio"},
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"button",label:"Aceptar",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100,click:"nuevo_apartado()"}
                    ]
                }*/
            ]
        }
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "id_cliente": webix.rules.isNotEmpty,
    }

};
var window_apartados ={
    view:"window",
    id:"window_apartados",
    modal:true,
    height:700,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Modulo",id:"label_toolbar_header",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:form_apartados,
    position:"center"
}
var window_abonos ={
    view:"window",
    id:"window_abonos",
    modal:true,
    width:600,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Abonos realizados",id:"label_toolbar_header",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:datatable_apartados,
    position:"center"
}
