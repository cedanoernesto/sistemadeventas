function cobrarApartado(){
    if ($$('form_apartados').validate() && $$('datatable_ventas').count()>0){
      // $$('form_realizar_venta').clear();
        //$$('form_realizar_venta').clearValidation();
            total_g_descuento = total_g;
            pago_inicial = Math.ceil(total_g_descuento*.30);
            $$('pago_inicial').setValue(pago_inicial);
            $$('id_articulo').setValue("0");
            $$('id_articulo').blur();
            $$('tipo_pago').setValue(1);
            $$('pago').setValue('');
            setInterval(function(){
                var cambio=0;
                if (parseFloat($$('pago').getValue())>=parseFloat($$('pago_inicial').getValue())){
                    cambio =parseFloat($$('pago').getValue())- parseFloat($$('pago_inicial').getValue());
                }
                $$('label_realizar_venta_cambio').define("label",cambio.toFixed(2));
                $$('label_realizar_venta_cambio').refresh();
            },100);
            $$('label_realizar_venta_total').define("label",total_g_descuento.toFixed(2));
            $$('label_realizar_venta_total').refresh();
            $$('window_realizar_apartado').show();
            $$('pago_inicial').focus();
        }
}

function pagar_abono(){
  pago = parseFloat($$('abono_apartado').getValue());
  abono = parseFloat($$('pago_abono').getValue());
  if (abono>=pago){
        var data =$$('form_abono_apartado').getValues();
    data['abono'] =pago ;
    data['pago'] = abono;
    data['cambio'] = abono-pago;
    data['tipo_pago'] = $$('tipo_pago_abonar').getValue();
    webix.ajax().post(URL_PAY_APARTADOS,data,function(response){
        response = JSON.parse(response);
        imprimirTicketAbono(response)
        limpiar();
        $$('pago').define('disabled',true);
        $$('window_realizar_abono').hide();
        webix.alert("Datos guardados correctamente.",function () {
            location.reload();
        })
    });
  }
  else
  webix.alert({type:'alert-error',text:'El pago no puede ser menor que el abono.'})
}

function nuevo_apartado(){
  if ($$('form_apartados').validate()){
    $$('pago_nuevo').setValue('');
    if ((parseFloat(art.precio_venta) - parseFloat($$('pago_inicial').getValue())) <= 0)
        webix.alert({
            type: "alert-error",
            text: "El pago inicial no puede ser mayor que el precio del articulo"
        });
    else{
        $$('window_realizar_venta').show();
        $$('label_realizar_venta_total').define('label','$'+parseFloat($$('pago_inicial').getValue()).toFixed(2));
        $$('label_realizar_venta_total').refresh();
        $$('pago_nuevo').focus();
        calcularCambioNuevo();
    }
  }
  else
    webix.alert({type:'alert-error',text:'Complete los campos requeridos.'});

}

function save_cliente(){
    if($$('form_clientes').validate()){
        $$('cargando').show();
        var data = $$('form_clientes').getValues();
        delete data['estatus'];
        webix.ajax().post(URL_SAVE_CLIENTES,data,function(response){
            $$('cargando').hide();
            $$('window_clientes').hide();
            response =JSON.parse(response);
            var m={
                text:response.message,
                ok:"Aceptar"
            };
            if(!response.estatus)
                m.type="alert-error";
            else
                $$('window_clientes').hide();
            webix.ajax().post(URL_GET_CLIENTES,{},function(value){
                value =JSON.parse(value);
                $$('id_cliente').define('options',value);
                $$('id_cliente').refresh();
                $$('id_cliente').setValue(response.id_md5);
            });
        });
    }
    else
        webix.alert(alert_error);
}
function apartar(tipo){
        $$('cargando').show();
        var data ={};
        data['pago'] = parseFloat($$('pago').getValue());
        data['pago_inicial'] = parseFloat($$('pago_inicial').getValue());
        data['tipo_pago'] = $$('tipo_pago').getValue();
        data['total'] = total_g_descuento;
        data['id_cliente'] = $$('id_cliente').getValue();
        data['id_descuento'] = $$('id_descuento').getValue();
            if (parseFloat($$('pago').getValue())>=parseFloat($$('pago_inicial').getValue())){
                webix.ajax().post(URL_SAVE_APARTADOS,{venta:data,detalles:venta},function(response){
                    response = JSON.parse(response)
                    imprimirTicket(response);
                    $$('cargando').hide();
                    webix.alert("Datos guardados correctamente.",function () {
                        location.reload();
                    })
                });
            }
            else{
                $$('cargando').hide();
                webix.alert({type:'alert-error',text:'El pago no puede ser menor que el pago inicial'});
            }

}
function imprimirTicketAbono(response) {
    var config = qz.configs.create(response.datos_ticket.impresora);
    var data = [
        '\x1B' + '\x40',          // init
        '\x1B' + '\x61' + '\x31', // center align
        '\x1B' + '\x21' + '\x30', // em mode on
        response.datos_ticket.nombre_negocio , ' \n \x0A',
        '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
        response.datos_ticket.datos_fiscales + '\n'+
        '\x1B' + '\x61' + '\x30', // left align
        'FOLIO: ' + response.venta.folio + '        ' + response.venta.fecha + '\n',
        'ATENDIO: ' + response.venta.nombre + '\x0A.',
        '\x0A',
        '#  ' + 'DESCRIPCION              ' + 'CANT   ' + 'PREC   ' + 'SUB   \n'
    ];
    function pad(input, length, padding) {
        var str = input + "";
        return (str.length > length) ? str.substr(0,25) + ' '  : pad((arguments[3] != undefined)?str:str+padding, length, padding);
    }
    response.detalles.forEach(function (item, index) {
        item.descripcion = pad(item.descripcion,25,' ')
        item.cantidad = pad(item.cantidad,7,' ')
        item.precio = pad(item.precio,7,' ')
        item.subtotal = pad(item.subtotal,6,' ',true)
        data.push( '\x1B' + '\x4D' + '\x31')
        data.push(index+1,'  ' + item.descripcion + item.cantidad  + item.precio + item.subtotal + '\n')
        data.push( '\x1B' + '\x4D' + '\x30')
    })
    /*if(response.venta.descuento != '0.00') {
     data.push(
     '\x0A',
     '\x1B' + '\x61' + '\x32',
     '\x1B' + '\x45' + '\x0D', // bold on
     'TOTAL: ' + (parseFloat(response.venta.descuento) + parseFloat(response.venta.total)).toFixed(2),
     '\n',
     'DESCUENTO('+response.venta.tipo_descuento+'): ' +response.venta.descuento,
     '\n'
     )
     }*/
    /*data.push(
     '\x1B' + '\x61' + '\x30', // left align
     '\x0A',
     'PAGO: ' + response.venta.tipo_pago
     )*/

    data.push(
        '\x0A',
        '\x1B' + '\x61' + '\x32',
        '\x1B' + '\x45' + '\x0D', // bold on
        'TOTAL: ' + response.venta.total,
        '\n',
        'ABONO: ' +response.venta.abono,
        '\n ',
        'NUEVO SALDO: ' + response.venta.saldo,
        '\x0A',
        '\x0A',
        'PAGO: ' + response.venta.pago,
        '\n',
        'CAMBIO: ' +response.venta.cambio,
        '\x0A',
        '\x0A',
        '\x1B' + '\x61' + '\x31', //center align
        response.datos_ticket.mensaje_gracias + ' \x0A',
        '\x1B' + '\x45' + '\x0A', // bold of
        response.datos_ticket.advertencia
        //
    )
    data.push( '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A',
        '\x1B', '\x69');
    //var data = [response]
    qz.print(config,data).catch(function (e) {
        console.error(e)
    })
}

function imprimirTicket(response) {
    return;
    var config = qz.configs.create(response.datos_ticket.impresora);
    var data = [
        '\x1B' + '\x40',          // init
        '\x1B' + '\x61' + '\x31', // center align
        '\x1B' + '\x21' + '\x30', // em mode on
        response.datos_ticket.nombre_negocio , ' \n \x0A',
        '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
        response.datos_ticket.datos_fiscales + '\n'+
        '\x1B' + '\x61' + '\x30', // left align
        'FOLIO: ' + response.venta.folio + '        ' + response.venta.fecha + '\n',
        'ATENDIO: ' + response.venta.nombre + '\x0A.',
        '\x0A',
        '#  ' + 'DESCRIPCION              ' + 'CANT   ' + 'PREC   ' + 'SUB   \n'
    ];
    function pad(input, length, padding) {
        var str = input + "";
        return (str.length > length) ? str.substr(0,25) + ' '  : pad((arguments[3] != undefined)?str:str+padding, length, padding);
    }
    response.detalles.forEach(function (item, index) {
        item.descripcion = pad(item.descripcion,25,' ')
        item.cantidad = pad(item.cantidad,7,' ')
        item.precio = pad(item.precio,7,' ')
        item.subtotal = pad(item.subtotal,6,' ',true)
        data.push( '\x1B' + '\x4D' + '\x31')
        data.push(index+1,'  ' + item.descripcion + item.cantidad  + item.precio + item.subtotal + '\n')
        data.push( '\x1B' + '\x4D' + '\x30')
    })
    /*if(response.venta.descuento != '0.00') {
        data.push(
            '\x0A',
            '\x1B' + '\x61' + '\x32',
            '\x1B' + '\x45' + '\x0D', // bold on
            'TOTAL: ' + (parseFloat(response.venta.descuento) + parseFloat(response.venta.total)).toFixed(2),
            '\n',
            'DESCUENTO('+response.venta.tipo_descuento+'): ' +response.venta.descuento,
            '\n'
        )
    }*/
    /*data.push(
        '\x1B' + '\x61' + '\x30', // left align
        '\x0A',
        'PAGO: ' + response.venta.tipo_pago
    )*/

    data.push(
        '\x0A',
        '\x1B' + '\x61' + '\x32',
        '\x1B' + '\x45' + '\x0D', // bold on
        'TOTAL: ' + response.venta.total,
        '\n',
        'PAGO INICIAL: ' +response.venta.abono,
        '\n ',
        'SALDO: ' + response.venta.saldo,
        '\x0A',
        '\x0A',
        'PAGO: ' + response.venta.pago,
        '\n',
        'CAMBIO: ' +response.venta.cambio,
        '\x0A',
        '\x0A',
        '\x1B' + '\x61' + '\x30', // left align
        'Cliente: ' + response.venta.cliente,
        '\x0A',
        'Liquidar la cantidad de $'+response.venta.saldo+' antes de '+response.venta.fecha_vencimiento,
        //'Cuenta con 30 dias para liquidar el saldo',
        '\x0A',
        '\x1B' + '\x61' + '\x31', //center align
        response.datos_ticket.mensaje_gracias + ' \x0A',
        '\x1B' + '\x45' + '\x0A', // bold of
        response.datos_ticket.advertencia
        //
    )
    data.push( '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A',
        '\x1B', '\x69');
    //var data = [response]
    qz.print(config,data).catch(function (e) {
        console.error(e)
    })
}

function calcularCambio(){
    setInterval(function(){
        var pago  = parseFloat($$('pago').getValue());
        var cambio =0;
        cambio =pago-saldo;
        if (!isNaN(cambio) && pago>saldo){
            $$('label_realizar_venta_cambio').define('label','$'+cambio);
            $$('label_realizar_venta_cambio').refresh();
        }
        else if ($$('pago').getValue()==''){
            $$('label_realizar_venta_cambio').define('label','$0');
            $$('label_realizar_venta_cambio').refresh();
        }
        else{
            $$('label_cambio').define('label','$0');
            $$('label_cambio').refresh();
        }
    },200);
}

function calcularCambioNuevo(){
    setInterval(function(){
        var pago_inicial  = parseFloat($$('pago_inicial').getValue());
        var pago_nuevo  = parseFloat($$('pago_nuevo').getValue());
        var cambio =pago_nuevo-pago_inicial;
        cambio = cambio.toFixed(2);
        $$('label_realizar_venta_cambio').define('label','$0');
        $$('label_realizar_venta_cambio').refresh();
        if (!isNaN(cambio) && pago_nuevo>=pago_inicial){
            $$('label_realizar_venta_cambio').define('label','$'+cambio);
            $$('label_realizar_venta_cambio').refresh();
        }
        else if ($$('pago_nuevo').getValue()==''){
            $$('label_realizar_venta_cambio').define('label','$0');
            $$('label_realizar_venta_cambio').refresh();
        }
        else{
            $$('label_realizar_venta_cambio').define('label','$0');
            $$('label_realizar_venta_cambio').refresh();
        }

    },200);
}
function calcularCambioAbono(){
    setInterval(function(){
        var pago_abono  = parseFloat($$('pago_abono').getValue());
        var cambio =pago_abono-total;
        cambio = cambio.toFixed(2);
        var valor ="";
        if (!isNaN(cambio) && pago_abono>=total)
            valor = '$'+cambio;
        else if ($$('pago_abono').getValue()=='')
            valor = "$0";
        else
          valor = "$0";
    $$('label_realizar_abono_cambio').define('label',valor);
    $$('label_realizar_abono_cambio').refresh();
    },200);
}
function abonar(){

    if($$('form_abono_apartado').validate()){
        var abono  = parseFloat($$('abono_apartado').getValue()).toFixed(2);
        $$('pago_abono').setValue('');
        if (abono>saldo){
            var pagara =  parseFloat($$('pago_abono').getValue());
           // $$('pago').setValue(saldo);
            $$('pago_abono').setValue(saldo);
        }
        if ($$('id_apartado').getValue()!="0"){
            if(abono>saldo){
                $$('pago_abono').setValue(abono);
                $$('label_realizar_abono_total').define('label','$'+saldo);
                total=saldo;
            }
            else{
                total = abono;
                $$('label_realizar_abono_total').define('label','$'+abono);
            }
            abonar=2;
            $$('window_realizar_abono').show();
            $$('pago_abono').focus();
            $$('label_realizar_abono_total').refresh();
            calcularCambioAbono();
        }
    }

}
function llenar(response){
    $$('label_folio').define('label',response.folio);
    $$('label_folio').refresh();
    $$('label_fecha').define('label',response.fecha);
    $$('label_fecha').refresh();
    $$('label_cliente').define('label',response.cliente);
    $$('label_cliente').refresh();
    $$('label_buscar_total').define('label',response.total);
    $$('label_buscar_total').refresh();
    $$('label_buscar_total').define('label',"$"+response.total);
    $$('label_buscar_total').refresh();
    $$('label_buscar_ultimo_abono').define('label',"$"+response.abono);
    $$('label_buscar_ultimo_abono').refresh();
    $$('label_buscar_saldo').define('label','$'+response.saldo);
    $$('label_buscar_saldo').refresh();
    $$('id_apartado').setValue(response.id);
    $$('pago').focus();
    total = parseFloat(response.total);
    saldo = parseFloat(response.saldo);
    webix.ajax().post(URL_ABONOS,{id:response.id},function(response){
        response =JSON.parse(response);
            $$('datatable_apartados').clearAll();
            $$('datatable_apartados').define('data',response);
            $$('datatable_apartados').refresh();

    });
    webix.ajax().post(URL_ARTICULOS,{id:response.id},function(response){
        response =JSON.parse(response);
            $$('datatable_articulos').clearAll();
            $$('datatable_articulos').define('data',response);
            $$('datatable_articulos').refresh();

    });

}
function limpiar(){
    $$('label_folio').define('label','');
    $$('label_folio').refresh();
    $$('label_fecha').define('label','');
    $$('label_fecha').refresh();
    $$('label_cliente').define('label','');
    $$('label_cliente').refresh();
    $$('label_buscar_saldo').define('label','');
    $$('label_buscar_saldo').refresh();
    $$('label_buscar_total').define('label','');
    $$('label_buscar_total').refresh();
    $$('label_buscar_ultimo_abono').define('label','');
    $$('label_buscar_ultimo_abono').refresh();
    $$('id_apartado').setValue(0);
    $$('pago').setValue('');
    $$('abono_apartado').setValue('');
    $$('buscar_form').clear();
    $$('datatable_apartados').clearAll();
    $$('datatable_articulos').clearAll();
}


function buscar(){
    $$('cargando').show();
    if ($$('buscar_form').validate()){
        webix.ajax().post(URL_SEARCH_APARTADOS,$$('buscar_form').getValues(),function(response){
            response  = JSON.parse(response);
            if (response.length>1){
                $$('cargando').hide();
                $$('window_varios_apartados').show();
                response.forEach(function(item,index){
                    var fecha = new Date (item.fecha);
                    response[index]['value'] = item.cliente +' $'+item.total +' - '+ item.fecha;
                });
                $$('lista_apartado').define('data',response);
                $$('lista_apartado').refresh();

            }
            else if (!response.length){
                webix.alert({type:'alert-error',text:'Apartado no encontrado'});
                limpiar();
                $$('codigo').setValue('');
                $$('codigo').focus();
                $$('cargando').hide();
                $$('mostrar_abonos').define('disabled',true);

            }

            else{
                $$('mostrar_abonos').define('disabled',false);
                $$('pago').define('disabled',false);
                $$('cargando').hide();
                llenar(response[0]);
                $$('abono_apartado').focus();
            }
        });
    }
    $$('pago').setValue('');
}

function calcularSaldo(){
    setInterval(function(){
        var pago_inicial =$$('pago_inicial').getValue();
        var saldo = (parseFloat(art.precio_venta)-parseFloat(pago_inicial)).toFixed(2);
        if (pago_inicial == '') {
            $$('label_saldo').define('label','Saldo: $0');
            $$('label_saldo').refresh();
        }

        if (parseFloat(pago_inicial)<=(parseFloat(art.precio_venta))){
            $$('label_saldo').define('label','Saldo: $'+saldo);
            $$('label_saldo').refresh();
        }
        else{
            if (pago_inicial!=''){
                $$('label_saldo').define('label','Saldo: $0');
                $$('label_saldo').refresh();
            }
        }

    },100);
}
