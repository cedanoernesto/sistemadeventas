URL_SAVE_USUARIOS= BASE_URL +"sistema/usuarios/save";
URL_TIPOS_PERFILES = BASE_URL +"sistema/usuarios/perfiles";

function validate(){
        var data = $$('form_usuarios').getValues();
        delete data['$css'];
        delete data['nombre_perfil'];
        delete data['estado'];
    console.log(data);
    if($$('form_usuarios').validate()){
        if (data['contrasena'] == data['repite_contrasena']){
        $$('cargando').show();
        delete data['estatus'];
        webix.ajax().post(URL_SAVE_USUARIOS,data,function(response){
            $$('cargando').hide();
            response =JSON.parse(response);
            var m ={
                text:response.message,
                ok:"Aceptar"
            }
            if(!response.estatus)
                m.type="alert-error";
            else{
                $$('generic_window').hide();
                $$('datatable_usuarios').clearAll();
                $$('datatable_usuarios').load(URL_GET_USUARIOS);
            }
            webix.alert(m);
        });
      }
      else
        webix.alert({type:'alert-error',text:'Las contraseñas no coinciden.'});
    }
    else
        webix.alert(alert_error);
      }


var form_usuarios={
    view:"form",
    id:"form_usuarios",
    elements:[
        {rows:[
            {cols:[
                {view:"text",id:"id",name:"id",hidden:true,value:"0"},
                {view:"text",label:"Nombre del usuario",id:"nombre",name:"nombre",required:true},
                {view:"text",label:"Usuario",id:"usuario",name:"usuario",required:true,width:200,invalidMessage:required},

            ]},
            {
              cols:[
                {view:'text',type:'password',label:'Contraseña',name:'contrasena',id:'contrasena'},
                  {view:'text',type:'password',label:'Repita contrasena',name:'repite_contrasena',id:'repite_contrasena'},

              ]
            },
            {cols:[
                {view:"combo",label:"Tipo de usuario",id:"id_perfil",name:"id_perfil",width:180,invalidMessage:required,options:URL_TIPOS_PERFILES},
                {}
            ]},
            {cols:[
                label_required,
                {view:"button",width:150,id:"btn_change_estatus",click:function(){
                    var text = "¿Desea activar este usuario?"
                    var estado=1;
                    var item = $$('datatable_usuarios').getSelectedItem();
                    if (item.estado==1){
                        text= "¿Desea desactivar este usuario?";
                        estado=2;
                    }
                    changeStatus(URL_SAVE_USUARIOS,{id:item.id,estatus:estado},text,function(response){
                        response =JSON.parse(response);
                        webix.alert(response.message);
                        $$('generic_window').hide();
                        $$('datatable_usuarios').load(URL_GET_usuarios);
                    });
                }},
                {},
                {view:"button",type:"imageButton",id:'button_save_usuarios',image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
            ]}
        ]}
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "nombre":webix.rules.isNotEmpty,
            "usuario":webix.rules.isNotEmpty,
                "id_perfil":webix.rules.isNotEmpty,
                    "contrasena":webix.rules.isNotEmpty,
                      "repite_contrasena":webix.rules.isNotEmpty

    },
    on:{
        onSubmit:validate,
    }
};
toolbar_header['cols'][0].label="Información del usuario";
generic_window.width=600;
generic_window.body=form_usuarios;
