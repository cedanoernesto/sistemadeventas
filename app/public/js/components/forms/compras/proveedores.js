URL_SAVE_PROVEEDORES= BASE_URL +"compras/proveedores/save";
function validate(){
    if($$('form_proveedores').validate()){
        var data =$$('form_proveedores').getValues();
        delete data['estatus'];
        $$('cargando').show();
        webix.ajax().post(URL_SAVE_PROVEEDORES,data,function(response){
            $$('cargando').hide();
            response =JSON.parse(response);
            var m ={
                text:response.message,
                ok:"Aceptar"
            }
            if(!response.estatus)
                m.type="alert-error";
            else{
                $$('generic_window').hide();
                $$('datatable_proveedores').clearAll();
                $$('datatable_proveedores').load(URL_GET_PROVEEDORES);
            }
            webix.alert(m);
        });
    }
    else
        webix.alert(alert_error);
}

var form_proveedores={
    view:"form",
    id:"form_proveedores",
    elements:[
        {rows:[
            {cols:[
                {view:"text",id:"id",name:"id",hidden:true,value:"0"},
                {view:"text",label:"Nombre completo",id:"nombre",name:"nombre",required:true,invalidMessage:required},
                {view:"text",label:"Teléfono",id:"telefono",name:"telefono",required:true,width:200,invalidMessage:valid_number},

            ]},
            {cols:[
                {view:"text",label:"Correo electrónico",id:"correo_electronico",name:"correo_electronico",width:280,invalidMessage:valid_email},
                {view:"text",label:"RFC",id:"rfc",name:"rfc",required:true,invalidMessage:valid_rfc},
            ]},
            {view:"text",label:"Domicilio",id:"domicilio",name:"domicilio",required:true,invalidMessage:required},
            {cols:[
                label_required,
                {view:"button",type:"danger",label:"",width:180,id:"btn_change_estatus",click:function(){
                    var text = "¿Desea activar este proveedor?"
                    var estado=1;
                    var item = $$('datatable_proveedores').getSelectedItem();
                    if (item.estado==1){
                        text= "¿Desea desactivar este proveedor?";
                        estado=2;
                    }
                    changeStatus(URL_SAVE_PROVEEDORES,{id:item.id,estatus:estado},text,function(response){
                        response =JSON.parse(response);
                        webix.alert(response.message);
                        $$('generic_window').hide();
                        $$('datatable_proveedores').load(URL_GET_PROVEEDORES);
                    });
                }},
                {},
                {view:"button",type:"imageButton",image:BASE_URL_ICONS+"success.png",label:"Aceptar",width:100,click:"validate()"}
            ]}
        ]}
    ],
    elementsConfig:{
        labelPosition:"top"
    },
    rules:{
        "nombre":webix.rules.isNotEmpty,
        "telefono":webix.rules.isNumber,
        "correo_electronico":isEmail,
        "domicilio":webix.rules.isNotEmpty,
        "rfc":isRfc
    },
    on:{
        onSubmit:validate,
    }
};
toolbar_header['cols'][0].label="Información del proveedor";
generic_window.width=550;
generic_window.body=form_proveedores;
