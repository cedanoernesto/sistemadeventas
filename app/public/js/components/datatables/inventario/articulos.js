URL_GET_ARTICULOS =BASE_URL+"inventario/articulos/datatable";
URL_GET_TIPOS_VENTA =BASE_URL+"sistema/general/getTiposVenta";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
function select(id){
  var item = $$('datatable_articulos').getItem(id);
  $$('generic_window').show();
  item['unidades_sueltas'] ="0";
  $$('unidades_sueltas').setValue("0");
  $$('form_articulos').setValues(item);
  $$('existencia_unidad').setValue(parseFloat(item.existencia_unidad).toFixed(1));
  $$('form_articulos').clearValidation();
  onCheck();
  /*   if (item.venta_unidad==1){
   $$('btn_ver_cantidades').define("disabled",false);
   $$('btn_ver_cantidades').refresh();
   }
   else{
   $$('btn_ver_cantidades').define("disabled",true);
   $$('btn_ver_cantidades').refresh();
   }*/
  $$('btn_change_estatus').show();
  var label ="Activar producto";
  var type="base";
  if (item.estado==1){
    label = "Desactivar producto";
    type="danger";
  }
  $$('btn_change_estatus').define("type",type);
  $$('btn_change_estatus').define("label",label);
  $$('btn_change_estatus').refresh();
  $$('descripcion').focus();
}
var datatable_articulos ={
  view:"datatable",
  id:"datatable_articulos",
  select:"row",
  navigation:true,
  url:URL_GET_ARTICULOS,
  datafetch:15,
  scheme:{
    $change:function(item){
      if (item.estado == 2)
        item.$css = "inactivo";
      else if (parseInt(item.existencia) <= parseInt(item.existencia_minima) && item.estatus!=2)
        item.$css = "bajo_inventario";
      else
        item.$css="";
    }

  },
  columns:[
    {id:"id",hidden:true},
    {id:"cantidad_unidad",hidden:true},
    {id:"precio_unidad",hidden:true},
    {id:"id_tipo_venta",hidden:true},
    {id:"estado",hidden:true},
    {id:"codigo_barras",width:200,header:["Codigo de barras",{content:"serverFilter"}],sort:"server"},
    {id:"descripcion",fillspace:true,header:["Descripción",{content:"serverFilter"}],sort:"server"},
    {id:"precio_compra",width:120,header:["Precio compra",{content:"serverFilter"}],sort:"server"},
    {id:"precio_venta",width:120,header:["Precio venta",{content:"serverFilter"}],sort:"server"},
    {id:"precio_mayoreo",width:130,header:["Precio mayoreo",{content:"serverFilter"}],sort:"server"},
    {id:"existencia",width:100,header:["Existencia",{content:"serverFilter"}],sort:"server"},
    {id:"existencia_minima",width:130,header:["Existencia min",{content:"serverFilter"}],sort:"server"},
    {id:"tipo_venta",width:120,header:["Tipo de venta",{content:"serverSelectFilter"/*,options :URL_GET_TIPOS_VENTA*/}]},
    {id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados,value:1}]},
  ],
  pager:generic_pager,
  on:{
    onItemDblClick:function(id){
      select(id);
    },
    onKeyPress:function(val){
      if (val==13){
        //select(this.getSelectedId());
      }

    }
  }
};