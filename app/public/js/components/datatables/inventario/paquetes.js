URL_GET_PAQUETES = BASE_URL +"inventario/paquetes/datatable";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
URL_GET_ARTICULOS_SELECT =BASE_URL+"inventario/articulos/select/0";
webix.ajax().post(URL_GET_ARTICULOS_SELECT,{},function(response){
  options =[];
  options.push({id:"0",value:"Todos"});

  response = JSON.parse(response);
  response.forEach(function(item){
    options.push(item);
  });
datatable_paquetes.columns[0].header[1].options=options;
});
var datatable_paquetes={
  view:"datatable",
  id:"datatable_paquetes",
  select:"row",
  datafetch:10,
  scheme:{
    $change:function(item){
      if (item.estado == 2)
        item.$css = "inactivo";
      else
        item.$css="";
    }
  },
  columns:[
    {id:"id",hidden:true},
    {id:"estado",hidden:true},
    {id:"estado",hidden:true},
   // {id:"id_articulo",hidden:true},
    {id:"descripcion",header:["Descripción",{content:"serverFilter"}],sort:"server",fillspace:true},
    {id:"nombre_articulo",header:["Articulo",{content:"serverSelectFilter"}],sort:"server",width:300},
    {id:"precio",header:["Precio",{content:"serverFilter"}],sort:"server",width:100},
  /*  {id:"cantidad_exacta",header:["Cantidad exacta",{content:"serverFilter"}],sort:"server",width:150},
    {id:"cantidad_minima",header:["Cantidad minima",{content:"serverFilter"}],sort:"server",width:150},
    {id:"cantidad_maxima",header:["Cantidad maxima",{content:"serverFilter"}],sort:"server",width:150},*/
    {id:"nombre_tipo_cantidad",header:["Tipo cantidad",{content:"serverFilter"}],sort:"server",width:150},
    {id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados,value:1}]},
  ],
  url:URL_GET_PAQUETES,
  pager:generic_pager,
  on:{
    onItemDblClick:function(id){
      var item = this.getItem(id);
      $$('generic_window').show();
      $$('id_articulo').define("options",URL_GET_ARTICULOS+"/"+item.id_articulo)
      $$('form_paquetes').setValues(item);
      if (obj_pref.maneja_hora){
        var hora_inicio = new Date(item.hora_inicio);
        var hora_fin = new Date(item.hora_fin);
          $$('hora_inicio').setValue(hora_inicio.getHours()+":"+hora_inicio.getMinutes());
          $$('hora_fin').setValue(hora_fin.getHours()+":"+hora_fin.getMinutes());
      }
      $$('btn_change_estatus').show();
      var label ="Activar paquete";
      var type="base";
      if (item.estado==1){
        label = "Desactivar paquete";
        type="danger";
      }
      $$('btn_change_estatus').define("type",type);
      $$('btn_change_estatus').define("label",label);
      $$('btn_change_estatus').refresh();
    }
  }
};
