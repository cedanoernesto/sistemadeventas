URL_GET_SERVICIOS =BASE_URL+"inventario/servicios/datatable";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
var datatable_servicios ={
    view:"datatable",
    id:"datatable_servicios",
    select:"row",
    url:URL_GET_SERVICIOS,
   // datafetch:10,
    scheme:{
        $change:function(item){
            if (item.estado == 2)
                item.$css = "inactivo";
            else
                item.$css="";
        }
    },
    columns:[
        {id:"id",hidden:true},
        {id:"estado",hidden:true},
        {id:"descripcion",width:200,header:["Descripción del servicio",{content:"serverFilter"}],sort:"server",width:300,fillspace:true},
        {id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados, value:1}]},
    ],
    pager:generic_pager,
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);
            $$('generic_window').show();
            $$('form_servicios').setValues(item);
            $$('form_servicios').clearValidation();
            $$('btn_change_estatus').show();
            var label ="Activar servicio";
            var type="base";
            if (item.estado==1){
                label = "Desactivar servicio";
                type="danger";
            }
            $$('btn_change_estatus').define("type",type);
            $$('btn_change_estatus').define("label",label);
            $$('btn_change_estatus').refresh();
        }
    }
};
