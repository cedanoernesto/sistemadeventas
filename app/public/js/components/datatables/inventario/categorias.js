URL_GET_CATEGORIAS=BASE_URL+"inventario/categorias/datatable";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
var datatable_categorias ={
    view:"datatable",
    id:"datatable_categorias",
    select:"row",
    url:URL_GET_CATEGORIAS  ,
    datafetch:10,
    scheme:{
        $change:function(item){
            if (item.estado == 2)
                item.$css = "inactivo";
            else
                item.$css="";
        }

    },
    columns:[
        {id:"id",hidden:true},
        {id:"nombre",width:200,fillspace:true,header:["Nombre de la categoria",{content:"serverFilter"}],sort:"server",width:300},
        {id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados}]},
    ],
    pager:generic_pager,
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);
            $$('generic_window').show();
            $$('form_categorias').setValues(item);
            $$('form_categorias').clearValidation();

            $$('btn_change_estatus').show();
            var label ="Activar categoria";
            var type="base";
            if (item.estado==1){
                label = "Desactivar categoria";
                type="danger";
            }
            $$('btn_change_estatus').define("type",type);
            $$('btn_change_estatus').define("label",label);
            $$('btn_change_estatus').refresh();
            $$('nombre').focus();
        },
        onAfterLoad:function(){
            console.log(this.data);
        }
    }
};
