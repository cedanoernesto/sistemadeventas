URL_GET_CLIENTES =BASE_URL+"inventario/clientes/datatable";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
var datatable_clientes ={
    view:"datatable",
    id:"datatable_clientes",
    select:"row",
    url:URL_GET_CLIENTES,
    datafetch:10,
    scheme:{
        $change:function(item){
            if (item.estado == 2)
                item.$css = "inactivo";
            else
                item.$css="";
        }

    },
    columns:[
        {id:"id",hidden:true},
        {id:"estado",hidden:true},
        {id:"nombre",width:200,header:["Nombre del cliente",{content:"serverFilter"}],sort:"server",width:300},
        {id:"rfc",width:200,header:["RFC",{content:"serverFilter"}],sort:"server",width:300},
        {id:"telefono",header:["Teléfono",{content:"serverFilter"}],sort:"server",width:150},
        {id:"correo_electronico",width:150,header:["Correo electrónico",{content:"serverFilter"}],sort:"server",width:300},
        {id:"domicilio",width:150,header:["Domicilio",{content:"serverFilter"}],sort:"server",fillspace:true},
        {id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados}]},
    ],
    pager:generic_pager,
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);
            $$('generic_window').show();
            $$('form_clientes').setValues(item);
            $$('form_clientes').clearValidation();

            $$('btn_change_estatus').show();
            var label ="Activar Cliente";
            var type="base";
            if (item.estado==1){
                label = "Desactivar cliente";
                type="danger";
            }
            $$('btn_change_estatus').define("type",type);
            $$('btn_change_estatus').define("label",label);
            $$('btn_change_estatus').refresh();
        },
        onAfterLoad:function(){
            console.log(this.data);
        }
    }
};
