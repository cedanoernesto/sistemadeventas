URL_GET_USUARIOS =BASE_URL+"sistema/usuarios/datatable";
var datatable_usuarios ={
    view:"datatable",
    id:"datatable_usuarios",
    select:"row",
    url:URL_GET_USUARIOS,
    datafetch:10,
    scheme:{
        $change:function(item){
            if (item.estado == 2)
                item.$css = "inactivo";
            else
                item.$css="";
        }
    },
    columns:[
        {id:"id",hidden:true},
        {id:"estado",hidden:true},
        {id:"id_perfil",hidden:true},
        {id:"nombre",width:200,header:["Nombre del usuario",{content:"serverFilter"}],sort:"server",width:300,fillspace:true},
        {id:"usuario",width:200,header:["Usuario",{content:"serverFilter"}],sort:"server",width:300},
        {id:"nombre_perfil",width:200,header:["Tipo de usuario",{content:"serverSelectFilter",options:[]}],sort:"server",width:300},
        //{id:"correo_electronico",width:150,header:["Correo electrónico",{content:"serverFilter"}],sort:"server",width:300},
        {id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados}]},
    ],
    pager:generic_pager,
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);
            $$('generic_window').show();
            $$('form_usuarios').setValues(item);
            $$('form_usuarios').clearValidation();

            $$('btn_change_estatus').show();
            var label ="Activar usuario";
            var type="base";
            if (item.estado==1){
                label = "Desactivar usuario";
                type="danger";
            }
            $$('btn_change_estatus').define("type",type);
            $$('btn_change_estatus').define("label",label);
            $$('btn_change_estatus').refresh();
        },
        onAfterLoad:function(){
            console.log(this.data);
        }
    }
};
