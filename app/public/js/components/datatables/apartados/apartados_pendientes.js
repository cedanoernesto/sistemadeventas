URL_GET_APARTADOS_PENDIENTES =BASE_URL+"apartados/apartados_pendientes/datatable";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
var datatable_apartados_pendientes ={
    view:"datatable",
    id:"datatable_apartados_pendientes",
    select:"row",
    url:URL_GET_APARTADOS_PENDIENTES,
    datafetch:10,
    scheme:{
        $change:function(item){
            if (item.estado == 2)
                item.$css = "inactivo";
            else
                item.$css="";
        }

    },
    columns:[
        {id:"id",hidden:true},
        {id:"estado",hidden:true},
        {id:"folio",header:["Folio",{content:"serverFilter"}],sort:"server",width:150},
        {id:"fecha",header:["Fecha",{content:"serverFilter"}],sort:"server",width:200},
        {id:"nombre",width:200,header:["Nombre del cliente",{content:"serverFilter"}],sort:"server",width:300,fillspace:true},
        {id:"total",header:["Total",{content:"serverFilter"}],sort:"server",width:150},
        {id:"saldo",header:["Saldo",{content:"serverFilter"}],sort:"server",width:150},
        {id:"fecha_vencimiento",header:["Fecha de vencimiento",{content:"serverFilter"}],sort:"server",width:200},
        {id:"fecha_vencimiento_tolerancia",width:150,header:["Fecha de tolerancia",{content:"serverFilter"}],sort:"server"},
        //{id:"estatus",width:130,header:["Estado",{content:"serverSelectFilter",options:options_estados}]},
    ],
    pager:generic_pager,
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);
            $$('generic_window').show();
            $$('form_apartados_pendientes').setValues(item);
            $$('form_apartados_pendientes').clearValidation();
            var fecha_tolerancia = $$('fecha_vencimiento_tolerancia').getPopup().getBody();
            fecha_tolerancia.define('minDate',new Date(item.fecha_vencimiento));
            fecha_tolerancia.refresh();

            
            /*
            $$('btn_change_estatus').show();
            var label ="Activar Apartado";
            var type="base";
            if (item.estado==1){
                label = "Desactivar Apartado";
                type="danger";
            }
            $$('btn_change_estatus').define("type",type);
            $$('btn_change_estatus').define("label",label);
            $$('btn_change_estatus').refresh();
            */
        }
    }
};
