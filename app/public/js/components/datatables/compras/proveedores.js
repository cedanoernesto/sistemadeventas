URL_GET_PROVEEDORES =BASE_URL+"compras/proveedores/datatable";
URL_GET_ESTADOS =BASE_URL+"sistema/general/getEstados";
var datatable_proveedores ={
    view:"datatable",
    id:"datatable_proveedores",
    select:"row",
    url:URL_GET_PROVEEDORES,
    datafetch:10,
    scheme:{
        $change:function(item){
            if (item.estado == 2)
                item.$css = "inactivo";
            else
            item.$css="";
        }

    },
    columns:[
        {id:"id",hidden:true},
        {id:"nombre",width:200,header:["Nombre del proveedor",{content:"serverFilter"}],sort:"server",width:300},
        {id:"rfc",header:["RFC",{content:"serverFilter"}],sort:"server",width:150},
        {id:"telefono",header:["Teléfono",{content:"serverFilter"}],sort:"server",width:150},
        {id:"correo_electronico",width:150,header:["Correo electrónico",{content:"serverFilter"}],sort:"server",width:200},
        {id:"domicilio",width:150,header:["Domicilio",{content:"serverFilter"}],sort:"server",fillspace:true},
        {id:"estatus",width:150,header:["Estatus",{content:"serverSelectFilter",options:URL_GET_ESTADOS}]}
    ],
    pager:generic_pager,
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);
            $$('generic_window').show();
            $$('form_proveedores').setValues(item);
            $$('form_proveedores').clearValidation();


            $$('btn_change_estatus').show();
            var label ="Activar proveedor";
            var type="base";
            if (item.estado==1){
                label = "Desactivar proveedor";
                type="danger";
            }
            $$('btn_change_estatus').define("type",type);
            $$('btn_change_estatus').define("label",label);
            $$('btn_change_estatus').refresh();
        },
        onAfterLoad:function(){
            $$('datatable_proveedores').unselectAll();
        }
    }
};
