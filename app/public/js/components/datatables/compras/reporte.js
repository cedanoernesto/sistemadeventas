URL_GET_REPORTE =BASE_URL+"compras/reporte/datatable/";
URL_GET_REPORTE_DETALLE =BASE_URL+"compras/reporte/detalle/";
var datatable_reporte ={
    view:"datatable",
    id:"datatable_reporte",
    select:"row",
    url:URL_GET_REPORTE+2,
    footer:true,
    datafetch:10,
    columns:[
        {id:"tipo",hidden:true},
        {id:"fecha",width:200,header:"Fecha",fillspace:true,sort:"server",width:300,footer:{text:"Total:", colspan:1 }},
        {id:"total",header:"Total",sort:"server",width:200, footer:{content:"summColumn"}},
    ],
    on:{
        onItemClick:function(id){
            var item = this.getItem(id);
            var pr="";
            if(item.tipo==1){
                pr="1/"+item.id;
            }
            else if (item.tipo==2){
                pr="2/"+item._fecha;
            }
            else if(item.tipo==3){
                pr="3/"+item._fecha;
            }
            $$('datatable_reporte_detalles').clearAll();
            $$('datatable_reporte_detalles').define('url',URL_GET_REPORTE_DETALLE+pr);
            $$('datatable_reporte_detalles').refresh();
            $$('datatable_reporte_detalles').showOverlay("Cargando..");
        },

        onAfterLoad:function(){

        }
    }
};
var datatable_reporte_detalles ={
    view:"datatable",
    id:"datatable_reporte_detalles",
    url:URL_GET_REPORTE_DETALLE,
    datafetch:10,
    columns:[
        {id:"tipo",hidden:true},
        {id:"descripcion",width:200,header:"Descripcion",fillspace:true,sort:"server",footer:{text:"Total:", colspan:2 }},
        {id:"precio_compra",header:"Precio de compra",width:150,sort:"server", footer:{content:"summColumn"}},
        {id:"cantidad",width:100,header:"Cantidad",sort:"server"},
        {id:"total",width:120,header:"Total",sort:"server"},
    ],
    on:{
        onItemDblClick:function(id){
            var item = this.getItem(id);

        },
        onBeforeLoad:function(){
            this.showOverlay("Seleccione un registro del resumen para ver los detalles");
            // $$('datatable_proveedores').unselectAll();
        },
        onAfterLoad:function () {
            this.hideOverlay();
        }
    }
};

