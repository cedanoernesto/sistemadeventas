URL_DATA_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/data';
URL_DATATABLE_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/ventas/';
URL_DETALLE_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/venta';
URL_TICKET_CORTE_CAJA = BASE_URL +'ventas/ventas/ticket/';
URL_TICKET_CORTE_CAJA_DETALLES = BASE_URL +'ventas/tickets_anteriores/imprimirCorte';
URL_SAVE_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/save/';
URL_SAVE_MULTIPLE_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/saveMultiple/';
URL_LISTA_MULTIPLE_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/getMultiple/';
URL_CHECK_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/check/';
URL_GASTOS_CORTE_CAJA = BASE_URL +'ventas/tickets_anteriores/gastos/';
URL_SALIDAS = BASE_URL +'ventas/tickets_anteriores/salidas/';
var total_caja =0;

var window_multiple_corte={
    view:'window',
    id:'window_multiple_corte',
    width:350,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Corte de caja",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    position:'center',
    modal:true,
    body:{
        rows:[
            {
                view:'form',
                id:'form_multiple_corte',
                elements:[
                    {
                        cols: [
                            {view:'label',
                                label:'Total en caja:   ',
                                width:100},
                            {
                                view:'label',
                                id:'label_total_caja',
                                css:'valor'
                            },

                        ]
                    },
                    {
                        view:'text',
                        id:'monto_retirar_corte_caja',
                        name:'monto_retirar_corte_caja',
                        required:true,
                        label:'Monto a retirar'
                    }
                ],
                elementsConfig:{
                    labelPosition:"top"
                },
                rules:{
                    'monto_retirar_corte_caja':function (val) {
                        if (webix.rules.isNumber(val)){
                            if (parseFloat(val)<= total_caja){
                                return true;
                            }
                            else {
                                webix.alert("El monto no puede ser mayor a : " + "<b>"+total_caja+"</b>");
                                return false;
                            }
                        }
                        else
                            return false;

                    }
                },
                on:{
                    onSubmit:'save_multiple_corte_caja()'
                }
            },
            {
                cols:[
                    {},
                    {view:'button',type:'imageButton',image:BASE_URL_ICONS+'success.png',label:'Aceptar',width:100,click:'save_multiple_corte_caja()'}
                ]

            }
        ]
    }
}


var window_mostrar_multiple_corte_caja={
    view:'window',
    id:'window_mostrar_multiple_corte_caja',
    width:500,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Detalle de cortes de caja",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    position:'center',
    modal:true,
    body:{
        rows:[
            {
                view:'list',
                id:'lista_cortes',
                template:"<b>#value#</b>"
            }
        ]
    }
}

var window_gastos={
    view:'window',
    id:'window_gastos',
    width:750,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Detalle de gastos",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    position:'center',
    modal:true,
    body:{
        rows:[
            {
                view:'list',
                id:'lista_gastos',
                template:"<b>#value#</b>"
            }
        ]
    }
}

var window_salidas={
    view:'window',
    id:'window_salidas',
    width:400,
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Detalle de salidas",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    position:'center',
    modal:true,
    body:{
        rows:[
            {
                view:'list',
                id:'lista_salidas',
                template:"<b>#value#</b>"
            }
        ]
    }
}



var window_corte_caja ={
    view:'window',
    id:'window_corte_caja',
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Crear corte de caja",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    position:'center',
    modal:true,
    body:{
        view:'form',
        id:'form_corte_caja',
        elements:[
            {
                rows:[
                    {view:'text',label:'Fondo',id:'fondo',name:'fondo',invalidMessage:required},
                    {view:'datepicker',label:'Para el dia de',id:'fecha',name:'fecha',invalidMessage:'Ingrese una fecha',format:'%Y-%m-%d'},
                    {cols:[
                        {},
                        {view:'button',type:'imageButton',image:BASE_URL_ICONS+'success.png',label:'Aceptar',width:100,click:'save()'}
                    ]}
                ]
            }
        ],
        elementsConfig:{
            labelPosition:'top'
        },
        rules:{
            'fondo':webix.rules.isNumber,
            'fecha':webix.rules.isNotEmpty
        },
        on:{
            onSubmit:'save()'
        }

    }
}



var window_detalle={
    view:'window',
    id:'window_detalle',
    modal:true,
    width:600,
    height:500,
    position:'center',
    head:toolbar_header,
    body:{
        rows:[
            {
                view:'datatable',
                id:'datatable_detalle',
                columns:[
                    {id:'descripcion',header:['Descripcion',{content:'textFilter'}],fillspace:true},
                    {id:'precio',header:['Precio',{content:'textFilter'}]},
                    {id:'cantidad',header:['Cantidad',{content:'textFilter'}]},
                    {id:'subtotal',header:['Subtotal',{content:'textFilter'}]},

                ]
            }
        ]
    }
}

var view_corte_caja={

    rows:[
        /*{
            cols:[

            ]
        },*/
        {view:'fieldset',
            label:'Totales de venta',
            body:{
                rows:[
                    {
                        cols:[
                            {
                                id:'cont_tipo_efectivo',
                                cols:[
                                    {
                                        rows:[
                                            {view:'label',label:'Numero de ventas (Efectivo): ',width:190,css:'titulo'},
                                            {view:'label',label:'Total por ventas: ',width:190,css:'titulo'},
                                        ]
                                    },
                                    {
                                        rows:[
                                            {view:'label',label:'0',id:'numero_ventas',width:100,align:'left',css:'valor'},
                                            {view:'label',label:'0',id:'total_ventas',width:100,align:'left',css:'valor'},
                                        ]
                                    }
                                ]
                            },
                            {
                                id:'cont_tipo_copia',
                                cols:[
                                    {
                                        rows:[
                                            {view:'label',label:'Numero de ventas por copia: ',width:190,css:'titulo'},
                                            {view:'label',label:'Total de ventas por copia: ',width:190,css:'titulo'},

                                        ]
                                    },
                                    {
                                        rows:[
                                            {view:'label',label:'0',id:'numero_copias',width:100,css:'valor'},
                                            {view:'label',label:'0',id:'total_copias',width:100,css:'valor'},
                                        ]
                                    }
                                ]
                            },
                            {
                                id:'cont_tipo_apartados',
                                cols:[
                                    {
                                        rows:[
                                            {view:'label',label:'Pagos por apartados: ',width:140,css:'titulo'},
                                            {view:'label',label:'Total por apartados: ',width:140,css:'titulo'},

                                        ]
                                    },
                                    {
                                        rows:[
                                            {view:'label',label:'0',id:'numero_apartados',width:100,css:'valor'},
                                            {view:'label',label:'0',id:'total_apartados',width:100,css:'valor'},
                                        ]
                                    }
                                ]
                            },
                            {
                                id:'cont_pago_tarjeta',
                                cols:[
                                    {
                                        rows:[
                                            {view:'label',label:'Pagos con tarjeta: ',width:140,css:'titulo'},
                                            {view:'label',label:'Total por tarjetas: ',width:140,css:'titulo'},
                                        ]
                                    },
                                    {
                                        rows:[
                                            {view:'label',label:'0',id:'numero_tarjeta',width:100,css:'valor'},
                                            {view:'label',label:'0',id:'total_tarjetas',width:100,css:'valor'},
                                        ]
                                    }
                                ]
                            },
                            {
                                id:'cont_total',
                                cols:[
                                    {
                                        rows:[
                                            {view:'label',label:'Ventas totales: ',width:120,css:'titulo'},
                                            {view:'label',label:'Total por ventas: ',width:120,css:'titulo'},

                                        ]
                                    },
                                    {
                                        rows:[
                                            {view:'label',label:'0',id:'numero_ventas_totales',width:100,css:'valor'},
                                            {view:'label',label:'0',id:'total_ventas_total',width:100,css:'valor'},
                                        ]
                                    }
                                ]
                            },




                        ]
                    }
                    /*  {
                          cols:[
                              {view:'label',label:'Numero de ventas (Efectivo): ',width:190,css:'titulo'},
                              {view:'label',label:'0',id:'numero_ventas',width:100,align:'left',css:'valor'},
                              {view:'label',label:'Numero de ventas por copia: ',width:190,css:'titulo'},
                              {view:'label',label:'0',id:'numero_copias',width:100,css:'valor'},
                              {view:'label',label:'Pagos por apartados: ',width:140,css:'titulo'},
                              {view:'label',label:'0',id:'numero_apartados',width:100,css:'valor'},
                              {view:'label',label:'Pagos con tarjeta: ',width:140,css:'titulo'},
                              {view:'label',label:'0',id:'numero_tarjeta',width:100,css:'valor'},
                              {view:'label',label:'Ventas totales: ',width:120,css:'titulo'},
                              {view:'label',label:'0',id:'numero_ventas_totales',width:100,css:'valor'},
                          ]
                      },
                      {
                          cols:[
                              {view:'label',label:'Total por ventas: ',width:190,css:'titulo'},
                              {view:'label',label:'0',id:'total_ventas',width:100,align:'left',css:'valor'},
                              {view:'label',label:'Total de ventas por copia: ',width:190,css:'titulo'},
                              {view:'label',label:'0',id:'total_copias',width:100,css:'valor'},
                              {view:'label',label:'Total por apartados: ',width:140,css:'titulo'},
                              {view:'label',label:'0',id:'total_apartados',width:100,css:'valor'},
                              {view:'label',label:'Total por tarjetas: ',width:140,css:'titulo'},
                              {view:'label',label:'0',id:'total_tarjetas',width:100,css:'valor'},
                              {view:'label',label:'Total por ventas: ',width:120,css:'titulo'},
                              {view:'label',label:'0',id:'total_ventas_total',width:100,css:'valor'},
                          ]
                      }*/
                ]
            }
        },
        {
            cols:[
                {width:600},
                {view:'label',label:'Caja: ',css:'titulo_final',width:50,css:'valor_ganancia'},
                {view:'label',label:'Gastos: ',template:'<a href="#" class="a_titulo_final">#label#</a>',width:65,css:'titulo_final',click:'gastos()'},
                {view:'label',label:'0',id:'gastos',width:80,css:'valor_gastos'},
                {view:'label',label:'Salidas: ',template:'<a href="#" class="a_titulo_final">#label#</a>',width:65,css:'titulo_final',click:'salidas()'},
                {view:'label',label:'0',id:'salidas',width:80,css:'valor'},
                {view:'label',label:'Fondo: ',width:65,css:'titulo_final'},
                {view:'label',label:'0',id:'label_fondo',width:70,css:'valor'},
                {       id:'cont_cortes',
                    cols:[
                        {view:'label',label:'Cortes: ',width:65,css:'titulo_final',template:"<a href='#'>#label#</a>",click:'MostrarCortes()'},
                        {view:'label',label:'0',id:'label_cortes',width:70,css:'valor'},
                    ]
                },
                {view:'label',label:'Total: ',width:60,css:'titulo_final'},
                {view:'label',label:'0',id:'total',width:100,css:'valor'},
            ]
        },
        {
            cols:[
                {view:'datepicker',id:'fecha',label:'Seleccionar fecha',labelPosition:'top',format:'%Y-%m-%d',on:{
                    onChange:function (value) {
                        data(value);
                    }
                }},
                {
                    rows:[
                        {},
                        {view:'button',id:'btn_corte_caja',label:'Imprimir todos los tickets',width:200,click:'imprimirTickets()'},
                    ]
                },
                //{view:'button',type:'imageButton',image:BASE_URL_ICONS+'export_pdf.png',label:'Generar reporte del dia',width:200,click:'generarReporte()'},
                //{view:'button',type:'imageButton',label:'Imprimir corte del dia',width:200,click:'imprimirCorte()'},
                {width:250},
                {view:'label',label:'Ganancias: ',css:'titulo_final',width:100,css:'valor_ganancia'},
                {view:'label',label:'Ventas: ',width:65,css:'titulo_final'},
                {view:'label',label:'0',id:'ganancia_ventas',width:100,css:'valor_ganancia'},
                {
                    id:'cont_ganancia_copias',
                    cols:[
                        {view:'label',label:'Copias: ',width:65,css:'titulo_final'},
                        {view:'label',label:'0',id:'ganancia_copias',width:100,css:'valor_ganancia'},
                    ]
                },
                {view:'label',label:'Total: ',width:60,css:'titulo_final'},
                {view:'label',label:'0',id:'ganancia_total',width:100,css:'valor_ganancia'},
            ]
        },
        {
            cols:[
                {width:10},
                {
                    rows:[
                        {
                            view:'datatable',
                            id:'datatable_corte_caja',
                            select:'row',
                            url:URL_DATATABLE_CORTE_CAJA,
                            columns:[
                                {id:'id',hidden:true},
                                {id:'folio',header:['Folio',{content:'serverFilter'}]},
                                {id:'fecha',header:['Fecha',{content:'serverFilter'}],fillspace:true},
                                {id:'total',header:['Total',{content:'serverFilter'}]},
                                {id:'tipo',header:['Tipo',{content:'serverSelectFilter',options:[{id:1,value:'Venta'},{id:2,value:'Apartado'},{id:3,value:'Copia'}]}]},
                                {header:'Ver ticket',template:"<a onclick='verTicket(#id#)' href='#'>Imprimir</a>"}
                            ],
                            on:{
                                onItemDblClick:function(id){
                                    var ide = this.getItem(id).ide;
                                    webix.ajax().post(URL_DETALLE_CORTE_CAJA,{id:ide},function(response){
                                        response = JSON.parse(response);
                                        $$('window_detalle').show();
                                        $$('datatable_detalle').clearAll();
                                        $$('datatable_detalle').define('data',response);
                                        $$('datatable_detalle').refresh();

                                    });
                                }
                            },
                            pager:generic_pager
                            //pager:'pager'
                        },
                    ]
                },
            ]
        }
    ]
}
