

function MostrarCortes() {
    $$('window_mostrar_multiple_corte_caja').show();
    $$('lista_cortes').define('url',URL_LISTA_MULTIPLE_CORTE_CAJA);
    $$('lista_cortes').refesh();
}
function  save_multiple_corte_caja(){
    if ($$('form_multiple_corte').validate()){
        webix.ajax().post(URL_SAVE_MULTIPLE_CORTE_CAJA,$$('form_multiple_corte').getValues(),function (response) {
            response = JSON.parse(response);
            $$('window_multiple_corte').hide();
            webix.alert(response.message);
        });
    }
}



function loadChart(){
    webix.ajax().post(URL_DATA_CORTE_CAJA,{},function(response){
        response = JSON.parse(response);
        var chart = $('#chart').highcharts();
        chart.addSeries({
            name: 'Porcentaje',
            colorByPoint: true,
            data:[{name:'Ventas (Efectivo)',y:response.total_ventas/response.total*100},{name:'Ventas por copia',y:response.total_copias/response.total*100},{name:'Por apartados',y:response.total_apartados/response.total*100},{name:'Tarjetas credito/debito',y:response.total_tarjetas/response.total*100}]
        });
    });
}



function generarReporte(){
    webix.confirm({
        text:'¿Desea generar el reporte con detalles de venta?',
        ok:'Si',
        cancel:'No',
        callback:function(result){
            var detalle =(result)?'356a192b7913b04c54574d18c28d46e6395428ab':'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c';
            window.open(BASE_URL+'reportes/pdf/ventas?detalle='+detalle+'&tipo=356a192b7913b04c54574d18c28d46e6395428ab'+'&group=356a192b7913b04c54574d18c28d46e6395428ab');
        }
    });
}


function verTicket(id){
    var ide = $$('datatable_corte_caja').getItem(id).ide;
    webix.ajax().get(URL_TICKET_CORTE_CAJA+ide,function (response) {
        response = JSON.parse(response);
        imprimirTicket(response)
    })
}


function data(){
    var data;
    webix.ajax().post(URL_DATA_CORTE_CAJA,{},function(response){
        response = JSON.parse(response);
        $$('numero_ventas').define('label',response.numero_ventas);
        $$('numero_ventas').refresh();
        $$('numero_copias').define('label',response.numero_copias);
        $$('numero_copias').refresh();
        $$('numero_apartados').define('label',response.numero_apartados);
        $$('numero_apartados').refresh();
        $$('numero_tarjeta').define('label',response.numero_tarjeta);
        $$('numero_tarjeta').refresh();
        $$('numero_ventas_totales').define('label',response.numero_total);
        $$('numero_ventas_totales').refresh();

        $$('total_ventas').define('label',response.total_ventas);
        $$('total_ventas').refresh();
        $$('total_copias').define('label',response.total_copias);
        $$('total_copias').refresh();
        $$('total_apartados').define('label',response.total_apartados);
        $$('total_apartados').refresh();
        $$('total_tarjetas').define('label',response.total_tarjetas);
        $$('total_tarjetas').refresh();

        $$('total_ventas_total').define('label',response.total);
        $$('total_ventas_total').refresh();

        $$('gastos').define('label',response.gastos);
        $$('gastos').refresh();

        $$('salidas').define('label',response.salidas);
        $$('salidas').refresh();

        $$('label_fondo').define('label',response.fondo);
        $$('label_fondo').refresh();

        $$('label_cortes').define('label',parseFloat(response.total_cortes).toFixed(2));
        $$('label_cortes').refresh();

        total_caja = ((parseFloat(response.fondo)+parseFloat(response.total_caja))-parseFloat(response.gastos)) - parseFloat(response.total_cortes);
        $$('total').define('label',total_caja.toFixed(2));
        $$('total').refresh();

        $$('ganancia_ventas').define('label',response.ganancia_ventas.toFixed(2));
        $$('ganancia_ventas').refresh();
        $$('ganancia_copias').define('label',parseFloat(response.ganancia_copias).toFixed(2));
        $$('ganancia_copias').refresh();
        $$('ganancia_total').define('label',(parseFloat(response.ganancia_total)-parseFloat(response.gastos)).toFixed(2));
        $$('ganancia_total').refresh();

    });
}
function corte_caja(){
    if (!parseInt(obj_pref.corte)){
        webix.confirm({
            text:'¿Desea crear el corte de caja?',
            ok:'Si',
            cancel:'No',
            callback:function(result){
                if (result){
                    $$('form_corte_caja').clear();
                    $$('form_corte_caja').clearValidation();
                    var calendar = $$('fecha').getPopup().getBody();
                    var date_min =new Date();
                    var date_max =new Date();
                    date_min.setDate(date_min.getDate()-1);
                    calendar.define('minDate',date_min);
                    date_max.setDate(date_max.getDate()+1);
                    calendar.define('maxDate',date_max);
                    $$('fecha').refresh();
                    $$('window_corte_caja').show();
                    $$('fondo').focus();
                    var date = new Date();
                    date.setDate(date.getDate()+1);
                    $$('fecha').setValue(date);
                }
            }
        });
    }
    else{
        $$('window_multiple_corte').show();
        $$('label_total_caja').define('label',total_caja);
        $$('label_total_caja').refresh();
        $$('monto_retirar_corte_caja').setValue(total_caja);
        $$('monto_retirar_corte_caja').focus();

    }

}

function check(){
    webix.ajax().post(URL_CHECK_CORTE_CAJA,{},function(response){
        if (parseInt(response)>0)
            $$('btn_corte_caja').define('disabled',true);
    });

}
function save(){
    if ($$('form_corte_caja').validate()){
        webix.ajax().post(URL_SAVE_CORTE_CAJA,$$('form_corte_caja').getValues(),function(response){
            response = JSON.parse(response);
            $$('window_corte_caja').hide();
            webix.alert(response.message);
            $$('btn_corte_caja').define('disabled',true);
        });
    }
    else
        webix.alert({text:'Complete los campos requeridos.',type:'alert-error'});
}

function gastos(){
    $$('window_gastos').show();
    webix.ajax().post(URL_GASTOS_CORTE_CAJA,{},function(response){
        response = JSON.parse(response);
        $$('lista_gastos').clearAll();
        $$('lista_gastos').define('data',response);
        $$('lista_gastos').refresh();
    });
}

function salidas(){
    $$('window_salidas').show();
    webix.ajax().post(URL_SALIDAS,{},function(response){
        response = JSON.parse(response);
        $$('lista_salidas').clearAll();
        $$('lista_salidas').define('data',response);
        $$('lista_salidas').refresh();
    });
}
function imprimirCorte() {
    webix.confirm({
        text:"¿Desea imprimir ticket de corte de caja?",
        ok:"Si",
        cancel:"No",
        callback:function(res){
            if (res){
                webix.ajax().get(URL_TICKET_CORTE_CAJA_DETALLES, function (response) {
                    response = JSON.parse(response);
                    var config = qz.configs.create(response.datos_ticket.impresora);
                    var data = [
                        '\x1B' + '\x40',          // init
                        '\x1B' + '\x61' + '\x31', // center align
                        '\x1B' + '\x21' + '\x30', // em mode on
                        response.datos_ticket.nombre_negocio , ' \n \x0A',
                        '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
                        response.datos_ticket.datos_fiscales + '\n'+
                        '\x1B' + '\x61' + '\x31', // center align
                        'CORTE DE CAJA',
                        '\x0A',
                        response.fecha + '\n',
                        '\x1B' + '\x61' + '\x30', // left align
                    ];
                    function pad(input, length, padding) {
                        var str = input + "";
                        return (str.length > length) ? str.substr(0,34) + ' '  : pad((arguments[3] != undefined)?str:str+padding, length, padding);

                    }
                    data.push(
                        '\x0A',
                        ' TOTAL POR VENTAS: ',
                        '\x1B' + '\x45' + '\x0D', // bold on
                        response.total_ventas,
                        '\x1B' + '\x45' + '\x0A', // bold of
                        '\n',
                        ' GASTOS: ',
                        '\x1B' + '\x45' + '\x0D', // bold on
                        response.gastos,
                        '\x1B' + '\x45' + '\x0A', // bold of
                        '\n ',
                        'TOTAL: ',
                        '\x1B' + '\x45' + '\x0D', // bold on
                        response.total,
                        '\x1B' + '\x45' + '\x0A', // bold of
                        '\x0A',
                        '\x0A'
                    )
                    data.push( '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A',
                        '\x1B', '\x69');
                    qz.print(config,data).catch(function (e) {
                        console.error(e)
                    })
                })
            }
        }
    });
    //window.open()
}
function imprimirTicket(response) {
    var config = qz.configs.create(response.datos_ticket.impresora);
    var data = [
        '\x1B' + '\x40',          // init
        '\x1B' + '\x61' + '\x31', // center align
        '\x1B' + '\x21' + '\x30', // em mode on
        response.datos_ticket.nombre_negocio , ' \n \x0A',
        '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
        response.datos_ticket.datos_fiscales + '\n'+
        '\x1B' + '\x61' + '\x30', // left align
        'FOLIO: ' + response.venta.folio + '        ' + response.venta.fecha + '\n',
        'ATENDIO: ' + response.venta.nombre + '\x0A.',
        '\x0A',
        '#  ' + 'DESCRIPCION              ' + 'CANT   ' + 'PREC   ' + 'SUB   \n'
    ];
    function pad(input, length, padding) {
        var str = input + "";
        //console.log(arguments[3])
        return (str.length > length) ? str.substr(0,34) + ' '  : pad((arguments[3] != undefined)?str:str+padding, length, padding);

    }
    response.detalles.forEach(function (item, index) {
        item.descripcion = pad(item.descripcion,34,' ')
        item.cantidad = pad(item.cantidad,7,' ')
        item.precio = pad(item.precio,7,' ')
        item.subtotal = pad(item.subtotal,6,' ',true)
        data.push( '\x1B' + '\x4D' + '\x31')
        data.push(index+1,'  ' + item.descripcion + item.cantidad  + item.precio + item.subtotal + '\n')
        data.push( '\x1B' + '\x4D' + '\x30')
    })
    if(response.venta.descuento != '0.00') {
        data.push(
            '\x0A',
            '\x1B' + '\x61' + '\x32',
            '\x1B' + '\x45' + '\x0D', // bold on
            'TOTAL: ' + (parseFloat(response.venta.descuento) + parseFloat(response.venta.total)).toFixed(2),
            '\n',
            'DESCUENTO('+response.venta.tipo_descuento+'): ' +response.venta.descuento,
            '\n'
        )
    }
    data.push(
        '\x1B' + '\x61' + '\x30', // left align
        '\x0A',
        'PAGO: ' + response.venta.tipo_pago
    )
    data.push(
        '\x0A',
        '\x1B' + '\x61' + '\x32',
        '\x1B' + '\x45' + '\x0D', // bold on
        'TOTAL: ' + response.venta.total,
        '\n',
        'PAGO: ' +response.venta.pago,
        '\n ',
        'CAMBIO: ' + response.venta.cambio,
        '\x0A',
        '\x0A',
        '\x1B' + '\x61' + '\x31', //center align
        response.datos_ticket.mensaje_gracias + ' \x0A',
        '\x1B' + '\x45' + '\x0A' + response.datos_ticket.advertencia, // bold of
        '\x0A' + 'REIMPRESION'
        //
    )
    data.push( '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A',
        '\x1B', '\x69');
    //var data = [response]
    qz.print(config,data).catch(function (e) {
        console.error(e)
    })
}

