function uniqueId() {
    var date = Date.now();
    // If created at same millisecond as previous
    if (date <= uniqueId.previous) {
        date = ++uniqueId.previous;
    } else {
        uniqueId.previous = date;
    }

    return date;
}
function cancelacionVenta(){
    webix.confirm({
        text:'¿Desea cancelar esta venta?',
        ok:'Si',
        cancel:'No',
        callback:function (result) {
            if (result){
                webix.ajax().post(
                    URL_CANCELAR_VENTA,
                    {id:id_venta_cancelar},
                    function(response){
                        response = JSON.parse(response);
                        webix.alert(response.message);
                        actualizarArticulos();
                        $$('window_cancelacion').hide();

                    });
            }

        }
    });
};
function verTicket(url){
    window.open(url,"_blank","width=400 height=900");
}
function buscar_venta(){
    if ($$('form_cancelacion').validate()){
        webix.ajax().post(URL_BUSCAR_VENTA,$$('form_cancelacion').getValues(),function(response){
            response = JSON.parse(response);
            if (response.estatus){
                $$('label_ticket').define('label','Ver ticket');
                $$('label_ticket').refresh();
                $$('label_ticket').attachEvent('onItemClick',function () {
                    verTicket(response.url);
                });
                $$('cancelar_venta').show();
                id_venta_cancelar = response.id;
            }
            else{
                webix.alert({
                    type:'alert-error',
                    text:response.message,
                    callback:function () {
                        $$('folio_cancelacion').setValue('');
                        $$('folio_cancelacion').focus();
                        $$('cancelar_venta').hide();
                        $$('label_ticket').define('label','');
                        $$('label_ticket').refresh();
                    }
                });
            }
        });
    }
}
function confirmaMayoreo(f){
    webix.confirm({
        text: '¿Desea aplicar precio de mayoreo?',
        ok: 'Si',
        cancel: 'No',
        callback: function (result) {
            precioMayoreo(result, true);
            f();
            if (result)
                webix.message("Precio de mayoreo aplicado");
        }
    });

}

function aplicarIvaCopia(val){
    var precio_copia_temp = precio_copia* parseFloat($$('cantidad_copia').getValue());
    if (!isNaN(precio_copia_temp)){
        if (val){
            total_copia = precio_copia_temp*1.16;
        }
        else{
            total_copia = precio_copia_temp;
        }
        $$('label_total_copia').define("label","$"+total_copia.toFixed(2));
        $$('label_total_copia').refresh();

    }

}
function aplicarPrecio(item,articulo){
    if (item.tipo_venta==1){
        return parseFloat(articulo.precio_unidad);
    }
    else if (item.tipo_venta==2){
        return parseFloat(articulo.precio_venta);
    }
}
function aplicarPrecioMayoreo(item,cantidad){
    var flag=false;
    cantidad = parseFloat(cantidad);
    art = item;
    if (item.cantidad_mayoreo!=0 && tipo_venta==2){
        if ($$('precio_mayoreo').getValue())
            flag=true;
        else if (cantidad>=parseFloat(item.cantidad_mayoreo))
            flag=true;
        else
            flag=false;
    }
    if ( item.cantidad_unidad_mayoreo!=0 && tipo_venta==1){
        if ($$('precio_mayoreo').getValue())
            flag=true;
        else  if (cantidad>=parseFloat(item.cantidad_unidad_mayoreo))
            flag=true;
        else
            flag=false;
    }

    precioMayoreo(flag,true);
    return flag;
}
function agregarComun(){
    if ($$('form_comun').validate()){
        var datatable = $$('datatable_ventas');
        var sub = 0;
        var fg =0;
        var view_lenght =$$('comun_precio').getChildViews().length;
        for(var i=1; i<= view_lenght; i++){
            subt_temp =parseFloat($$('comun_precio_'+1).getValue());
            if (subt_temp == parseFloat($$('comun_precio_'+i).getValue()))
                fg++;
            sub += parseFloat($$('comun_precio_'+i).getValue());
        }

        var detail = {
            venta_unidad:0,
            comun:1,
            precio_mayoreo:0,
            tipo_venta:2,
            id_articulo:$$('id_articulo').getValue(),
            descripcion:$$('comun_descripcion').getValue(),
            cantidad:view_lenght,
            precio:subt_temp,
            subtotal:sub,
            flag:true,
            index: venta.length
        }
        if (fg == view_lenght){
            detail.cantidad = view_lenght;}
        else{
            for(var i=1; i<= view_lenght; i++){
                sub = parseFloat($$('comun_precio_'+i).getValue());
                detail.cantidad = 1;
                detail.subtotal = sub;
                detail.precio = sub;
            }
        }
        addDetail(detail)
        $$('window_comun').hide();
        $$('id_articulo').setValue("0");
        $$('cantidad').setValue('0');
        $$('cantidad').define('disabled',true);
        art.comun=0;
        $$('id_articulo').focus();
    }

}

function cantidadComun(val){
    var  inc =0;
    for(var i=1; i<= $$('comun_precio').getChildViews().length+val; i++){
        $$('comun_precio').removeView('comun_precio_'+i);
    }
    //$$('comun_precio').config.rows =[];
    if (val<15){
        for(var i=1; i<=val; i++){
            inc++;
            $$('comun_precio').addView({
                view: 'text',
                label:'Precio '+i,
                id:'comun_precio_'+i,
                name:'comun_precio_'+i,
                idr:i,
                invalidMessage:"Campo requerido.",
                validate:webix.rules.isNumber,
                on:{
                    onKeyPress:function(key){
                        if (key==13){
                            if (this.data.idr ==val)
                                agregarComun();
                        }
                    }
                }
            });
        }
        if (inc>0)
            $$('comun_precio_'+1).focus();
    }

}
function buscaArticuloPaquete(id,cantidad,_tipo_venta){
    var flag =false;
    cantidad  = parseFloat(cantidad);
    paquetes.forEach(function(item){
        if (item.id_articulo ==id &&  (cantidad>= parseFloat(item.cantidad_minima)  &&  cantidad <=parseFloat(item.cantidad_maxima) || cantidad==parseFloat(item.cantidad_exacta))  && _tipo_venta == item.tipo_venta){
            id_tipo_cantidad = item.id_tipo_cantidad;
            sub = item.precio;
            if (!obj_pref.maneja_hora){
                if (id_tipo_cantidad!=2){
                    precio = parseFloat(item.precio);
                }else{
                    descripcion_paquete=item.descripcion;
                }
                flag= true;
            }
            else{
                var hora_inicio = new Date(item.hora_inicio);
                var hora_fin = new Date(item.hora_fin);
                hora_inicio.setFullYear(0);
                hora_inicio.setMonth(0);
                hora_inicio.setDate(0);
                hora_fin.setFullYear(0);
                hora_fin.setMonth(0);
                hora_fin.setDate(0);
                hora_cliente.setFullYear(0);
                hora_cliente.setMonth(0);
                hora_cliente.setDate(0);
                if (hora_inicio <= hora_cliente && hora_fin >= hora_cliente){
                    if (id_tipo_cantidad!=2){
                        precio = parseFloat(item.precio);
                    }
                    else{
                        descripcion_paquete=item.descripcion;
                    }
                    flag=true;
                }
            }
        }
    });

    return flag;
}
function getPaquetes(){
    webix.ajax().sync().post(URL_GET_PAQUETES,{},function(response){
        response = JSON.parse(response);
        paquetes= response;
    });
}

function verificaFondo(){
    webix.ajax().post(URL_CHECK_FONDO, {}, function (response) {
        $$('nfondo').setValue(response);
        if (parseInt(response)==0){
            $$('window_fondo').show();
            $$('fondo').focus();
        }

    });
}

function saveFondo(){
    if ($$('form_corte_caja').validate()){
        webix.ajax().post(URL_SAVE_CORTE_CAJA,$$('form_corte_caja').getValues(),function(response){
            response = JSON.parse(response);
            $$('window_fondo').hide();
            webix.alert(response.message,function(){$$('id_articulo').focus();});
        });
    }
    else
        webix.alert({text:'Complete los campos requeridos.',type:'alert-error'});
}

function guardarGasto(){
    if ($$('form_gastos').validate()){
        webix.ajax().post(URL_SAVE_GASTO,$$('form_gastos').getValues(),function(response){
            response = JSON.parse(response);
            webix.alert(response.message);
            $$('window_gastos').hide();
        });
    }
    else{
        webix.alert({type:'alert-error',text:'Ingrese datos correctos.'});
    }
}
function guardarSalida(){
    if ($$('form_salida').validate()){
        webix.ajax().post(URL_SAVE_SALIDA,$$('form_salida').getValues(),function(response){
            response = JSON.parse(response);
            if (response.estatus){
                $$('window_salida').hide();
            }
            webix.alert(response.message);
        });
    }
    else{
        webix.alert({type:'alert-error',text:'Ingrese datos correctos.'});
    }
}
function gastos(){
    $$('window_gastos').show();
    $$('form_gastos').clear();
    $$('form_gastos').clearValidation();
    $$('descripcion').focus();
}
function salida(){
    $$('window_salida').show();
    $$('form_salida').clear();
    $$('form_salida').clearValidation();
    $$('salida').focus();
}
function actualizarArticulos(){
    webix.ajax().sync().post(URL_GET_ARTICULOS,{},function(response){
        response = JSON.parse(response);
        var data=[];
        response.forEach(function(item,index){
            data.push({value:item.descripcion,id:item.id});
        });
        /*
         $$('id_articulo').define('suggest',{
         id:'suggest_id_articulo',
         data:data,
         filter:function (obj,value) {
         value = value.toUpperCase().trim();
         var type =typeof (obj.value);
         if (type=='string' && obj.value.indexOf(value) == 0){
         return obj;
         }
         return obj.value.indexOf(value) == 0;
         }
         });*/
        $$('id_articulo').define('options',data)
        $$('id_articulo').refresh();
        articulos = response;
        $$('id_articulo').focus()

    });
}
function nuevaNota(){
    window.open(BASE_URL+"ventas/ventas");
}

function mostrarPrecio(tipo){
    if (art.comun==0){
        var n=0;
        precio= art.precio_unidad;
        existencia = art.existencia_unidad;
        tipo_venta=1;
        if (tipo==2){
            existencia= art.existencia;
            if (venta_unidad==1){
                //  art.descripcion=contenedor+" "+art.descripcion;
            }
            tipo_venta=2;
            precio = art.precio_venta;
            venta.forEach(function(item,index){
                if(item.id_articulo==$$('id_articulo').getValue()){
                    if (parseInt($$('cantidad').getValue())<=existencia)
                        existencia-=item.cantidad;
                    if (item.precio_mayoreo){
                        precio = art.precio_mayoreo;
                    }
                    n++;
                }
            });
        }
        else{
            venta.forEach(function(item,index){
                if(item.id_articulo==$$('id_articulo').getValue() && item.tipo_venta==2){
                    if (parseInt($$('cantidad').getValue())<=existencia)
                        existencia-=(item.cantidad* parseFloat(art.cantidad_unidad));
                    if (item.precio_mayoreo){
                        precio = art.precio_unidad_mayoreo;
                    }
                    n++;
                }
                if(item.id_articulo==$$('id_articulo').getValue() && item.tipo_venta == 1){
                    if (parseInt($$('cantidad').getValue())<=existencia)
                        existencia-=item.cantidad;
                    if (item.precio_mayoreo){
                        precio = art.precio_unidad_mayoreo;
                    }
                    n++;
                }
            });
        };
        if (!n){
            $$('precio_mayoreo').setValue(0);
        }
        $$('label_precio').define('label','$'+precio);
        $$('label_precio').refresh();
        if ($$('datatable_ventas').getSelectedId()==undefined){
            $$('label_existencia').define('label',existencia);
            $$('label_existencia').refresh();
        }
        else{
            var _cant = venta[$$('datatable_ventas').getSelectedItem().index].cantidad;
            existencia+= _cant;
            $$('label_existencia').define('label',existencia);
            $$('label_existencia').refresh();
        }
        $$('label_mayoreo').define('label',art.cantidad_mayoreo);
        $$('label_mayoreo').refresh();
        $$('window_tipo_venta').hide();
        calcularSubtotal($$('cantidad').getValue());
        $$('cantidad').define('disabled',false);
        $$('cantidad').setValue('1');
        $$('cantidad').refresh();
        $$('cantidad').focus();
        $$('cantidad').getInputNode().select();
    }
}
function calcularSubtotal(cantidad){
    sub = parseFloat(precio)*parseInt(cantidad);
    $$('label_subtotal').define('label','$'+sub.toFixed(2));
    $$('label_subtotal').refresh();
}

function actualizarExistencia(cantidad){
    if (cantidad>existencia && obj_pref.inventario_estricto){
        webix.alert({type:"alert-error",text:"Articulo insuficiente"});
        return false;
    }
    else{
        $$('label_existencia').define("label",existencia);
        $$('label_existencia').refresh();
        return true;
    }


}
function calcularTotal(){
    var tot=0;
    venta.forEach(function(item){
        tot+=parseFloat(item.subtotal);
    })
    total_g= tot;
    $$('label_total').define("label","$"+tot.toFixed(2));
    $$('label_total').refresh();
    $$('label_cantidad').define("label",venta.length);
    $$('label_cantidad').refresh();
}

function deletes(id){
    var datatable = $$('datatable_ventas');
    webix.confirm({
        text:"¿Desea eliminar el articulo de la venta?",
        ok:"Si",
        cancel:"No",
        callback:function(result){
            if (result){
                if (datatable.getItem(id).id_articulo==$$('id_articulo').getValue()){
                    /*$$('label_existencia').define("label",art.existencia);*/
                    $$('id_articulo').define('disabled',false);
                    $$('label_existencia').refresh();
                }
                var ra=0;
                var id_art= $$('datatable_ventas').getItem(id).id_articulo;
                var ui= $$('datatable_ventas').getItem(id).ui;
                for (var i=0; i<venta.length; i++){
                    if (ui==venta[i].ui){
                        ra=i;
                    }
                }
                venta.splice(ra,1);
                datatable.remove(id);
                var count=0;
                datatable.eachRow(function(row){
                    count++;
                    datatable.updateItem(row,{numero:count});
                });
                $$('id_articulo').focus();
            }
        }
    });
}

function add(edit){
    edit  = edit  || undefined
    var flag = true;
    var found_detail = 0;
    var arti = art;
    if ($$('form_venta').validate()){
        venta.forEach(function(item,index){
            if (art.id_articulo == item.id_articulo && item.tipo_venta == tipo_venta && item.id_tipo_cantidad != 2 && !buscaArticuloPaquete(arti.id_articulo,cantidad,tipo_venta) && edit == undefined){
                flag= false;
                found_detail = index;
            }
        });
        //Valores por default cuando no se edita
        var cantidad = parseFloat($$('cantidad').getValue());
        var index = venta.length;
        var prec = parseFloat(precio).toFixed(2)
        var id_articulo = art.id_articulo;
        // si ya existe
        if (!flag){
            cantidad = parseFloat(venta[found_detail].cantidad)+parseFloat($$('cantidad').getValue());
            index = found_detail;
        }
        //Si se va a editar
        else if (edit != undefined) {
            arti = obtenerArticulo(edit.id_articulo);
            cantidad = edit.cantidad;
            index = edit.index;
            flag = false;
            prec = (edit.tipo_venta==1)?arti.precio_unidad:arti.precio_venta;
            if (edit.comun){
                prec = edit.precio
            }
            id_articulo = edit.id_articulo;

        }
        //Verifica si se cuenta con la cantidad solicitada siempre y cuando este el modo estricto
        if (actualizarExistencia(cantidad)){
            //preparamos el elemento de la venta
            var unique_id = uniqueId();
            var detail = {
                ui:unique_id,
                venta_unidad:venta_unidad,
                precio_mayoreo:$$('precio_mayoreo').getValue(),
                tipo_venta:tipo_venta,
                id_articulo:id_articulo,
                descripcion:art.descripcion,
                cantidad:cantidad,
                precio:prec,
                subtotal:(prec*cantidad).toFixed(2),
                comun: arti.comun,
                index:index,
                number:venta.length+1,
                flag:flag
            };
            //Verifica si el detalle aun no esta
            var precio_paquete = buscaArticuloPaquete(id_articulo,cantidad,tipo_venta)
            //console.log(precio_paquete)
            if (precio_paquete){
                webix.message(text_paquete);
                if (id_tipo_cantidad==2){
                    detail.subtotal=sub;
                    //detail.precio=sub/detail.cantidad;
                    detail.cantidad=1;
                    detail.descripcion= descripcion_paquete;
                    detail.subtotal=sub;
                    detail.precio=sub;
                }
                addDetail(detail)
            }  else if (aplicarPrecioMayoreo(arti,cantidad)){
                confirmaMayoreo(function () {
                    detail.precio = precio
                    detail.subtotal=(precio*cantidad).toFixed(2)
                    detail.precio_mayoreo= aplica_mayoreo
                    addDetail(detail)
                })
                $$('precio_mayoreo').setValue(0);
            }
            else {
                addDetail(detail)
            }
            id_tipo_cantidad=0;
            calcularTotal();
            cleanArticulo();
        }
    }


    /* datatable.eachRow(function(row){
     if (art.id_articulo==this.getItem(row).id_articulo && this.getItem(row).tipo_venta==tipo_venta && this.getItem(row).id_tipo_cantidad!=2 && !buscaArticuloPaquete(art.id_articulo,cantidad,tipo_venta)){
     flag= false;
     r= row;
     }
     });*/
}
function cleanArticulo() {
    $$('precio_mayoreo').disable();
    $$('btn_agregar').disable();
    $$('id_articulo').focus();
    $$('id_articulo').setValue("  ");
}
function addDetail(detail){
    //console.log(detail)
    if (detail.flag){
        venta.push(detail)
    }
    else {
        venta[detail.index].cantidad = detail.cantidad;
        venta[detail.index].precio = detail.precio;
        venta[detail.index].subtotal = detail.subtotal;
    }
    $$('datatable_ventas').define('data',venta);
    $$('datatable_ventas').refresh()
    calcularTotal();
}
function obtenerArticulo(id){
    var result;
    articulos.forEach(function(item){
        if (item.id==id)
            result = item;
    });
    return result;
}

function obtenerArticuloCodigo(codigo){
    var result;
    articulos.forEach(function(item){
        if (item.codigo_barras==codigo){
            result = item.id;
        }
    });
    return result;
}
function mostrarArticulo(nv,ov){
    var datatable = $$('datatable_ventas');
    if (nv!='' && nv!=0){
        var flag=false;
        var tp=0;
        $$('cantidad').define("disabled",false);
        $$("cantidad").setValue(1);
        $$('cantidad').focus();
        var response = obtenerArticulo(nv);
        art.precio_unidad_mayoreo = parseFloat(response['precio_unidad_mayoreo']);
        art.venta_unidad =response['venta_unidad'];
        art.cantidad_unidad =parseFloat(response['cantidad_unidad']);
        art.existencia =parseFloat(response['existencia']);
        art.precio_mayoreo =parseFloat(response['precio_mayoreo']);
        art.descripcion =response['descripcion'];
        art.id_articulo =response['id'];
        art.precio_venta =parseFloat(response['precio_venta']);
        art.precio_unidad = parseFloat(response['precio_unidad']);
        art.id_tipo_venta =response['id_tipo_venta'];
        art.comun = parseInt(response['comun']);
        //debugger
        art.cantidad_mayoreo =parseFloat(response['cantidad_mayoreo']);
        art.cantidad_unidad_mayoreo =parseFloat(response['cantidad_unidad_mayoreo']);
        venta_unidad=response['venta_unidad'];
        //console.log(art)
        if(art.comun==0){
            // art.comun = false;
            $$('precio_mayoreo').define("disabled",false);
            venta.forEach(function(item,index){
                if(item.id_articulo==$$('id_articulo').getValue()){
                    if (item.precio_mayoreo){
                        $$('precio_mayoreo').define("disabled",true);
                        $$('precio_mayoreo').setValue(1);
                    }
                    if (item.venta_unidad){
                        flag=true;
                        tp=item.tipo_venta;
                    }

                }
            });
            $$('label_existencia').refresh();
            if (response['venta_unidad']==1){
                // if (!flag){
                $$('id_articulo').blur();
                contenedor=response['contenedor'];
                art.existencia_unidad = response['existencia_unidad'];
                $$('label_tipo_venta_1').define("label",""+response['tipo_venta']+" [1]");
                $$('label_tipo_venta_2').define("label",""+response['contenedor']+" [2]");
                $$('label_tipo_venta_1').refresh();
                $$('label_tipo_venta_2').refresh();
                $$('window_tipo_venta').show();
                $$('label_subtotal').setValue("0.00");
                $$('cantidad').define('disabled',true);
                //  }
                // else
                //  mostrarPrecio(tp);
            }
            else{
                //debugger;
                mostrarPrecio(2);
                if (use_cod==1 && art.comun==0)
                    add();
            }
        }
        else{
            $$('form_comun').clear();
            $$('form_comun').clearValidation();
            $$('window_comun').show();
            $$('comun_descripcion').define('disabled',true);
            $$('comun_descripcion').setValue(art.descripcion);
            $$('comun_cantidad').setValue('1');
            $$('comun_cantidad').focus();
        }
        //})
        $$('btn_agregar').enable();


    }
    else{
        $$('cantidad').setValue(0);
        $$('label_existencia').define('label','0.00');
        $$('label_existencia').refresh();
        $$('label_mayoreo').define('label','0.00');
        $$('label_mayoreo').refresh();
        $$('label_precio').define('label','0.00');
        $$('label_precio').refresh();
        $$('cantidad').define("disabled",true);
        $$('precio_mayoreo').define("disabled",true);

    }

}
function precioMayoreo(nv,interno){
    interno = (interno) ? interno : false;
    if (nv) {
        if (tipo_venta == 1 && venta_unidad == 1)
            precio = art.precio_unidad_mayoreo;
        else if (tipo_venta == 2)
            precio = art.precio_mayoreo;
        if (!interno)
            $$('precio_mayoreo').setValue(1);
    }
    else {
        if (tipo_venta == 1 && venta_unidad == 1)
            precio = art.precio_unidad;
        else if (tipo_venta == 2)
            precio = art.precio_venta;
        if (!interno)
            $$('precio_mayoreo').setValue(0);
    }
    if (!interno) {
        $$('label_precio').define("label", "$" + precio);
        $$('label_precio').refresh();
        calcularSubtotal($$('cantidad').getValue());
    }

    aplica_mayoreo = nv;
}
function cobrarVenta(){
    total_g_descuento = total_g;
    if (obj_pref.descuentos.length>0)
        $$('id_descuento').setValue(obj_pref.descuentos[0].id);
    $$('id_articulo').setValue("0");
    $$('id_articulo').blur();
    if ($$('datatable_ventas').count()>0){
        $$('tipo_pago').setValue(1);
        $$('pago').setValue('');
        setInterval(function(){
            var cambio=0;
            if (parseFloat($$('pago').getValue())>=total_g_descuento)
                cambio =parseFloat($$('pago').getValue())- total_g_descuento;
            $$('label_realizar_venta_cambio').define("label",cambio.toFixed(2));
            $$('label_realizar_venta_cambio').refresh();
        },100);
        $$('label_realizar_venta_total').define("label",parseFloat(total_g_descuento).toFixed(2));
        $$('label_realizar_venta_total').refresh();
        $$('window_realizar_venta').show();
        $$('pago').focus();
    }
}
function realizarVenta(){
    $$('window_realizar_venta').hide();
    venta.forEach(function (item) {
        delete  item['index']
        delete  item['number']
        delete  item['flag']
        delete  item['numero']
    })
    webix.ajax().post(URL_SAVE_VENTA,
        {
            venta_detalles:venta,
            venta:{
                tipo:1,
                tipo_pago:$$('tipo_pago').getValue(),
                total:total_g_descuento,
                descuento:total_g  - total_g_descuento,
                inventario_estricto:obj_pref.inventario_estricto,
                pago:$$('pago').getValue(),
                cambio:parseFloat($$('pago').getValue())-total_g_descuento,
                id_descuento:$$('id_descuento').getValue()
            }
        },
        function(response){
            response = JSON.parse(response);
            // var w =window.open(response.url,"_blank","width=400 height=900");
            limpiarVenta();
            actualizarArticulos();
            $$('window_realizar_venta').hide();
            //notificacion();
            if (obj_pref.maneja_ticket){
                /*webix.confirm({
                 text:"Venta realizada, ¿Desea imprimir ticket?",
                 ok:"Si",
                 cancel:"No",
                 callback:function(r){
                 if (r){
                 var w =window.open(response.url,"_blank","width=700 height=900");
                 }
                 limpiarVenta();
                 $$('id_articulo').focus();
                 }
                 });*/
                imprimirTicket(response);
            }
            else{
                webix.alert("Venta realizada");
            }
        });
}
function imprimirTicket(response) {
    var config = qz.configs.create(response.datos_ticket.impresora);
    var data = [
        '\x1B' + '\x40',          // init
        '\x1B' + '\x61' + '\x31', // center align
        '\x1B' + '\x21' + '\x30', // em mode on
        response.datos_ticket.nombre_negocio , ' \n \x0A',
        '\x1B' + '\x21' + '\x0A' + '\x1B' + '\x45' + '\x0A', // em mode off
        response.datos_ticket.datos_fiscales + '\n'+
        '\x1B' + '\x61' + '\x30', // left align
        'FOLIO: ' + response.venta.folio + '        ' + response.venta.fecha + '\n',
        'ATENDIO: ' + response.venta.nombre + '\x0A.',
        '\x0A',
        '#  ' + 'DESCRIPCION              ' + 'CANT   ' + 'PREC   ' + 'SUB   \n'
    ];
    function pad(input, length, padding) {
        var str = input + "";
        return (str.length > length) ? str.substr(0,34) + ' '  : pad((arguments[3] != undefined)?str:str+padding, length, padding);
    }
    response.detalles.forEach(function (item, index) {
        item.descripcion = pad(item.descripcion,34,' ')
        item.cantidad = pad(item.cantidad,7,' ')
        item.precio = pad(item.precio,7,' ')
        item.subtotal = pad(item.subtotal,6,' ',true)
        data.push( '\x1B' + '\x4D' + '\x31')
        data.push(index+1,'  ' + item.descripcion + item.cantidad  + item.precio + item.subtotal + '\n')
        data.push( '\x1B' + '\x4D' + '\x30')
    })
    if(response.venta.descuento != '0.00') {
        data.push(
            '\x0A',
            '\x1B' + '\x61' + '\x32',
            '\x1B' + '\x45' + '\x0D', // bold on
            'TOTAL: ' + (parseFloat(response.venta.descuento) + parseFloat(response.venta.total)).toFixed(2),
            '\n',
            'DESCUENTO('+response.venta.tipo_descuento+'): ' +response.venta.descuento,
            '\n'
        )
    }
    data.push(
        '\x1B' + '\x61' + '\x30', // left align
        '\x0A',
        'PAGO: ' + response.venta.tipo_pago
    )
    data.push(
        '\x0A',
        '\x1B' + '\x61' + '\x32',
        '\x1B' + '\x45' + '\x0D', // bold on
        'TOTAL: ' + response.venta.total,
        '\n',
        'PAGO: ' +response.venta.pago,
        '\n ',
        'CAMBIO: ' + response.venta.cambio,
        '\x0A',
        '\x0A',
        '\x1B' + '\x61' + '\x31', //center align
        response.datos_ticket.mensaje_gracias + ' \x0A',
        '\x1B' + '\x45' + '\x0A', // bold of
        response.datos_ticket.advertencia
        //
    )
    data.push( '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A' , '\x0A',
        '\x1B', '\x69');
    //var data = [response]
    qz.print(config,data).catch(function (e) {
        console.error(e)
    })
}
function cancelarVenta(){
    webix.confirm({
        text:"¿Que desea cancelar?",
        ok:"Venta actual",
        cancel:"Venta anterior",
        callback:function(result){
            if (result){
                if($$('datatable_ventas').count()>0)
                    limpiarVenta();
            }
            else {
                $$('folio_cancelacion').setValue('');
                $$('label_ticket').define('label','');
                $$('cancelar_venta').hide();
                $$('window_cancelacion').show();
                $$('folio_cancelacion').focus();

            }
        }
    });
}

function limpiarVenta(){
    venta=[];
    $$('datatable_ventas').clearAll();
    calcularTotal();
}

function mostrarCopia(){
    $$('cantidad_copia').setValue('');
    $$('pago_copia').setValue('');
    $$('aplicar_iva').setValue(0);
    $$('window_copia').show();
    // $$('id_articulo').blur();
    $$('cantidad_copia').focus();
    $$('label_precio_copia').define('label','');
    $$('label_precio_copia').refresh();
    $$('label_total_copia').define('label','');
    $$('label_total_copia').refresh();
    detectaPrecioCopia();
}
function detectaPrecioCopia(){
    webix.ajax().post(URL_GET_PRECIO_COPIA,{cantidad:$$('cantidad_copia').getValue()},function(response){
        response= JSON.parse(response);
        precio_copia=parseFloat(response[0]['precio']);
        total_copia = precio_copia*parseInt($$('cantidad_copia').getValue());
        $$('precio_copia').setValue(precio_copia);
        $$('label_precio_copia').define("label","$"+precio_copia);
        $$('label_precio_copia').refresh();
        $$('label_total_copia').define("label","$"+precio_copia*parseInt($$('cantidad_copia').getValue()));
        $$('label_total_copia').refresh();
    });
}

function calcularCambioCopia(){
    setInterval(function(){
        if ($$('pago_copia').getValue()!="" && $$('pago_copia').getValue()!=0 && parseFloat($$('pago_copia').getValue())>total_copia){
            $$('label_cambio_copia').define("label","$"+(parseFloat($$('pago_copia').getValue())-total_copia).toFixed(2));
            $$('label_cambio_copia').refresh();
        }
        else{
            $$('label_cambio_copia').define("label","$ 0.00");
            $$('label_cambio_copia').refresh();
        }

    },100);
}
function cobrarCopia(){
    if ($$('pago_copia').getValue()==""){
        $$('pago_copia').setValue(total_copia)
    }
    var cantidad_copia = parseInt($$('cantidad_copia').getValue());
    var precio_copia =parseFloat($$('precio_copia').getValue());
    var pago_copia =parseFloat($$('pago_copia').getValue());

    if (pago_copia>=total_copia){
        webix.ajax().post(URL_SAVE_VENTA,
            {venta_detalles:[{descripcion:"COPIA",precio:precio_copia,cantidad:cantidad_copia,subtotal:total_copia,id_articulo:0,venta_unidad:0,precio_mayoreo:0,tipo_venta:1}],
                venta:{tipo:3,total:total_copia,pago:pago_copia,cambio:pago_copia-total_copia,aplico_iva:$$('aplicar_iva').getValue()}},
            function(response){
                $$('window_copia').hide();
                response = JSON.parse(response);
                if (obj_pref.maneja_ticket){
                    webix.confirm({
                        text:"Venta realizada, ¿Desea imprimir ticket?",
                        ok:"Si",
                        cancel:"No",
                        callback:function(r){
                            if (r){
                                var w =window.open(response.url,"_blank","width=700 height=900");
                                setTimeout(function(){
                                    w.close();
                                },400);
                            }
                            limpiarVenta();
                        }
                    });
                }
                else
                    webix.alert("Venta realizada");
            });
    }
    else
        webix.alert({type: "alert-error", text: "El pago no puede ser menor que el total"});
}

/*function actualizarCantidad(item){
 venta[item.index].cantidad = item.cantidad;
 tipo_venta= item.tipo_venta;
 if (aplicarPrecioMayoreo(articulo,venta[item.index].cantidad)){
 confirmaMayoreo(function () {
 venta[item.index].precio = precio;
 venta[item.index].subtotal =(venta[item.index].cantidad*venta[item.index].precio).toFixed(2);
 $$('datatable_ventas').eachRow(function(id){
 var element = this.getItem(id);
 if (element.index == item.index && venta[item.index].tipo_venta == element.tipo_venta){
 $$('datatable_ventas').updateItem(id,venta[element.index]);
 }
 });
 calcularTotal();
 });
 }
 else {
 var precio_paquete = buscaArticuloPaquete(venta[item.index].id_articulo,venta[item.index].cantidad,venta[item.index].tipo_venta);
 if (precio_paquete){
 webix.message(text_paquete);
 }
 venta[item.index].precio =  aplicarPrecio(item,articulo);
 venta[item.index].subtotal =(venta[item.index].cantidad*venta[item.index].precio).toFixed(2);
 $$('datatable_ventas').eachRow(function(id){
 var articulo = this.getItem(id);
 if (articulo.index == item.index && venta[item.index].tipo_venta == articulo.tipo_venta){
 // console.log(venta[item.index])
 $$('datatable_ventas').updateItem(id,venta[item.index]);
 }

 });
 calcularTotal();
 }
 }*/
function editarArticulo(item){
    $$('id_articulo').setValue(venta[item.index].id_articulo);
    $$('id_articulo').define("disabled",true);
    $$('precio_mayoreo').setValue(venta[item.index].precio_mayoreo);
    $$('cantidad').setValue(venta[item.index].cantidad);
}
function detectaPrecioMayoreo(){
    if ($$('id_articulo').getValue()!="") {
        if ($$('precio_mayoreo').isEnabled()) {
            //if( $$('precio_mayoreo').getValue() =="0"){
            //  $$('precio_mayoreo').setValue(0);
            webix.confirm({
                text: "¿Desea aplicar precio de mayoreo?",
                ok: "Si",
                cancel: "No",
                callback: function (result) {
                    if (result) {
                        precioMayoreo(1);
                        $$('precio_mayoreo').setValue(1);
                        if (tipo_venta == 1 && venta_unidad == 1)
                            precio = art.precio_unidad_mayoreo;
                        else if (tipo_venta == 2 && venta_unidad == 0 || venta_unidad == 1)
                            precio = art.precio_mayoreo;
                        calcularSubtotal($$('cantidad').getValue())
                    }
                    else {
                        calcularSubtotal($$('cantidad').getValue());
                        $$('precio_mayoreo').setValue(0);
                        precioMayoreo(0);
                    }
                }
            });

            // }
        }
    }
}
