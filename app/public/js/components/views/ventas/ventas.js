URL_GET_ARTICULOS =BASE_URL+"ventas/ventas/getArticulos";
URL_GET_ARTICULO =BASE_URL+"ventas/ventas/getArticulo";
URL_GET_PAQUETES =BASE_URL+"ventas/ventas/getPaquetes";
URL_SAVE_VENTA =BASE_URL+"ventas/ventas/save";
URL_SAVE_GASTO =BASE_URL+"ventas/ventas/saveGasto";
URL_SAVE_SALIDA =BASE_URL+"ventas/ventas/saveSalida";
URL_GET_PRECIO_COPIA=BASE_URL+"ventas/ventas/getPrecioCopia";
URL_CHECK_FONDO=BASE_URL+"ventas/ventas/checkFondo";
URL_SAVE_CORTE_CAJA = BASE_URL +'ventas/ventas/saveFondo/';
URL_ARTICULOS_COMUN= BASE_URL +'ventas/ventas/getArticulosComun/';
URL_BUSCAR_VENTA= BASE_URL +'ventas/ventas/buscarVenta/';
URL_CANCELAR_VENTA= BASE_URL +'ventas/ventas/cancelarVenta/';
var text_paquete = "Precio de paquete aplicado.";
var art ={};
var venta =[];
var cont=0;
var precio =0;
var tipo_venta=0;
var existencia=0;
var contenedor="";
var venta_unidad=0;
var total_g=0;
var total_g_descuento=0;
var total_copia=0;
var precio_copia=0;
var flag_copia=0;
var articulos =[];
var articulosCombo=[];
var paquetes=[];
var use_cod=false;
var cod="";
var art_comun=false;
var id_venta_cancelar=0;
var id_tipo_cantidad=0;
precios_copias=[];
precio_mayoreo=0;
var aplica_mayoreo=false;
toolbar_header.cols[0].label="Realizar venta";
var modulo =0;
var descripcion_paquete="";
var form_cancelacion =   {
    view:'form',
    id:'form_cancelacion',
    elements:[
        {
            rows:[
                {
                    cols:[
                        {view:'text',id:'folio_cancelacion',name:'folio_cancelacion',label:'Folio'},
                        {
                            rows:[
                                {},
                                {view:'button',label:'Buscar',type:'imageButton',image:BASE_URL_ICONS+'search.png',width:100,click:"buscar_venta()"},
                            ]
                        }
                    ]
                },
                {
                    height:40,
                    cols:[
                        {id:'label_ticket',view:'label',template:'<a href="#">#label#</a>',borderless:true},
                        {id:'cancelar_venta',view:'button',label:'Cancelar',type:'imageButton',image:BASE_URL_ICONS+'cancelar_venta.png',width:100,click:"cancelacionVenta()"},
                    ]
                }
            ]
        }
    ],
    elementsConfig:{
        labelPosition:'top'
    },
    on:{
        //onSubmit:buscar_venta
    },
    rules:{
        'folio_cancelacion':webix.rules.isNotEmpty
    }
};
var window_cancelacion={
    view:'window',
    id:'window_cancelacion',
    modal:true,
    position:'center',
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Cancelar venta",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:form_cancelacion
};
var window_fondo ={
    view:'window',
    id:'window_fondo',
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Fondo de caja",align:"center"}
        ]
    },
    position:'center',
    modal:true,
    body:{
        view:'form',
        id:'form_corte_caja',
        elements:[
            {
                rows:[
                    {view:'text',label:'Fondo',id:'fondo',name:'fondo',invalidMessage:valid_number},
                    {cols:[
                        {},
                        {view:'button',type:'imageButton',image:BASE_URL_ICONS+'success.png',label:'Aceptar',width:100,click:'saveFondo()'}
                    ]}
                ]
            }
        ],
        elementsConfig:{
            labelPosition:'top'
        },
        rules:{
            'fondo':webix.rules.isNumber,
        },
        on:{
            onSubmit:'saveFondo()'
        }

    }
}
var window_salida={
    view:'window',
    id:'window_salida',
    modal:true,
    position:'center',
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Salida de caja",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:{
        rows:[
            {
                view:'form',
                id:'form_salida',
                elements:[
                    {view:'text',label:'Cantidad',id:'salida',name:'salida'},
                    {cols:[
                        {},
                        {view:'button',type:'imageButton',image:BASE_URL_ICONS+'success.png',label:'Aceptar',width:100,invalidMessage:"Numero incorrecto",click:'guardarSalida()'}
                    ]}
                ],
                elementsConfig:{
                    labelPosition:'top'
                },
                rules:{
                    'salida':webix.rules.isNumber
                },
                on:{
                    onSubmit:guardarSalida
                }
            }
        ]
    }
}

var window_gastos={
    view:'window',
    id:'window_gastos',
    modal:true,
    position:'center',
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Gastos",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:{
        rows:[
            {
                view:'form',
                id:'form_gastos',
                elements:[
                    {view:'text',label:'Descripción',id:'descripcion',name:'descripcion',invalidMessage:"Campo requerido."},
                    {view:'text',label:'Cantidad',id:'gasto',name:'gasto'},
                    {cols:[
                        {},
                        {view:'button',type:'imageButton',image:BASE_URL_ICONS+'success.png',label:'Aceptar',width:100,invalidMessage:"Numero incorrecto",click:'guardarGasto()'}
                    ]}
                ],
                elementsConfig:{
                    labelPosition:'top'
                },
                rules:{
                    'descripcion':webix.rules.isNotEmpty,
                    'gasto':webix.rules.isNumber
                },
                on:{
                    onSubmit:guardarGasto
                }
            }
        ]
    }
}


var window_comun={
    view:'window',
    id:'window_comun',
    modal:true,
    position:'center',
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Articulo comun/servicio",id:"label_toolbar_header",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    body:{
        rows:[
            {
                view:'form',
                id:'form_comun',
                elements:[
                    {view:'text',label:'Descripción',id:'comun_descripcion',name:'descripcion',invalidMessage:"Campo requerido."},
                    {view:'text',label:'Cantidad',id:'comun_cantidad',name:'comun_cantidad',on:{
                        onchange:cantidadComun
                    }},
                    {
                        id:'comun_precio',
                        rows:[
                        ]
                    },
                    {},
                    {cols:[
                        {},
                        {view:'button',type:'imageButton',image:BASE_URL_ICONS+'success.png',label:'Aceptar',width:100,invalidMessage:"Numero incorrecto",click:'agregarComun()'}
                    ]}

                ],
                elementsConfig:{
                    labelPosition:'top'
                },
                rules:{
                    'comun_cantidad':webix.rules.isNumber
                },
            }
        ]
    }
}
var toolbar_ventas={
    height:33,
    cols:[
        {view:"button",type:"imageButton",image:BASE_URL_ICONS+"export_document.png",id:'boton_copias',label:"Copias [Ctrl+F2]",width:150,click:"mostrarCopia()"},
        {width:10},
        {view:"button",type:"imageButton",image:BASE_URL_ICONS+"gastos.png",id:'boton_gastos',label:"Gastos [Ctrl+F3]",width:140,click:"gastos()"},
        {view:"button",type:"imageButton",image:BASE_URL_ICONS+"gastos.png",id:'boton_salida',label:"Salida",width:140,click:"salida()"},
        {view:"button",type:"imageButton",image:BASE_URL_ICONS+"agregar_venta.png",id:'boton_nueva_nota',label:"Nueva nota ",width:170,click:"nuevaNota()"},
        //{view:"button",type:"imageButton",image:BASE_URL_ICONS+"gastos.png",id:'boton_entradas',label:"Gastos [Ctrl+F6]",width:140,click:"gastos()"},
    ]
}
var window_copia={
    view:"window",
    id:"window_copia",
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Copias",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    modal:true,
    position:"center",
    body:{
        rows:[
            {view:"form",
                elements:[
                    {rows:[
                        {cols:[
                            {view:"text",id:"cantidad_copia",name:"cantidad_copia",label:"Cantidad",value:'',labelPosition:"top",width:130,on:{
                                onChange:detectaPrecioCopia,
                                onKeyPress:function(key){
                                    if (key>=48 && key<=57 || key ==8 || key==27)
                                        event.returnValue=true;
                                    else if (key==13){
                                        calcularCambioCopia();
                                        $$('pago_copia').focus();
                                    }
                                    else
                                        event.returnValue=false;
                                }
                            }},
                            {view:"label",label:"Precio: "},
                            {view:"label",id:"label_precio_copia",css:"precio"},
                            {view:"text",id:"precio_copia",hidden:true}
                        ]},
                        {
                            cols:[
                                {
                                    view:'checkbox',
                                    id:'aplicar_iva',
                                    label:'Aplicar IVA',
                                    labelWidth:100,
                                    value:0,
                                    on:{
                                        onChange:aplicarIvaCopia
                                    }
                                },
                                {}
                            ]
                        },
                        {
                            cols:[
                                {view:"label",label:"Total: "},
                                {view:"label",id:"label_total_copia",css:"precio"},
                            ]
                        },
                        {
                            cols:[
                                {view:"text",label:"Pago",id:"pago_copia",name:"pago_copia",labelPosition:"top",on:{
                                    onKeyPress:function(key){
                                        if (key>=48 && key<=57 || key ==8 || key==27 || key == 190)
                                            event.returnValue=true;
                                        else if (key==13)
                                            cobrarCopia();
                                        else
                                            event.returnValue=false;
                                    }
                                }}
                            ]
                        },
                        {
                            cols:[
                                {view:"label",label:"Cambio: "},
                                {view:"label",id:"label_cambio_copia",css:"precio",label:"0.00"},
                            ]
                        },
                        {
                            cols:[
                                {},
                                {view:"button",label:"Aceptar",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100,click:"cobrarCopia()"}
                            ]
                        },

                    ]}
                ]
            }
        ]
    }
}


var window_realizar_venta={
    view:"window",
    id:"window_realizar_venta",
    width:520,
    animate:"flip",
    head:{
        view:"toolbar",
        cols:[
            {view:"label",label:"Cobrar venta",align:"center"},
            {
                view:"button",
                type:"image",
                image:BASE_URL_ICONS+"/cancel24.png",
                width:40,
                borderless:true,
                click:function(){
                    this.getTopParentView().hide();
                }
            }
        ]
    },
    modal:false,
    position:"center",
    body:{
        view:"form",
        id:"form_realizar_venta",
        elements:[
            {rows:[
                {
                    cols:[
                        {width:20},
                        {
                            view:"radio",label:"Tipo de pago",width:400,name:"tipo_pago",id:"tipo_pago",labelPosition:"top",options:[],
                            on:{
                                onChange:function(nv,vv){
                                    if (nv==1){
                                        $$('pago').define("disabled",false);
                                        $$('pago').focus();
                                        $$('pago').setValue('');
                                    }
                                    else{
                                        $$('pago').setValue(total_g_descuento);
                                        $$('pago').define("disabled",true);
                                    }
                                }
                            }
                        },
                        {}
                    ]
                },
                {
                    cols:[
                        {width:20},
                        {
                            view:"radio",label:"Descuento",name:"id_descuento",id:"id_descuento",labelPosition:"top",options:[],
                            on:{
                                onChange:function(nv,vv){
                                    var options=  this.data.options;
                                    var desc = 0;
                                    options.forEach(function (item) {
                                        if (nv == item.id)
                                            desc = parseFloat(item.descuento)/100;
                                    });
                                    if (desc!=0){
                                        total_g_descuento= (total_g*(1- desc)).toFixed(2);
                                    }
                                    else{
                                        total_g_descuento= total_g.toFixed(2);
                                    }
                                    //  total_g_descuento = (total_g_descuento).toFixed(2);
                                    $$('label_realizar_venta_total').define('label',total_g_descuento);
                                    $$('label_realizar_venta_total').refresh();
                                    $$('pago').focus();
                                }
                            }
                        },
                        {width:20}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Total: ",css:"total",width:80},
                        {view:"label",label:"",id:"label_realizar_venta_total",css:"total",width:110},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"text",label:"Pago con:",id:'pago',name:'pago',labelPosition:"top",width:150,on:{
                            onKeyPress:function(key){
                                if (key>=48 && key<=57 || key ==8 || key==27)
                                    event.returnValue=true;
                                else if (key==13)
                                    event.returnValue=true;
                                else if (key==190)
                                    event.returnValue=true;
                                else if (key==110)
                                    event.returnValue=true;
                                else
                                    event.returnValue=false;
                            }
                        }},
                        {}
                    ]
                },
                {
                    cols:[
                        {},
                        {view:"label",label:"Cambio: ",css:"total",width:120},
                        {view:"label",label:"0.00",id:"label_realizar_venta_cambio",css:"total",width:100},
                        {}
                    ]
                },
                {cols:[
                    {},
                    {view:"button",label:"Aceptar",type:"imageButton",image:BASE_URL_ICONS+"success.png",width:100,id:'btn_realizar',click:"$$('form_realizar_venta').callEvent('onSubmit')"}
                ]}
            ]}
        ],
        rules:{
            'pago': webix.rules.isNumber
        }
        ,
        on: {
            onSubmit: function () {
                if ($$('pago').getValue()==""){
                    $$('pago').setValue(parseFloat(total_g_descuento).toFixed(2));
                }
                else{
                    if ($$('form_realizar_venta').validate()) {
                        if (parseFloat($$('pago').getValue()) >= total_g_descuento && $$('pago').getValue()!="" && !isNaN($$('pago').getValue()))
                            realizarVenta();
                        else {
                            webix.alert({type: "alert-error", text: "El pago no puede ser menor que el total"});
                        }
                    }
                    else {
                        webix.alert({type: "alert-error", text: "Ingrese numeros validos"});
                    }

                }


            }
        }
    }
}
var window_tipo_venta={
    view:"window",
    id:"window_tipo_venta",
    head:"Seleccioné tipo de venta",
    modal:true,
    position:"center",
    body:{
        rows:[
            {
                cols:[
                    {width:1},
                    {view:"label",label:"",id:"label_tipo_venta_1",css:"tipo_venta",width:150,click:function(){mostrarPrecio(1);   if (use_cod)add();}},
                    {view:"label",label:"",id:"label_tipo_venta_2",css:"tipo_venta",width:150,click:function(){mostrarPrecio(2);  if (use_cod) add();}},
                    {width:1}
                ]
            }
        ]
    }
};
var notify_paquete = {
    text: 'Precio de paquete aplicado',
    type: 'success',
    opacity: .9
};
var form_ventas ={
    view:"form",
    id:"form_venta",
    elements:[
        {
            cols:[
                {
                    rows:[
                        {view:"combo",id:"id_articulo",name:"id_articulo",width:300,label:"Articulo",width:450,
                            on:{
                                onChange:mostrarArticulo,
                                onKeyPress:function(val,e){
                                    if (val==13 ){
                                        if (webix.rules.isNumber($$('id_articulo').getText())==true){
                                            use_cod=true;
                                            var id_articulo = obtenerArticuloCodigo($$('id_articulo').getText().trim());
                                            $$('id_articulo').setValue(id_articulo);
                                            if (id_articulo != undefined) {
                                                if (art.venta_unidad==0 && use_cod==1 && obj_pref.codigo_barras_automatico==1){
                                                    add();
                                                }
                                                else if (use_cod==1 && art.venta_unidad==1 && obj_pref.codigo_barras_automatico==1){
                                                    mostrarPrecio(2);
                                                    add();
                                                }
                                            }
                                            else {
                                                webix.alert('Articulo no encontrado.',function () {
                                                    $$('id_articulo').focus()
                                                })
                                            }
                                        }
                                        else{
                                            use_cod=false;
                                            if (art.comun==0){
                                                $$('cantidad').focus();
                                            }
                                            else{
                                                $$('comun_cantidad').focus();
                                            }
                                        }
                                        $$('id_articulo').getPopup().hide();
                                    }
                                    else if (val==27){
                                        this.setValue("");
                                    }
                                    use_cod=false;

                                }
                            }
                        },
                        {
                            cols:[
                                {view:"label",label:"Existencia"},
                                {view:"label",label:"0",css:"existencia",id:"label_existencia"},
                                {view:"label",label:"Min mayoreo"},
                                {view:"label",label:"0",css:"existencia",id:"label_mayoreo"},
                            ]
                        }

                    ]
                },
                {width:10},
                {rows:[
                    {view:"counter",value:0,id:"cantidad",name:"cantidad",label:"Cantidad",disabled:true,width:115,on:{
                        onKeyPress:function(val){
                            var cant = parseFloat(this.getValue());
                            /*direccionales para aumentar las cantidades por las flechas*/
                            if (val == 38) {
                                if (obj_pref.inventario_estricto){
                                    if (this.getValue()+1 <= art.existencia){
                                        this.setValue(this.getValue()+1);
                                    }
                                }
                                else {
                                    this.setValue(this.getValue()+1);
                                }
                            }
                            else if (val == 40) {
                                if (this.getValue()>1){
                                    this.setValue(this.getValue()-1);
                                }
                            }
                            else if (val>=48 && val<=57 ){
                                event.returnValue = true;
                            }
                            else if (val==8){
                                event.returnValue = true;
                            }
                            else if (val==13 && art.comun==0){
                                add();
                            }
                            else if (tipo_venta==1 && venta_unidad==1){
                                event.returnValue=true;
                            }
                            else if (val==110){
                                event.returnValue=true;
                            }
                            else if (val>=37 && val <=40){
                                event.returnValue=true;
                            }
                            else{
                                event.returnValue = false;
                            }
                            //calcularSubtotal(this.getValue());
                        },
                        onChange:function(){
                            calcularSubtotal(this.getValue());
                        }
                    }},
                    {height:50}
                ]},
                {width:150,
                    rows:[
                        {view:"label",label:"Precio"},
                        {view:"label",label:"0.00",css:"precio",id:"label_precio"},
                        // {view:"text",id:"precio",hidden:true},
                        {view:"toggle",offIcon:"circle",
                            onIcon:"check-circle",type:"iconButton",id:"precio_mayoreo",label:"Mayoreo [Shift]",labelPosition:"left",labelWidth:120,width:150,on:{
                            onItemClick:detectaPrecioMayoreo,
                            onChange:precioMayoreo
                        }}
                    ]
                },
                {width:100,
                    rows:[
                        {view:"label",label:"Subtotal"},
                        {view:"label",label:"0.00",css:"precio",id:"label_subtotal"},
                    ]
                },
                {
                    rows:[
                        {},
                        {
                            view:"button",
                            label:"Agregar",
                            type:"imageButton",
                            image:BASE_URL_ICONS+"new.png",
                            width:130,click:"add()",
                            id:'btn_agregar',
                            width:120
                        },
                        {}
                    ]}
            ]
        },
    ],
    elementsConfig: {
        labelPosition: "top"
    },
    rules:{
        //"id_articulo":webix.rules.isNotEmpty,
        "cantidad" :function(val){
            if (webix.rules.isNumber(val) && val>0)
                return true;
        }
    }
};

var total={
    rows:[
        {
            cols:[
                {},
                {view:"label",label:"Total: ",css:"precio",width:80},
                {view:"label",id:"label_total",label:"0.00 ",css:"total"}
            ]
        },
        {},
        {
            cols:[
                {},
                {view:"label",label:"Cantidad: ",css:"cantidad",width:80},
                {view:"label",id:"label_cantidad",label:"0 ",css:"cantidad_valor"}
            ]
        },
        {cols:[
            {},
            {view:"button",label:"Realizar venta [Ctrl+F1]",click:"cobrarVenta()",type:"imageButton",image:BASE_URL_ICONS+"realizar_venta.png",width:195},
            {view:"button",label:"Cancelación de venta [Supr]",click:"cancelarVenta()",type:"imageButton",image:BASE_URL_ICONS+"cancelar_venta.png",width:180},
            {}
        ]},
        {height:15},
        toolbar_ventas
    ]
};
var sup_ventas={
    view:"fieldset",
    label:"Datos de la venta",
    body:{
        cols:[
            form_ventas,
            total
        ]
    }
};
var datatable_ventas ={
    view:"datatable",
    id:"datatable_ventas",
    select:"row",
    navigation:true,
    editable:true,
    columns:[
        {id:"id",hidden:true},
        //{id:"numero",header:"#",width:35,template:"#number#"},
        {id:"existencia",hidden:true},
        {id:"id_articulo",hidden:true},
        {id:"descripcion",header:["Descripción del articulo"],fillspace:true},
        {id:"cantidad",header:"Cantidad",editor:"text"},
        {id:"precio",header:"Precio",template:"#precio#"},
        {id:"subtotal",header:"Subtotal",template:"<b>#subtotal#</b>"},
        {id:"eliminar",header:"Eliminar",template:'<button onclick="deletes(#id#)">Eliminar</button>'}
    ],
    on:{
        onAfterAdd:calcularTotal,
        onAfterDelete:calcularTotal,
        onBeforeEditStart:function(id){
            //var item = this.getItem(id)
            //console.log(item)
            // if (id.column == "cantidad" && !item.comun == '0') return false;
        },
        onAfterEditStop:function(){
            var item = this.getSelectedItem();
            add(item)
            /*//console.log(item)
            if (item.comun == 0){
                var mensaje = {type:'alert-error',text:'Articulo insuficiente'};
                var flag = true;
                var articulo = obtenerArticulo(item.id_articulo);
                console.log(articulo.descripcion)
                var existencia =0;
                var cantidad = parseFloat(item.cantidad);
                if (item.tipo_venta==1){
                    if (cantidad>parseFloat(articulo.existencia_unidad) && obj_pref.inventario_estricto == 1){
                        webix.alert(mensaje);
                        existencia = articulo.existencia_unidad;
                        flag =false;
                    }
                }
                else {
                    if (cantidad>parseFloat(articulo.existencia) && obj_pref.inventario_estricto==1){
                        webix.alert(mensaje);
                        existencia=articulo.existencia;
                        flag =false;
                    }
                }
                if(flag){
                    actualizarCantidad(item);
                }
                else{
                    //this.updateItem(item.id,{cantidad:existencia});
                    actualizarCantidad(item);
                }
                $$('id_articulo').focus();
            }*/
        }
    }
}
