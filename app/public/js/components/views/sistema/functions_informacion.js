function save (){
    if ($$('form_informacion').validate()){
        var data  = $$('form_informacion').getValues();
        delete data['tema'];
        delete data['id'];
        webix.ajax().post(URL_SAVE,data,function(response){
            response = JSON.parse(response);
            webix.alert(response.message);
        });
    }
    else{
        webix.alert({type:'alert-error',text:'Debe completar todos los campos.'})
    }
}

function tema(n,v){
    webix.ajax().post(URL_SAVE,{id_tema:n},function(response){
        if (v!=undefined){
            $$('cargando').show();
            location.reload();
        }


    });
}