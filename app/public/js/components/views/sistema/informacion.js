URL_TEMAS=BASE_URL+'sistema/informacion/getTemas';
URL_INFORMACION=BASE_URL+'sistema/informacion/informacion';
URL_UPLOAD=BASE_URL+'sistema/informacion/do_upload';
URL_SAVE=BASE_URL+'sistema/informacion/save';
var form={
    view:'form',
    id:'form_informacion',
    elements:[
        {rows:[
            {view:'fieldset',label:'Información del negocio',
                body:{
                    rows:[
                        {
                            cols:[
                                {view:'text',id:'nombre_negocio',name:'nombre_negocio',label:'Nombre del negocio',labelWidth:150},
                                {view:'text',id:'propietario',name:'propietario',label:'Nombre del propietario',labelWidth:150},
                            ]},
                        {height:20},
                        {cols:[
                            {view:'text',id:'rfc',name:'rfc',label:'RFC'},
                            {view:'text',id:'telefono',name:'telefono',label:'Telefono del negocio',labelWidth:150},
                            {view:'text',id:'telefono_propietario',name:'telefono_propietario',label:'Telefono del propietario',labelWidth:160},]},
                        {height:20},{
                            cols:[
                                {
                                    rows:[
                                        {view:'combo',label:'Tema',options:URL_TEMAS,id:'id_tema',name:'id_tema',width:350,on:{
                                            onchange:tema
                                        }},
                                        {height:30}
                                    ]},
                                {rows:[
                                    {view:'uploader',label:'Subir logo',id:'uploader_logo',link:'list_logo',upload:URL_UPLOAD,multiple:false,datatype:'json'},
                                    {view:'list',type:'uploader',id:'list_logo'}
                                ]
                                }]},
                        {height:30},
                        {
                            cols:[
                            {},
                            {view:'button',label:'Guardar',type:'imageButton',image:BASE_URL_ICONS+'success.png',width:100,click:"save()"}
                        ]
                        }
                    ]
                }
            }
        ]
        }
    ],
    elementsConfig:{
        labelPosition:"top"},
    rules:{
        "nombre_negocio":webix.rules.isNotEmpty,
        "propietario":webix.rules.isNotEmpty,
        "rfc":webix.rules.isNotEmpty,
        "telefono":webix.rules.isNotEmpty,
        "telefono_propietario":webix.rules.isNotEmpty,
        "id_tema":webix.rules.isNotEmpty,},
    on:{
        onSubmit:save}
}