

function change_date(){
    var fecha_fin = $$('fecha_fin').getPopup().getBody();
    fecha_fin.define('minDate',$$('fecha_inicio').getValue());
    $$('fecha_fin').refresh();
}


function change_especifico(val){
    if (val){
        $$('fieldset_especifico').define('disabled',false);
        $$('tipo_basico').setValue(0);
        $$('reporte_detallado').setValue(1);
    }
    else
        $$('fieldset_especifico').define('disabled',true);
}
function change_basico(val){
    if (val){
        $$('tipo_reporte').define('disabled',false);
        $$('tipo_especifico').setValue(0);
    }
    else
        $$('tipo_reporte').define('disabled',true);
}

function reporteArticulos(){
    window.open(BASE_URL+'reportes/pdf/articulos');
}
function  reporte(){
    if ($$('form_reporte_ventas').validate()){
        if ($$('tipo_especifico').getValue()){
            var reporte ="ventas";
            if(id_negocio==2)
                reporte = "ventas2";
            var detalle = ($$('reporte_detallado').getValue()==1)?'356a192b7913b04c54574d18c28d46e6395428ab':'b6589fc6ab0dc82cf12099d1c2d40ab994e8410c';
            var tipo_reporte = ($$('tipo_especifico').getValue())?'ac3478d69a3c81fa62e60f5c3696165a4e5e6ac4':$$('tipo_reporte').getValue();
            var group = $$('tipo_agrupar').getValue();
            location.reload();
            window.open(BASE_URL+'reportes/pdf/'+reporte+'?tipo='+tipo_reporte+'&detalle='+detalle+'&fecha_inicio='+$$('fecha_inicio').getText()+'&fecha_fin='+$$('fecha_fin').getText()+'&group='+group);
        }
        else{
            webix.alert({type:'alert-error',text:'Seleccione un tipo de reporte'});
        }

        }
}