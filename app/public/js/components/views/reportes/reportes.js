
var sup_reportes ={
    width:1200,
    rows:[
        {view:'fieldset',label:'Ventas',
        body:{
         cols:[
             {},
             {
                 view:'form',
                 id:'form_reporte_ventas',
                 elements:[
                     {
                         rows:[
                             {view:'label',label:'Generar reportes de ventas',align:'center'},
                             /*{cols:[
                                 {
                                     view:'checkbox',
                                     id:'tipo_basico',
                                     label:'Basico',
                                     value:1,
                                     width:120,
                                     labelWidth:80,
                                     on:{
                                         onChange:change_basico,
                                         onAfterLoad:function(){
                                             alert("Jola");
                                         }
                                     }
                                 },
                                 {view:'combo',id:'tipo_reporte',name:'tipo_reporte',value:'356a192b7913b04c54574d18c28d46e6395428ab',label:'Tipo de reporte',width:400,labelWidth:120,options:[
                                     {id:'356a192b7913b04c54574d18c28d46e6395428ab',value:'Dia'},
                                     {id:'da4b9237bacccdf19c0760cab7aec4a8359010b0',value:'Semana'},
                                     {id:'77de68daecd823babbb58edb1c8e14d7106e83bb',value:'Mes'},
                                     {id:'1b6453892473a467d07372d45eb05abc2031647a',value:'Año'},
                                 ]}
                             ]},*/
                             {
                                 cols:[
                                     {
                                         view:'checkbox',
                                         id:'tipo_especifico',
                                         label:'Especifico',
                                         width:120,
                                         labelWidth:80,
                                         on:{
                                             onChange:change_especifico
                                         }
                                     },
                                     {
                                         view:'fieldset',
                                         id:'fieldset_especifico',
                                         label:'Reporte especifico',
                                         disabled:true,
                                         width:450,
                                         body:{
                                             rows:[
                                                 {
                                                     cols:[
                                                         {view:'datepicker',label:'De: ',id:'fecha_inicio',width:200,format:'%Y-%m-%d',labelWidth:60,on:{
                                                             onChange:change_date
                                                         }},
                                                         {view:'datepicker',label:'Hasta: ',id:'fecha_fin',width:200,format:'%Y-%m-%d',labelWidth:60},
                                                     ]
                                                 },
                                                 {
                                                     view:'radio',
                                                     value:'da4b9237bacccdf19c0760cab7aec4a8359010b0',
                                                     id:'tipo_agrupar',
                                                     label:'Agrupar por:',
                                                     labelWidth:100,
                                                     width:450,
                                                     options:[
                                                         //{id:'356a192b7913b04c54574d18c28d46e6395428ab',value:'Venta'},
                                                         {id:'da4b9237bacccdf19c0760cab7aec4a8359010b0',value:'Dia',},
                                                        // {id:'77de68daecd823babbb58edb1c8e14d7106e83bb',value:'Semana'},
                                                         //{id:'1b6453892473a467d07372d45eb05abc2031647a',value:'Mes'},
                                                     ]
                                                 }
                                             ]
                                         }},
                                 ]
                             },
                             { cols:[
                                 {width:120},
                                 {view:'checkbox',label:'Reporte detallado',labelWidth:130,id:'reporte_detallado',name:'reporte_detallado'},
                             ]},
                            // {view:'radio',label:'Semana'},
                             {
                                 cols:[
                                     {width:120},
                                     {view:'button',label:'Generar reporte de ventas',type:'imageButton',image:BASE_URL_ICONS+'export_pdf.png',width:230,click:'reporte()'},

                                 ]
                             }


                         ]
                     }
                 ],
                 borderless:true,
                 rules:{
                   // "tipo_reporte":webix.rules.isNotEmpty
                 }
             },
             {}
         ]
        }
        },
        {height:30},
        {view:'fieldset',label:'Articulos',
            body:{
                cols:[
                    {},
                    {
                        rows:[
                            {view:'label',label:'Generar reportes de articulos en baja existencia',align:'center',width:400},
                            {view:'button',label:'Generar reporte de articulos',type:'imageButton',image:BASE_URL_ICONS+'export_pdf.png',width:230,align:'center',click:'reporteArticulos()'},
                        ]
                    },
                    {}
                ]
            }
        },
        /*
        {width:30},
        {view:'datepicker',label:'Fecha de inicio',id:'fecha_inicio',width:250,labelWidth:110,format:'%Y-%m-%d'},
        {view:'datepicker',label:'Fecha de fin',id:'fecha_fin',width:250,labelWidth:110,format:'%Y-%m-%d'},
        {view:'button',label:'Buscar',type:'imageButton',image:BASE_URL_ICONS+'search.png',width:100}*/
    ]
}
