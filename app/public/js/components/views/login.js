URL_GET_ACCESS =BASE_URL +"login/getAccess";
function show_popup_secure(x){
    $$("win1").show(x, { pos: "bottom"});
}
function validate(){
    var form = $$('login_form');
    if (form.validate()){
       // $$('cargando').show();
        var data = form.getValues();
        data['id_negocio'] = id_negocio;
        webix.ajax().post(URL_GET_ACCESS,data,function(response){
            response = JSON.parse(response);
            if (response.status){
                window.location=response.url;
            }
            else{
                alert_error.text="Usuario o contraseña incorrectos";
                alert_error.callback=function () {
                    form.clear();
                    form.clearValidation();
                    $$('usuario').focus();
                };
                webix.alert(alert_error);

            }
            $$('cargando').hide();
        });
    }
}
var login_form ={
    id:"login_form",
    view:"form",
    width:400,
    height:200,
    elements:[
        {rows:[
            {view:"text",name:"usuario",id:"usuario",label:"Usuario",labelWidth:90,invalidMessage:"Campo requerido"},
            {view:"text",type:"password",name:"contrasena",id:"contrasena",label:"Contraseña",labelWidth:90,invalidMessage:"Campo requerido" },
            {cols:[
                {}, {view:"button",label:"Entrar",width:150,position:"center",click:"validate()",type:'form'},{}
            ]},
            {
                cols:[
                    {},
                    {view:'label',label:'Su datos estan protegidos.',css:'green',width:180},
                    {view:'template', id:'icon_secure',css:'secure_icon',template:"<img onmouseover='show_popup_secure(this)' width='32px' src='"+BASE_URL+ 'public/images/secure.png'+"'/>",borderless:true, width:50,height:50},
                    {}
                ]
            }

        ]}
    ],
    rules:{
        "usuario":webix.rules.isNotEmpty,
        "contrasena":webix.rules.isNotEmpty
    },
    on:{
        onSubmit:validate
    }
};
var login_window={
    view:"window",
    head:"Inicio de sesión",
    body:webix.copy(login_form),
    //modal:true,
    width:1000,
    position:"center",
    height:400
};
 var popup_secure={
    view:"popup",
    id:"win1",
    height:150,
    width:300,
     animate:{ direction:"top"},
    head:"This window is centered",
    position:"center",
    body:{
        rows:[
            {view:'template',css:'green',template:'Usamos la tecnología de encriptado SSL para mantener todos sus datos seguros.',borderless:true,height:50},
            {view:'label',css:'a_green',label:'Más información',align:'center',template:'<a target="_blank" href="https://www.cloudflare.com/ssl/">#label#</a>'}
        ]
    }
};
