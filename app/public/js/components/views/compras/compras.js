URL_GET_ARTICULOS = BASE_URL + "ventas/ventas/getArticulos";
URL_GET_ARTICULO = BASE_URL + "ventas/ventas/getArticulo";
URL_GET_ARTICULO = BASE_URL + "ventas/ventas/getArticulo";
URL_COMPRAS_SAVE = BASE_URL + "compras/compras/save";
var art = {};
var articulos = [];
var articulosCombo = [];
function obtenerArticulo(id) {
    var result;
    articulos.forEach(function(item) {
        if (item.id == id)
            result = item;
    });
    return result;
}
function obtenerArticuloCodigo(codigo) {
    var result;
    articulos.forEach(function(item) {
        if (item.codigo_barras == codigo) {
            result = item.id;
        }
    });
    return result;
}
function actualizarArticulos() {
    webix.ajax().sync().post(URL_GET_ARTICULOS, {}, function(response) {
        response = JSON.parse(response);
        articulos = response;
        articulos.forEach(function(item) {
            articulosCombo.push({
                value: item.descripcion,
                id: item.id
            });
        });
        $$('id_articulo').define('options', articulosCombo);
        $$('id_articulo').refresh();
        $$('id_articulo').focus()
    });
}
function number(val) {
    if (val >= 48 && val <= 57)
        event.returnValue = true;
    else if (val == 8)
        event.returnValue = true;
    else if (val == 190 || val == go110)
        event.returnValue = true;
    else
        event.returnValue = false;
}

function comprar() {
    if ($$('form_compras').validate()) {
        webix.ajax().post(URL_COMPRAS_SAVE, $$('form_compras').getValues(), function(response) {
            response = JSON.parse(response);
            webix.alert(response.message);
            $$('cantidad').blur();
            $$('form_compras').clear();
            $$('form_compras').clearValidation();
            $$('label_existencia').define('label', 'Existencia: ');
            $$('label_existencia').refresh();
            actualizarArticulos();
            $$('id_articulo').focus();
        });
    } else {
        webix.alert({
            type: "alert-error",
            text: "Todos los campos son necesarios."
        });
        $$('cantidad').focus();
    }
}

function llenar(response) {
    art.precio_unidad_mayoreo = response['precio_unidad_mayoreo'];
    art.precio_compra = response['precio_compra'];
    art.precio_venta = response['precio_venta'];
    art.precio_mayoreo = response['precio_mayoreo'];
    art.venta_unidad = response['venta_unidad'];
    art.existencia = response['existencia'];
    art.id_articulo = response['id'];
    $$('venta_unidad').setValue(art.venta_unidad);
    $$('precio_compra').setValue(art.precio_compra);
    $$('precio_venta').setValue(art.precio_venta);
    $$('precio_mayoreo').setValue(art.precio_mayoreo);
    $$('label_existencia').define('label', 'Existencia: ' + art.existencia);
    $$('label_existencia').refresh();
}
var form_compras = {
    view: "form",
    id: "form_compras",
    elements: [
        {
        cols: [
            {},
            {
                width:700,
            rows: [{
                view: "text",
                hidden: true,
                id: "venta_unidad",
                name: "venta_unidad" + ""
            }, {
                view: "combo",
                id: "id_articulo",
                name: "id_articulo",
                label: "Articulo",
                on: {
                    onChange: function(nv) {
                        if (obtenerArticulo(nv) != undefined) {
                            llenar(obtenerArticulo(nv));
                            $$('cantidad').focus();
                        }
                    },
                    onKeyPress: function(val) {
                        if (val == 13) {
                            if (webix.rules.isNumber($$('id_articulo').getText())) {
                                var id_articulo = obtenerArticuloCodigo($$('id_articulo').getText());
                                llenar(obtenerArticulo(id_articulo));
                                $$('id_articulo').setValue(id_articulo);
                                $$('cantidad').focus();
                            }
                        }
                    }
                }
            }, {
                cols: [
                    {
                        view: "label",
                        label: "Existencia: ",
                        id: "label_existencia",
                        width: 150
                    },
                    {
                        view: "text",
                        label: "costo",
                        id: "precio_compra",
                        name: "precio_compra",
                        width:150,
                        on: {
                            onKeyPress: number
                        },

                    },
                    {
                        view: "text",
                        label: "Precio venta",
                        id: "precio_venta",
                        name: "precio_venta",
                        on: {
                            onKeyPress: number
                        }
                    },
                    {
                        view: "text",
                        label: "Precio (Mayoreo)",
                        id: "precio_mayoreo",
                        name: "precio_mayoreo",
                        width:200,
                        on: {
                            onKeyPress: number
                        }
                    }
                ]
            }, {
                view: "text",
                value: 0,
                id: "cantidad",
                name: "cantidad",
                label: "Cantidad",
                width: 150,
                on: {
                    onKeyPress: function(val) {
                        if (val >= 48 && val <= 57)
                            event.returnValue = true;
                        else if (val == 13)
                            comprar();
                        else if (val == 8)
                            event.returnValue = true;
                        else if (val == 190)
                            event.returnValue = true;
                        else
                            event.returnValue = false;
                    }
                }
            }, {
                cols: [{}, {
                    view: "button",
                    label: "Aceptar",
                    type: "imageButton",
                    image: BASE_URL_ICONS + "success.png",
                    width: 100,
                    click: "comprar()"
                }]
            }]
        }, {}]
    }],
    elementsConfig: {
        labelPosition: "top"
    },
    rules: {
        "id_articulo": webix.rules.isNotEmpty,
        "precio_compra": webix.rules.isNumber,
        "precio_venta": webix.rules.isNumber,
        "precio_mayoreo": webix.rules.isNumber,
        "cantidad": webix.rules.isNotEmpty
    }
}