var options_estados=[{id:"",value:"Todos"},{id:"1",value:"Activos"},{id:"2",value:"Inactivos"}];
var required ="Campo requerido";
var valid_number="Ingrese un numero valido.";
var valid_email = "Ingrese un correo electrónico valido.";
var valid_phone ="Ingrese un numero de telefono valido."
var valid_rfc = "ingrese un RFC valido";

function existValue(obj,index,value){
  var flag=false;
  obj.forEach(function (item) {
      if (item[index] == value)
          flag =true;
  });

  return flag;
}
function isNumericRequired(value){
  if (!isNaN(value) && parseInt(value)>0  && value.length>0)
    return true;
    else
      return false;
}
function isNumeric(value){
  if (!isNaN(value))
    return true;
    else
      return false;
}

function isNumericLength(value,min,max){
  if (value>0)
       return true;

}

function isEmail(value){
  if (value!=""){
    if (webix.rules.isEmail(value))
      return true
  }
  else
    return true;

}
function isRfc(value){
  var l = value.slice(0,4);
  var n =value.slice(4,10);
    if(webix.rules.isNumber(n) && isNaN(l) && value.length ==13)
      return true;

}
function formatObjPreferences(obj) {
   for (var index in obj){
     if (!isNaN(obj[index]))
       obj[index] = parseInt(obj[index]);
   }
  return obj;
}

function changeStatus(url,data,text,funct){
  webix.confirm({
    "text":text,
    ok:"Si",
    cancel:"No",
    callback:function(result){
      if(result){
        webix.ajax().post(url,data,funct);
      }
    }
  });
  return true;
}