var hora_cliente = new Date();
function clock(time){
    var m = "AM";
    var hrs = time.getHours();
    if (hrs>12){
        hrs -=12;
        m="PM";
    }
    var secs = time.getSeconds();
    if (secs<10)
        secs="0"+secs;
    var mins = time.getMinutes();
    if (mins<10)
        mins="0"+mins;
    var line = hrs+":"+mins+":"+secs+" "+m;
    return line;
}
function date(){
    days =[
        "Domingo",
        "Lunes",
        "Martes",
        "Miercoles",
        "Jueves",
        "Viernes",
        "Sabado"
    ];
    months =[
        "Enero",
        "Febrero",
        "Marzo",
        "Abril",
        "Mayo",
        "Junio",
        "Julio",
        "Agosto",
        "Septiembre",
        "Octubre",
        "Noviembre",
        "Diciembre"
    ];
    var date  = new Date();
    var day = date.getDay();
    var number = date.getDate();
    var month = date.getMonth();
    var year = date.getFullYear();
    var full_date = days[day] + " " +number + " de " + months[month] +" del "+ year;
    return full_date;

};
var window_calendar ={
    view:"window",
    id:"window_calendar",
    width:400,
    height:400,
    head:{view:"toolbar",cols:[{view:"label",label:"Calendario",align:"center"},{view:"button",label:"X",width:50,click:"$$('window_calendar').hide()"}]},
    position:"center",
    move:true,
    body:{
        view:"calendar",
        date:new Date()
    }
};




