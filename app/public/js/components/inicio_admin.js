URL_CHART_LINE = BASE_URL + 'inicio/ventas';




function loadChartLine(tipo){
    var chart = $('#chart_line').highcharts();
    webix.ajax().post(URL_CHART_LINE,{tipo:tipo},function(response){
        response = JSON.parse(response);
        chart.xAxis[0].setCategories(response.categorias);
        response.ventas['data'].forEach(function (item,index){
            response.ventas['data'][index]=parseFloat(response.ventas['data'][index]);
        });
        response.ganancias['data'].forEach(function (item,index){
            response.ganancias['data'][index]=parseFloat(response.ganancias['data'][index]);
        });
        while(chart.series.length > 0)
            chart.series[0].remove(true);
        chart.addSeries(response.ventas);
        chart.addSeries(response.ganancias);
    });

}